import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: ()=>import('../views/index/IndexView.vue')
    },
    {
      path: '/search',
      name:'search',
      component: () => import('../views/index/SearchView.vue')
    },
    {
      path: '/all',
      // component: () => import('../views/all/IndexView.vue')
      component: () => import('../views/all/RoleView.vue')
    },
    {
      path: '/about',
      component: () => import('../views/about/AboutView.vue')
    },
    {
      path: '/i',
      component: () => import('../views/i/IndexView.vue')
    }
  ]
})

export default router
