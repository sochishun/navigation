/**
 * 搜索引擎配置
 */
const data = [
    {
        "name": "baidu",
        "text": "百度",
        "link": "https://www.baidu.com/s?wd=__keyword__"
    },
    {
        "name": "bing",
        "text": "Bing",
        "link": "https://cn.bing.com/search?q=__keyword__"
    },
    {
        "name": "youdaodict",
        "text": "有道词典",
        "link": "http://dict.youdao.com/w/__keyword__"
    },
    {
        "name": "site",
        "text": "站内搜索",
        "link": "/search?s=__keyword__"
    }
];

export default data;