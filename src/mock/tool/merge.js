/**
 * 读取 _category.json 文件中的分类配置数据，合并所有分类文件的内容到 _all.js 文件。
 * 用法：在命令行进入 /src/mock/tool/ 目录路径，执行命令：node merge.js
 */
import fs from 'fs';

const sortList = JSON.parse(fs.readFileSync('../_category.json'));
let arr = [], path = '';
for (let name of sortList) {
    path = `../${name}.json`;
    if (!fs.existsSync(path)) {
        console.log('文件不存在：', path);
        continue;
    }
    arr.push({
        category: name,
        data: JSON.parse(fs.readFileSync(path))
    })
}

const content="const data=\n__data__;\n\nexport default data;\n";
fs.writeFileSync('../_all.js',content.replace('__data__',JSON.stringify(arr)));
console.log('文件合并完成.');