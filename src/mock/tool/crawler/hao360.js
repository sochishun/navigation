/**
 * 采集 360
 * @link https://hao.360.com
 */
const fs = require('fs');
const axios = require('axios')
const cheerio = require('cheerio')
// Node 环境当中不支持 GBK 编码，所以需要引用 iconv-lite 模块来转码
const iconv = require('iconv-lite');
const { text } = require('cheerio/lib/static');

axios.defaults.timeout = 10000; // 10秒超时
const SITE_NAME = 'hao360';
const RUNTIME_PATH = './runtime/' + SITE_NAME + '/';
const SQL_FILE_PATH = RUNTIME_PATH + SITE_NAME + '.sql';
const JSON_FILE_PATH = RUNTIME_PATH + SITE_NAME + '.json';
if (!fs.existsSync(RUNTIME_PATH)) {
    fs.mkdirSync(RUNTIME_PATH)
}


(async function () {
    let links = [
        { name: 'xiaoshuo', text: '小说', link: 'https://hao.360.com/sub/xiaoshuo_website_nav.html' },
        { name: 'yinyue', text: '音乐', link: 'https://hao.360.com/sub/yinyue_website_nav.html' },
        { name: 'zhaopin', text: '招聘', link: 'https://hao.360.com/sub/zhaopin_website_nav.html' },
        { name: 'youxiang', text: '邮箱', link: 'https://hao.360.com/sub/youxiang_website_nav.html' },
        { name: 'news', text: '新闻', link: 'https://hao.360.com/sub/news_website_nav.html' },
        { name: 'tiyu', text: '体育', link: 'https://hao.360.com/sub/tiyu_website_nav.html' },
        { name: 'caijing', text: '财经', link: 'https://hao.360.com/sub/caijing_website_nav.html' },
        { name: 'kuaidi', text: '快递', link: 'https://hao.360.com/sub/kuaidi_website_nav.html' },
        { name: 'diannao', text: '电脑', link: 'https://hao.360.com/sub/diannao_website_nav.html' },
        { name: 'nvxing', text: '女性', link: 'https://hao.360.com/sub/nvxing_website_nav.html' },
        { name: 'shouji', text: '手机', link: 'https://hao.360.com/sub/shouji_website_nav.html' },
        { name: 'qiche', text: '汽车', link: 'https://hao.360.com/sub/qiche_website_nav.html' },
        { name: 'zhengfu', text: '政府', link: 'https://hao.360.com/sub/zhengfu_website_nav.html' },
        { name: 'fangchan', text: '房产', link: 'https://hao.360.com/sub/fangchan_website_nav.html' },
        { name: 'sheji', text: '设计', link: 'https://hao.360.com/sub/sheji_website_nav.html' },
        { name: 'sheying', text: '摄影', link: 'https://hao.360.com/sub/sheying_website_nav.html' },
        { name: 'chongwu', text: '宠物', link: 'https://hao.360.com/sub/chongwu_website_nav.html' },
        { name: 'jiankang', text: '健康', link: 'https://hao.360.com/sub/jiankang_website_nav.html' },
        { name: 'kuzhan', text: '酷站', link: 'https://hao.360.com/sub/kuzhan_website_nav.html' },
        { name: 'difang_beijing', text: '北京', link: 'https://hao.360.com/sub/difang_beijing.html' },
        { name: 'difang_shanghai', text: '上海', link: 'https://hao.360.com/sub/difang_shanghai.html' },
        { name: 'difang_guangdong', text: '广东', link: 'https://hao.360.com/sub/difang_guangdong.html' },
    ];
    let list = [];
    for (let item of links) {
        list = list.concat(await getJson(item.name, item.text, item.link));
    }
    fs.writeFileSync(JSON_FILE_PATH, JSON.stringify(list));
    fs.writeFileSync(SQL_FILE_PATH, json2sql(list).join("\n"))
    console.log('列表采集完毕.');
})()


async function getJson(name, text, url) {
    let tempFile = RUNTIME_PATH + name + ".html", html = '';
    if (!fs.existsSync(tempFile)) {
        html = (await axios.get(url)).data;
        fs.writeFileSync(tempFile, html);
    } else {
        html = fs.readFileSync(tempFile);
    }
    // html=iconv.decode(html,'gbk');
    let $ = cheerio.load(html);
    let data = { text: text, data: [] };
    $('.text-link-wrap').each(function (i, el) {
        let $el = $(el), $elFirst = $el.find('h2');
        if (['大家都在搜', '精彩cube'].includes($elFirst.text())) {
            return;
        }
        let item = { text: $elFirst.text(), data: [] };
        $el.find('li a').each(function (i2, el2) {
            let $el2 = $(el2);
            item.data.push({ text: $el2.text(), link: $el2.attr('href') });
        })
        data.data.push(item);
        // console.log(data);
    })
    return data;
}
function json2sql(json) {
    let sqlArr = [], table = 'tbl_navigate', id = 0, pid = 0;
    json.forEach(item => {
        id++;
        sqlArr.push(`INSERT INTO ${table} (id, pid, text, link) VALUES ('${id}', 0, '${item.text}', '${item.link}');`);
        pid = id;
        item.children.forEach(item2 => {
            id++;
            sqlArr.push(`INSERT INTO ${table} (id, pid, text, link) VALUES ('${id}', ${pid}, '${item2.text}', '${item2.link}');`);
            pid = id;
            item2.children.forEach(item3 => {
                id++;
                sqlArr.push(`INSERT INTO ${table} (id, pid, text, link) VALUES ('${id}', ${pid}, '${item3.text}', '${item3.link}');`);
            })
        })
    })
    return sqlArr;
}