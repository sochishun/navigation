/**
 * 收藏配置
 */

const data = [
    { text: '360导航少年版', url: 'https://young.hao.360.com/' },
    { text: '快科技', url: 'https://www.mydrivers.com/' },
    { text: '开源中国', url: 'https://www.oschina.net/' },
    { text: '微信读书', url: 'https://weread.qq.com/' },
    { text: '腾讯课堂', url: 'https://ke.qq.com/' },
    { text: '西瓜视频', url: 'https://www.ixigua.com/' },
    { text: '抖音视频', url: 'https://www.douyin.com/' },
    { text: '快手视频', url: 'https://www.kuaishou.com/' },
    { text: '小红书视频', url: 'https://www.xiaohongshu.com/explore' },
];

export default data;