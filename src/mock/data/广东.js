export const hotlinks = [
];

export default [
    {
        "text": "广东门户",
        "data": [
            {
                "text": "广东省人民政府",
                "link": "http://www.gd.gov.cn/"
            },
            {
                "text": "深圳新闻网",
                "link": "http://www.sznews.com/"
            },
            {
                "text": "南方网",
                "link": "http://www.southcn.com/"
            },
            {
                "text": "深圳之窗",
                "link": "http://www.shenchuang.com/"
            },
            {
                "text": "奥一网",
                "link": "http://www.oeeee.com/"
            },
            {
                "text": "荔枝网",
                "link": "http://www.gdtv.com.cn/"
            },
            {
                "text": "广州日报大洋网",
                "link": "http://www.dayoo.com/"
            },
            {
                "text": "深圳热线",
                "link": "http://www.szonline.net"
            },
            {
                "text": "南方周末",
                "link": "http://www.infzm.com/"
            },
            {
                "text": "南方网",
                "link": "http://www.southcn.com/"
            },
            {
                "text": "南方都市报",
                "link": "http://epaper.oeeee.com/"
            },
            {
                "text": "广州日报电子报",
                "link": "http://gzdaily.dayoo.com/"
            },
            {
                "text": "东莞阳光网",
                "link": "http://www.sun0769.com/"
            },
            {
                "text": "佛山新闻网",
                "link": "http://www.foshannews.net/"
            },
            {
                "text": "今日惠州网",
                "link": "http://www.huizhou.cn/"
            },
            {
                "text": "茂名在线",
                "link": "http://www.gdmm.com/"
            },
            {
                "text": "深港在线",
                "link": "http://www.szhk.com/"
            },
            {
                "text": "深圳都市网",
                "link": "http://www.citysz.net/"
            },
            {
                "text": "掌上东莞",
                "link": "http://www.dg163.cn/"
            },
            {
                "text": "大埔网",
                "link": "http://www.514200.com/"
            },
            {
                "text": "广州妈妈网",
                "link": "http://www.gzmama.com/"
            },
            {
                "text": "茂名论坛",
                "link": "http://bbs.gdmm.com/"
            },
            {
                "text": "惠州西子论坛",
                "link": "http://bbs.xizi.com/"
            }
        ]
    },
    {
        "text": "本地生活",
        "data": [
            {
                "text": "天气预报",
                "link": "http://www.weather.com.cn/html/province/guangdong.shtml"
            },
            {
                "text": "广东专家门诊",
                "link": "https://gz.haodf.com/"
            },
            {
                "text": "三甲医院大全",
                "link": "https://yyk.99.com.cn/sanjia/guangdong/"
            },
            {
                "text": "深圳房地产信息网",
                "link": "http://www.szhome.com/"
            },
            {
                "text": "广东省人才市场",
                "link": "http://www.gdrc.com/"
            },
            {
                "text": "进出口商品交易会",
                "link": "http://www.cantonfair.org.cn/"
            },
            {
                "text": "广东省大学网址",
                "link": "http://hao.360.cn/sub/daxue_guandong.html"
            },
            {
                "text": "中山大学",
                "link": "http://www.sysu.edu.cn/"
            },
            {
                "text": "华南理工大学",
                "link": "http://www.scut.edu.cn/"
            },
            {
                "text": "华南师范大学",
                "link": "http://www.scnu.edu.cn/"
            },
            {
                "text": "暨南大学",
                "link": "http://www.jnu.edu.cn/"
            }
        ]
    },
    {
        "text": "各地政府",
        "data": [
            {
                "text": "广州",
                "link": "http://www.gz.gov.cn/"
            },
            {
                "text": "深圳",
                "link": "http://www.sz.gov.cn/"
            },
            {
                "text": "珠海",
                "link": "http://www.zhuhai.gov.cn/"
            },
            {
                "text": "汕头",
                "link": "http://www.shantou.gov.cn/"
            },
            {
                "text": "佛山",
                "link": "http://www.foshan.gov.cn/"
            },
            {
                "text": "韶关",
                "link": "http://www.sg.gov.cn/ "
            },
            {
                "text": "河源",
                "link": "http://www.heyuan.gov.cn/"
            },
            {
                "text": "梅州",
                "link": "http://www.meizhou.gov.cn/"
            },
            {
                "text": "惠州",
                "link": "http://www.huizhou.gov.cn/"
            },
            {
                "text": "汕尾",
                "link": "http://www.shanwei.gov.cn/"
            },
            {
                "text": "东莞",
                "link": "http://www.dongguan.gov.cn/"
            },
            {
                "text": "中山",
                "link": "http://www.zs.gov.cn/"
            },
            {
                "text": "江门",
                "link": "http://www.jiangmen.gov.cn/"
            },
            {
                "text": "阳江",
                "link": "http://www.yangjiang.gov.cn/"
            },
            {
                "text": "湛江",
                "link": "http://www.zhanjiang.gov.cn/"
            },
            {
                "text": "茂名",
                "link": "http://www.maoming.gov.cn/"
            },
            {
                "text": "肇庆",
                "link": "http://www.zhaoqing.gov.cn/"
            },
            {
                "text": "清远",
                "link": "http://www.gdqy.gov.cn/"
            },
            {
                "text": "潮州",
                "link": "http://www.chaozhou.gov.cn/"
            },
            {
                "text": "揭阳",
                "link": "http://www.jieyang.gov.cn/"
            },
            {
                "text": "云浮",
                "link": "http://www.yunfu.gov.cn/"
            }
        ]
    }
]