export const hotlinks = [
    {
        "text": "中国移动",
        "link": "http://www.10086.cn/"
    },
    {
        "text": "中国联通",
        "link": "http://www.10010.com/"
    },
    {
        "text": "中国电信",
        "link": "http://www.189.cn/"
    },
    {
        "text": "IT168手机",
        "link": "http://mobile.it168.com"
    },
    {
        "text": "IT168",
        "link": "http://www.it168.com/"
    },
    {
        "text": "中关村在线",
        "link": "http://www.zol.com.cn"
    },
];

export default [
    {
        "text": "手机综合",
        "data": [
            {
                "text": "中国移动",
                "link": "http://www.10086.cn/"
            },
            {
                "text": "中国联通",
                "link": "http://www.10010.com/"
            },
            {
                "text": "中国电信",
                "link": "http://www.189.cn/"
            },
            {
                "text": "太平洋手机频道",
                "link": "http://mobile.pconline.com.cn"
            },
            {
                "text": "中关村手机频道",
                "link": "http://mobile.zol.com.cn"
            },
            {
                "text": "网易手机频道",
                "link": "http://mobile.163.com"
            },
            {
                "text": "IT168手机",
                "link": "http://mobile.it168.com"
            },
            {
                "text": "泡泡手机网",
                "link": "http://mobile.pcpop.com"
            },
            {
                "text": "iMobile",
                "link": "http://www.imobile.com.cn/"
            },
            {
                "text": "天极手机",
                "link": "http://mobile.yesky.com/"
            },
            {
                "text": "IT之家手机评测",
                "link": "https://mobile.ithome.com/"
            }
        ]
    },
    {
        "text": "手机品牌",
        "data": [
            {
                "text": "苹果",
                "link": "http://www.apple.com.cn/iphone/"
            },
            {
                "text": "三星",
                "link": "http://www.samsung.com/cn"
            },
            {
                "text": "华为",
                "link": "https://consumer.huawei.com/cn/phones/"
            },
            {
                "text": "荣耀",
                "link": "https://www.hihonor.com/cn/"
            },
            {
                "text": "小米",
                "link": "https://www.mi.com/"
            },
            {
                "text": "索尼",
                "link": "http://www.sonystyle.com.cn/products/xperia/index.htm"
            },
            {
                "text": "OPPO",
                "link": "http://www.oppo.com/"
            },
            {
                "text": "vivo",
                "link": "http://www.vivo.com.cn/"
            },
            {
                "text": "一加",
                "link": "http://www.oneplus.cn/"
            },
            {
                "text": "魅族",
                "link": "http://www.meizu.com/"
            },
            {
                "text": "努比亚",
                "link": "http://www.nubia.cn/"
            },
            {
                "text": "锤子科技",
                "link": "http://www.smartisan.com/"
            },
            {
                "text": "摩托罗拉",
                "link": "http://www.motorola.com.cn/"
            }
        ]
    },
    {
        "text": "电脑综合",
        "data": [
            {
                "text": "太平洋电脑网",
                "link": "http://www.pconline.com.cn"
            },
            {
                "text": "中关村在线",
                "link": "http://www.zol.com.cn"
            },
            {
                "text": "电脑之家",
                "link": "http://www.pchome.net"
            },
            {
                "text": "网易科技",
                "link": "http://tech.163.com"
            },
            {
                "text": "搜狐IT",
                "link": "http://it.sohu.com"
            },
            {
                "text": "腾讯科技",
                "link": "https://new.qq.com/ch/tech/"
            },
            {
                "text": "驱动中国",
                "link": "http://www.qudong.com/"
            },
            {
                "text": "牛华网",
                "link": "http://www.newhua.com/"
            },
            {
                "text": "泡泡网",
                "link": "http://www.pcpop.com"
            },
            {
                "text": "天极网",
                "link": "http://www.yesky.com/"
            },
            {
                "text": "IT168",
                "link": "http://www.it168.com/"
            },
            {
                "text": "互联网研究",
                "link": "http://www.199it.com/"
            },
            {
                "text": "电脑商情在线",
                "link": "http://www.cbinews.com/"
            },
            {
                "text": "人民网IT频道",
                "link": "http://it.people.com.cn/"
            },
            {
                "text": "凤凰网科技",
                "link": "http://tech.ifeng.com"
            },
            {
                "text": "笔记本频道",
                "link": "http://notebook.pchome.net/"
            }
        ]
    },
    {
        "text": "硬件DIY",
        "data": [
            {
                "text": "ZOL硬件DIY",
                "link": "http://diy.zol.com.cn/"
            },
            {
                "text": "太平洋DIY硬件",
                "link": "http://diy.pconline.com.cn/"
            },
            {
                "text": "天极产品库",
                "link": "http://product.yesky.com/"
            },
            {
                "text": "第三媒体硬件中心",
                "link": "http://hard.thethirdmedia.com/"
            },
            {
                "text": "Chiphell硬件论坛",
                "link": "http://www.chiphell.com/"
            }
        ]
    },
    {
        "text": "电脑品牌",
        "data": [
            {
                "text": "联想",
                "link": "http://www.lenovo.com.cn/"
            },
            {
                "text": "戴尔",
                "link": "http://www.dell.com.cn/"
            },
            {
                "text": "惠普",
                "link": "http://www.hp.com.cn/"
            },
            {
                "text": "宏碁",
                "link": "http://www.acer.com.cn/"
            },
            {
                "text": "华硕",
                "link": "http://www.asus.com.cn/"
            },
            {
                "text": "索尼",
                "link": "http://www.sony.com.cn/"
            },
            {
                "text": "苹果",
                "link": "http://www.apple.com.cn/"
            },
            {
                "text": "三星",
                "link": "http://www.samsung.com/cn/"
            },
            {
                "text": "神舟",
                "link": "http://www.hasee.com/"
            },
            {
                "text": "方正",
                "link": "http://www.foundertech.com/"
            },
            {
                "text": "华为",
                "link": "https://consumer.huawei.com/cn/"
            },
            {
                "text": "微星",
                "link": "https://cn.msi.com/"
            },
            {
                "text": "ThinkPad",
                "link": "https://www.thinkpad.com/"
            },
            {
                "text": "Intel",
                "link": "https://www.intel.cn/"
            },
            {
                "text": "AMD",
                "link": "https://www.amd.com/zh-hans"
            },
            {
                "text": "七彩虹",
                "link": "https://www.colorful.cn/"
            },
            {
                "text": "影驰",
                "link": "http://www.szgalaxy.com/"
            },
            {
                "text": "雷蛇",
                "link": "http://cn.razerzone.com/"
            },
            {
                "text": "美商海盗船",
                "link": "https://www.corsair.com/zh/zh/"
            },
            {
                "text": "海尔",
                "link": "http://www.haier.com/cn/consumer/computers/"
            },
            {
                "text": "富士通",
                "link": "http://www.fujitsu.com/cn/"
            },
            {
                "text": "金士顿",
                "link": "https://www.kingston.com/cn"
            },
            {
                "text": "NVIDIA",
                "link": "https://www.nvidia.com/zh-cn/"
            },
            {
                "text": "闪迪",
                "link": "http://www.sandisk.cn/"
            },
            {
                "text": "森海塞尔",
                "link": "https://zh-cn.sennheiser.com/"
            },
            {
                "text": "铁三角",
                "link": "https://www.audio-technica.com.hk/"
            },
            {
                "text": "TP-Link",
                "link": "https://www.tp-link.com.cn/"
            },
            {
                "text": "腾达",
                "link": "https://www.tenda.com.cn/"
            },
            {
                "text": "D-link",
                "link": "http://www.dlink.com.cn/"
            },
            {
                "text": "西部数据",
                "link": "https://www.wd.com/zh-cn/"
            },
            {
                "text": "希捷",
                "link": "https://www.seagate.com/cn/zh/"
            }
        ]
    }
]