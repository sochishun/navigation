export const hotlinks = [
    { "text": "百度地图", "link": "http://map.baidu.com/" },
    { "text": "12306", "link": "http://www.12306.cn/" }, 
    { "text": "春秋航空", "link": "http://www.china-sss.com/" },
    { "text": "神州租车", "link": "http://www.zuche.com/" },
];

export default [
    {
        "text": "地图 ", "data": [
            { "text": "百度地图", "link": "http://map.baidu.com/" },
            { "text": "搜狗地图", "link": "http://map.sogou.com/" },
            { "text": "腾讯地图", "link": "http://map.qq.com/" },
            { "text": "高德地图", "link": "http://ditu.amap.com/" },
            { "text": "中国地图", "link": "http://map.baidu.com/?newmap=1&s=s%26wd%3D%E5%85%A8%E5%9B%BD%26c%3D1&from=alamap&tpl=mapcity" },
            { "text": "地球在线", "link": "http://www.earthol.com/" },
            { "text": "我要地图", "link": "http://www.51ditu.com/" },
            { "text": "GPS之家", "link": "http://bbs.gpsuu.com/" },
            { "text": "船讯网", "link": "http://www.shipxy.com/" },
            { "text": "城市吧街景", "link": "http://www.city8.com/" }
        ]
    },
    {
        "text": "交通 ", "data": [
            { "text": "列车时刻查询", "link": "https://go.hao123.com/train" },
            { "text": "公交查询", "link": "http://map.baidu.com/?daddr=%20&dirflag=t" },
            { "text": "长途汽车票网", "link": "http://www.trip8080.com/" },
            { "text": "坐车网", "link": "http://www.zuoche.com/" },
            { "text": "航班查询", "link": "http://go.hao123.com/flight" },
            { "text": "交通违章查询", "link": "http://life.hao123.com/jiaotong" },
            { "text": "深圳地铁", "link": "https://map.baidu.com/subway/%E6%B7%B1%E5%9C%B3/@12697919.69,2560977.31,12z/ccode%3D340%26cname%3D%25E6%25B7%25B1%25E5%259C%25B3" },
            { "text": "北京地铁", "link": "http://map.baidu.com/subways/index.html?c=beijing" },
            { "text": "上海地铁", "link": "https://map.baidu.com/subway/%E4%B8%8A%E6%B5%B7/@13523265.31,3641114.64,12z/ccode%3D289%26cname%3D%25E4%25B8%258A%25E6%25B5%25B7" },
            { "text": "广州地铁", "link": "https://map.baidu.com/subway/%E5%B9%BF%E5%B7%9E/@12609384.2,2631450.58,12z/ccode%3D257%26cname%3D%25E5%25B9%25BF%25E5%25B7%259E" },
            { "text": "12306", "link": "http://www.12306.cn/" }, 
            { "text": "8684公交查询", "link": "http://www.8684.cn/" }, 
            { "text": "交通部", "link": "http://www.mot.gov.cn/" }
        ]
    },
    {
        "text": "航空 ", "data": [
            { "text": "中国国际航空", "link": "http://www.airchina.com.cn/" },
            { "text": "南方航空", "link": "http://www.csair.com/" },
            { "text": "海南航空", "link": "http://www.hnair.com/" },
            { "text": "东方航空", "link": "http://www.ceair.com/" },
            { "text": "春秋航空", "link": "http://www.china-sss.com/" },
            { "text": "四川航空", "link": "http://www.scal.com.cn/" },
            { "text": "厦门航空", "link": "https://www.xiamenair.com/zh-cn/" },
            { "text": "深圳航空", "link": "http://www.shenzhenair.com/" },
            { "text": "民航资源网", "link": "http://www.carnoc.com/" },
            { "text": "吉祥航空", "link": "http://www.juneyaoair.com/" },
            { "text": "首都航空", "link": "http://www.jdair.net/" },
            { "text": "飞常准航班动态", "link": "http://www.variflight.com/" },
            { "text": "香港航空", "link": "http://www.hongkongairlines.com" }
        ]
    },
    {
        "text": "各地铁路 ", "data": [
            { "text": "中国铁道论坛", "link": "http://bbs.railcn.net" },
            { "text": "南昌铁路局", "link": "http://www.nctlj.com.cn/" },
            { "text": "铁友网", "link": "http://www.tieyou.com/" },
            { "text": "国家铁路局", "link": "http://www.nra.gov.cn/" }
        ]
    },
    {
        "text": "租车/拼车", "data": [
            { "text": "神州租车", "link": "http://www.zuche.com/" },
            { "text": "一嗨租车", "link": "http://www.1hai.cn/" },
            { "text": "至尊租车", "link": "http://www.top1.cn/" },
            { "text": "58同城拼车", "link": "http://www.58.com/pinche/" },
            { "text": "赶集网拼车", "link": "http://www.ganji.com/pincheshangxiaban/" },
            { "text": "滴滴出行", "link": "http://www.xiaojukeji.com/" },
            { "text": "租租车", "link": "http://www.zuzuche.com" }
        ]
    }
]