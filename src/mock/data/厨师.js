export const hotlinks = [
    {
        "text": "下厨房",
        "link": "https://www.xiachufang.com/"
    },
    {
        "text": "美食杰",
        "link": "https://www.meishij.net/"
    },
    {
        "text": "烘焙帮",
        "link": "https://www.hongbeibang.com/"
    }
];

export default [
    {
        "text": "菜谱",
        "data": [
            {
                "text": "美食杰",
                "link": "https://www.meishij.net/"
            },
            {
                "text": "下厨房",
                "link": "https://www.xiachufang.com/"
            },
            {
                "text": "美食天下",
                "link": "https://www.meishichina.com/"
            },
            {
                "text": "豆果美食-会做饭很酷",
                "link": "https://www.douguo.com/"
            },
            {
                "text": "香哈菜谱-菜谱大全",
                "link": "https://www.xiangha.com/"
            },
            {
                "text": "天天菜谱",
                "link": "https://www.tiantiancaipu.com/"
            },
            {
                "text": "大厨艺",
                "link": "http://www.dachuyi.com/"
            },
            {
                "text": "食谱家常菜",
                "link": "http://www.aaeta.com/"
            },
            {
                "text": "掌厨-用心做顿好饭",
                "link": "http://h5.izhangchu.com/"
            },
            {
                "text": "烘焙帮",
                "link": "https://www.hongbeibang.com/"
            }
        ]
    }
]