export const hotlinks = [
    {
        "text": "Gitee GVP 推荐项目",
        "link": "https://toscode.gitee.com/explore/all"
    },
    {
        "text": "快科技",
        "link": "http://www.mydrivers.com/",
        "tooltip": "快科技(驱动之家旗下媒体)--科技改变未来"
    },
    {
        "text": "中文业界资讯站",
        "link": "https://www.cnbeta.com/",
        "tooltip": "中文业界资讯站"
    },
    {
        "text": "创业邦  -  创业是一种信仰",
        "link": "http://www.cyzone.cn/"
    }
];

export default [
    {
        "text": "项目管理工具",
        "data": [
            {
                "text": "Tracup 项目管理",
                "link": "https://www.tracup.com"
            }
        ]
    },
    {
        "text": "源码托管服务",
        "data": [
            {
                "text": "Gitee",
                "link": "https://gitee.com",
                "tooltip": "基于 Git 的代码托管和研发协作平台"
            },
            {
                "text": "GitHub",
                "link": "https://github.com",
                "tooltip": "全球最大的Git代码托管平台"
            },
            {
                "text": "CODING",
                "link": "https://coding.net/",
                "tooltip": "一站式 DevOps，提升研发效能"
            },
            {
                "text": "GitBook 教程",
                "link": "https://www.gitbook.com/",
                "tooltip": "GitBook 是一个基于 Node.js 的命令行工具，支持 Markdown 语法格式，可以输出 HTML、PDF、eBook 等格式的电子书"
            }
        ]
    },
    {
        "text": "开源卓越仓库",
        "data": [
            {
                "text": "Gitee GVP 推荐项目",
                "link": "https://toscode.gitee.com/explore/all"
            },
            {
                "text": "码云极速下载",
                "link": "https://gitee.com/organizations/mirrors/projects"
            },
            {
                "text": "packagist.org",
                "link": "https://packagist.org/"
            },
            {
                "text": "npmjs.com",
                "link": "https://www.npmjs.com/"
            }
        ]
    },
    {
        "text": "在线流程设计",
        "data": [
            {
                "text": "百度脑图",
                "link": "https://naotu.baidu.com"
            },
            {
                "text": "ProcessOn - 流程图",
                "link": "https://www.processon.com"
            },
            {
                "text": "数字绘 - 流程图",
                "link": "https://www.myshuju.net"
            }
        ]
    },
    {
        "text": "在线原型设计",
        "data": [
            {
                "text": "即时原型",
                "link": "https://www.xiaopiu.com/",
                "tooltip": "即时原型-产品原型设计工具与团队实时协作平台"
            },
            {
                "text": "即时设计",
                "link": "https://js.design/",
                "tooltip": "即时设计 - 可实时写作的专业UI设计工具"
            },
            {
                "text": "蓝湖",
                "link": "https://lanhuapp.com/",
                "tooltip": "蓝湖 - 高效的产品设计协作平台"
            },
            {
                "text": "墨刀",
                "link": "https://modao.cc/",
                "tooltip": "墨刀产品原型工具-在线原型设计与协作平台"
            },
            {
                "text": "摹客集设计协作平台",
                "link": "https://www.mockplus.cn/",
                "tooltip": "摹客，让设计和协作更快更简单"
            },
            {
                "text": "Figma",
                "link": "https://www.figma.com/",
                "tooltip": "Figma 是一个云端编辑的在线 UI 界面设计软件"
            },
            {
                "text": "Pixso",
                "link": "https://pixso.cn/",
                "tooltip": "Pixso 一站式UI设计协作工具"
            }
        ]
    },
    {
        "text": "在线文档管理",
        "data": [
            {
                "text": "ApiPost",
                "link": "https://www.apipost.cn/",
                "tooltip": "ApiPost - API 文档、调试、Mock、测试一体化协作平台"
            },
            {
                "text": "简单云",
                "link": "https://ezone.work/",
                "tooltip": "简单云(ezOne.work)一站式云原生DevOps研发协同平台|研发效能平台|让研发“简单一点”"
            }
        ]
    },
    {
        "text": "在线打包编译",
        "data": [
            {
                "text": "DCloud",
                "link": "https://www.dcloud.io/",
                "tooltip": "DCloud - HBuilder、uni-app、小程序开发、跨平台App、多端框架"
            },
            {
                "text": "APICloud",
                "link": "https://www.apicloud.com/",
                "tooltip": "APICloud 手机APP开发、APP制作技术专家 - 中国领先低代码开发平台"
            }
        ]
    },
    {
        "text": "在线海报设计",
        "data": [
            {
                "text": "易企秀",
                "link": "https://store.eqxiu.com/",
                "tooltip": "H5电子邀请函模板_海报制作模板_图片设计_表单问卷免费制作-易企秀"
            },
            {
                "text": "创客贴",
                "link": "https://www.chuangkit.com/",
                "tooltip": "创客贴-做图做视频必备_会打字就能做设计，商用有版权"
            },
            {
                "text": "稿定设计",
                "link": "https://www.gaoding.com/",
                "tooltip": "稿定设计-做图做视频必备_在线设计神器_海量版权素材模板"
            },
            {
                "text": "Canva可画",
                "link": "https://www.canva.cn/",
                "tooltip": "免费模板在线设计制作 - 模板素材库 - Canva可画"
            }
        ]
    },
    {
        "text": "在线图像处理",
        "data": [
            {
                "text": "TinyPNG 图片压缩",
                "link": "https://tinypng.com/"
            },
            {
                "text": "iLoveImg 修改图片尺寸",
                "link": "https://www.iloveimg.com/resize-image"
            },
            {
                "text": "img2go 修改图片尺寸",
                "link": "https://www.img2go.com/zh"
            },
            {
                "text": "cdkm 文件格式转换",
                "link": "https://cdkm.com/cn/"
            },
            {
                "text": "我拉网图像处理工具",
                "link": "http://pic.55.la/"
            },
            {
                "text": "Office-Converter 在线转换器",
                "link": "https://cn.office-converter.com/"
            },
            {
                "text": "图片编辑器 - 菜鸟教程",
                "link": "https://c.runoob.com/more/imageeditor/"
            },
            {
                "text": "SVG 在线编辑器 - 菜鸟教程",
                "link": "https://c.runoob.com/more/svgeditor/"
            }
        ]
    },
    {
        "text": "在线解析 JSON",
        "data": [
            {
                "text": "JSON 在线解析 - 菜鸟工具",
                "link": "https://c.runoob.com/front-end/53/"
            },
            {
                "text": "SO JSON在线工具",
                "link": "https://www.sojson.com/"
            },
            {
                "text": "在线JSON校验格式化工具（Be JSON）",
                "link": "https://www.bejson.com/"
            },
            {
                "text": "JSON在线解析及格式化验证 - JSON.cn",
                "link": "https://www.json.cn/"
            },
            {
                "text": "Json在线解析格式化工具",
                "link": "https://www.qjson.cn/"
            },
            {
                "text": "JSON在线解析及格式化验证",
                "link": "http://www.json.team/"
            }
        ]
    },
    {
        "text": "在线解析 Markdown",
        "data": [
            {
                "text": "Markdown在线编辑、实时预览",
                "link": "https://tool.ip138.com/markdown/"
            },
            {
                "text": "小书匠markdown编辑器",
                "link": "http://markdown.xiaoshujiang.com/"
            },
            {
                "text": "Cmd Markdown 编辑阅读器",
                "link": "https://www.zybuluo.com/mdeditor"
            },
            {
                "text": "MaHua 在线markdown编辑器",
                "link": "http://mahua.jser.me/"
            }
        ]
    },
    {
        "text": "在线解析 XML",
        "data": [
            {
                "text": "XML格式化、压缩",
                "link": "https://www.qjson.cn/Tools/XmlFormat/"
            }
        ]
    },
    {
        "text": "在线调试代码",
        "data": [
            {
                "text": "jsbin.com",
                "link": "https://jsbin.com/?html,js,output"
            },
            {
                "text": "jsrun.net",
                "link": "http://jsrun.net/new"
            },
            {
                "text": "RunJS在线编辑器",
                "link": "https://www.bejson.com/ui/runjs/"
            },
            {
                "text": "前端在线运行-奇妙工具箱",
                "link": "https://www.qjson.cn/Tools/run/"
            },
            {
                "text": "在线代码编辑器",
                "link": "https://www.jq22.com/webide/"
            },
            {
                "text": "代码在线运行 - 在线工具",
                "link": "https://tool.lu/coderunner"
            },
            {
                "text": "后端代码在线运行 - 菜鸟工具",
                "link": "https://c.runoob.com/compile/1/"
            }
        ]
    },
    {
        "text": "在线可视化编辑器",
        "data": [
            {
                "text": "ueditor在线代码编辑器",
                "link": "https://www.bejson.com/ui/ueditor/"
            },
            {
                "text": "ueditor在线代码编辑器",
                "link": "https://www.bejson.com/ui/ueditor/"
            },
            {
                "text": "在线HTML编辑器 - KindEditor",
                "link": "http://kindeditor.net/demo.php"
            },
            {
                "text": "TinyMCE Demo",
                "link": "https://www.tiny.cloud/docs/demo/full-featured/"
            },
            {
                "text": "KEditor 5 Demo",
                "link": "https://ckeditor.com/ckeditor-5/demo/#document"
            }
        ]
    },
    {
        "text": "在线模拟请求",
        "data": [
            {
                "text": "专业版模拟HTTP请求工具-BeJSON.com",
                "link": "https://www.bejson.com/network/profession_request_tools/"
            },
            {
                "text": "HTTP请求模拟 - SO JSON",
                "link": "https://www.sojson.com/http/test.html"
            },
            {
                "text": "HTTP接口测试工具-在线模拟HTTP请求 - 站长工具",
                "link": "http://tool.chinaz.com/tools/httptest.aspx"
            }
        ]
    },
    {
        "text": "在线数据转换",
        "data": [
            {
                "text": "在线查看时间计划",
                "link": "https://tool.lu/crontab"
            }
        ]
    },
    {
        "text": "日期时间",
        "data": [
            {
                "text": "日历网",
                "link": "https://www.rili.com.cn/"
            },
            {
                "text": "天气万年历",
                "link": "https://wannianli.tianqi.com/"
            }
        ]
    },
    {
        "text": "云工具箱",
        "data": [
            {
                "text": "菜鸟工具 - 不止于工具",
                "link": "https://c.runoob.com/"
            },
            {
                "text": "脚本之家在线工具",
                "link": "http://tools.jb51.net/"
            },
            {
                "text": "我拉网在线工具",
                "link": "http://pic.55.la/"
            },
            {
                "text": "在线工具 - 你的工具箱",
                "link": "https://tool.lu/"
            },
            {
                "text": "Box3 开发工具箱",
                "link": "http://www.box3.cn/index.html"
            },
            {
                "text":"开源中国在线工具",
                "link":"https://tool.oschina.net/"
            },
            {
                "text":"工具狗",
                "link":"https://www.gongjugou.com/"
            },
            {
                "text":"万能在线工具箱",
                "link":"https://www.fulimama.com/"
            },
            {
                "text": "简捷工具",
                "link": "http://www.shulijp.com/"
            },
            {
                "text": "精准云工具合集",
                "link": "https://jingzhunyun.com/"
            },
            {
                "text": "爱资料在线工具",
                "link": "https://www.toolnb.com/"
            }
        ]
    },
    {
        "text": "业界资讯",
        "data": [
            {
                "text": "快科技",
                "link": "http://www.mydrivers.com/",
                "tooltip": "快科技(驱动之家旗下媒体)--科技改变未来"
            },
            {
                "text": "中文业界资讯站",
                "link": "https://www.cnbeta.com/",
                "tooltip": "中文业界资讯站"
            },
            {
                "text": "干货集中营",
                "link": "http://gank.io/",
                "tooltip": "每日分享妹子图 和 技术干货，还有供大家中午休息的休闲视频"
            },
            {
                "text": "开发者头条",
                "link": "https://toutiao.io/",
                "tooltip": "程序员分享平台 | 程序员必装的App | 技术极客的头条新闻"
            },
            {
                "text": "IT时代网-解读信息时代的商业变革",
                "link": "http://www.ittime.com.cn/"
            },
            {
                "text": "艾瑞网-互联网数据资讯聚合平台",
                "link": "http://www.iresearch.cn/"
            },
            {
                "text": "TMT观察网_独特视角观察TMT行业",
                "link": "http://www.looktmt.com/"
            },
            {
                "text": "虎嗅网 - 不错过互联网的每个重要时刻。",
                "link": "https://www.huxiu.com/"
            },
            {
                "text": "咕噜网 - 独立、有深度的原创型科技媒体",
                "link": "http://www.cngulu.com/"
            },
            {
                "text": "钛媒体_网罗天下创新事",
                "link": "http://www.tmtpost.com/"
            },
            {
                "text": "互联网的一些事-最有料的互联网资讯!",
                "link": "http://www.yixieshi.com/"
            },
            {
                "text": "36氪_为创业者提供最好的产品和服务",
                "link": "http://36kr.com/"
            },
            {
                "text": "物联网的那些事 Totiot-关注物联网前沿资讯，交流智能化体验心得！",
                "link": "http://www.totiot.com/"
            },
            {
                "text": "InfoQ - 促进软件开发领域知识与创新的传播",
                "link": "http://www.infoq.com/cn/"
            },
            {
                "text": "牛犊网 - 关注创业领域和职场励志的媒体博客",
                "link": "http://www.newdur.com/"
            },
            {
                "text": "赛迪网",
                "link": "http://www.ccidnet.com"
            },
            {
                "text": "51CTO",
                "link": "http://www.51cto.com/"
            },
            {
                "text": "cnBeta",
                "link": "https://www.cnbeta.com/"
            },
            {
                "text": "《电脑爱好者》",
                "link": "http://www.cfan.com.cn"
            },
            {
                "text": "互联网周刊",
                "link": "http://www.ciweek.com/"
            },
            {
                "text": "比特网",
                "link": "http://www.chinabyte.com"
            },
            {
                "text": "IT时代网",
                "link": "https://www.ittime.com.cn/"
            },
            {
                "text": "速途网",
                "link": "http://www.sootoo.com/"
            },
            {
                "text": "趋势网",
                "link": "http://www.mtrend.net.cn/"
            },
            {
                "text": "CSDN",
                "link": "https://www.csdn.net/"
            }
        ]
    },
    {
        "text": "开发者社区",
        "data": [
            {
                "text": "开源中国",
                "link": "https://www.oschina.net/",
                "tooltip": "中文开源技术交流社区"
            },
            {
                "text": "博客园",
                "link": "https://www.cnblogs.com/",
                "tooltip": "开发者的网上家园"
            },
            {
                "text": "阿里云开发者社区",
                "link": "https://developer.aliyun.com/",
                "tooltip": "阿里云官网开发者社区"
            },
            {
                "text": "ImportNew - 专注Java & Android 技术分享",
                "link": "http://www.importnew.com/"
            },
            {
                "text": "酷壳 – 享受编程和技术所带来的快乐",
                "link": "http://coolshell.cn/"
            },
            {
                "text": "知乎 - 与世界分享你的知识、经验和见解",
                "link": "https://www.zhihu.com/"
            },
            {
                "text": "博客园 - 开发者的网上家园",
                "link": "http://www.cnblogs.com/"
            },
            {
                "text": "伯乐在线 - 做最专业的IT互联网职业社区。",
                "link": "http://www.jobbole.com/"
            },
            {
                "text": "卧龙阁-看见,每一个公司的每个人！公司评价,面经,工资待遇,员工工作心情这里都有",
                "link": "http://www.wolonge.com/"
            },
            {
                "text": "简书 - 找回文字的力量。",
                "link": "http://www.jianshu.com/"
            }
        ]
    },
    {
        "text": "名人博客",
        "data": [
            {
                "text": "廖雪峰的官方网站",
                "link": "https://www.liaoxuefeng.com"
            },
            {
                "text": "阮一峰的个人网站",
                "link": "http://www.ruanyifeng.com/"
            },
            {
                "text": "ThinkPHP 网址导航",
                "link": "http://sites.thinkphp.cn/"
            },
            {
                "text": "京东科技开发者",
                "link": "https://www.infoq.cn/u/jdt-developers/publish"
            },
            {
                "text": "鸟哥笔记-移动互联网第一干货平台！",
                "link": "http://www.niaogebiji.com/"
            },
        ]
    },
    {
        "text": "开发者期刊",
        "data": [
            {
                "text": "科技爱好者周刊 - 阮一峰",
                "link": "http://www.ruanyifeng.com/blog/"
            },
            {
                "text": "ThinkPHP开发者周刊 - ThinkPHP",
                "link": "https://weekly.thinkphp.cn/"
            },
            {
                "text": "技术、产品、营销 - 码农IO官方博客",
                "link": "http://blog.manong.io/"
            },
            {
                "text": "外刊IT评论 | 程序员工作、生活、爱情、事业",
                "link": "http://www.vaikan.com/"
            },
            {
                "text": "湾区日报：关注创业与技术，每天推送5篇优质英文文章",
                "link": "https://wanqu.co/"
            }
        ]
    },
    {
        "text": "项目外包",
        "data": [
            {
                "text": "智城外包网 - 最专业的软件外包网和项目外包、项目开发、人力外派、短期招聘、人力资源交易平台",
                "link": "http://www.taskcity.com/"
            }
        ]
    },
    {
        "text": "创客交流",
        "data": [
            {
                "text": "创天下——中国创业者与投资人第一社交媒体平台",
                "link": "http://www.chuangtianxia.cn/"
            },
            {
                "text": "MindStore - 在这里找到最好的产品与想法",
                "link": "http://mindstore.io/"
            },
            {
                "text": "新芽NewSeed：离资本最近的创业服务平台！",
                "link": "http://newseed.pedaily.cn/"
            },
            {
                "text": "创业邦  -  创业是一种信仰",
                "link": "http://www.cyzone.cn/"
            }
        ]
    }
]