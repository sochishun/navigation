export const hotlinks = [
    {
        "text": "36氪",
        "link": "http://www.36kr.com/"
    },
    {
        "text": "豆瓣",
        "link": "http://www.douban.com/"
    },
    {
        "text": "知乎",
        "link": "http://www.zhihu.com/"
    },
    {
        "text": "国外网站大全",
        "link": "http://www.world68.com/",
        "tooltip": "国外网站大全即世界各国网址大全，国外网站大全收录100多个国家知名网站"
    },
    {
        "text": "创业神器导航",
        "link": "https://hao.logosc.cn/",
        "tooltip": "分享最酷的互联网工具和创业资源！"
    },
    {
        "text": "199IT大数据导航",
        "link": "http://hao.199it.com/",
        "tooltip": "大数据导航，以大数据产业为主，大数据工具为辅，给用户提供一个更加快速找到大数据相关的工具平台。"
    },
    {
        "text": "中国最大名录研究开发组织",
        "link": "https://www.emagecompany.com/",
        "tooltip": "行业名录、省区名录、城市数据、国际名录、进出口业、黄页号簿、展商名录、免费资源等。"
    },
];

export default [
    {
        "text": "网站导航",
        "data": [
            {
                "text": "国际网址导航",
                "link": "http://hao.guojiz.com/",
                "tooltip": "一个主页整个世界"
            },
            {
                "text": "国外网站大全",
                "link": "http://www.world68.com/",
                "tooltip": "国外网站大全即世界各国网址大全，国外网站大全收录100多个国家知名网站"
            },
            {
                "text": "小森林导航",
                "link": "http://www.xsldh6.com/",
                "tooltip": "小森林导航-收录600多实用网站，整合200多个搜索引擎"
            },
            {
                "text": "牛导航",
                "link": "http://www.ziliao6.com/",
                "tooltip": "让你拥有全世界-实用工具导航-牛资料导航"
            },
            {
                "text": "创业神器导航",
                "link": "https://hao.logosc.cn/",
                "tooltip": "分享最酷的互联网工具和创业资源！"
            },
            {
                "text": "199IT大数据导航",
                "link": "http://hao.199it.com/",
                "tooltip": "大数据导航，以大数据产业为主，大数据工具为辅，给用户提供一个更加快速找到大数据相关的工具平台。"
            },
            {
                "text": "中国最大名录研究开发组织",
                "link": "https://www.emagecompany.com/",
                "tooltip": "行业名录、省区名录、城市数据、国际名录、进出口业、黄页号簿、展商名录、免费资源等。"
            },
            {
                "text": "KIM主页",
                "link": "https://kim.plopco.com/",
                "tooltip": "极简实用的个人主页"
            }
        ]
    },
    {
        "text": "电子邮箱",
        "data": [
            {
                "text": "网易163邮箱",
                "link": "https://mail.163.com/ "
            },
            {
                "text": "139手机邮箱",
                "link": "http://mail.10086.cn/"
            },
            {
                "text": "搜狐邮箱",
                "link": "https://mail.sohu.com/fe/#/login"
            },
            {
                "text": "QQ邮箱",
                "link": "https://mail.qq.com/"
            },
            {
                "text": "189邮箱",
                "link": "https://webmail30.189.cn/w2/"
            },
            {
                "text": "网易126邮箱",
                "link": "https://mail.126.com/"
            },
            {
                "text": "TOM邮箱",
                "link": "http://mail.tom.com/"
            },
            {
                "text": "88完美邮箱",
                "link": "https://www.88.com/?source=.hao123"
            },
            {
                "text": "新浪邮箱",
                "link": "http://mail.sina.com.cn/"
            },
            {
                "text": "outlook邮箱",
                "link": "https://outlook.live.com/owa/"
            },
            {
                "text": "阿里邮箱",
                "link": "https://mail.aliyun.com/"
            },
            {
                "text": "foxmail",
                "link": "http://www.foxmail.com/"
            },
            {
                "text": "2980邮箱",
                "link": "https://www.2980.com/"
            },
            {
                "text": "21CN邮箱",
                "link": "http://mail.21cn.com/"
            },
            {
                "text": "188财富邮",
                "link": "http://www.188.com/"
            },
            {
                "text": "网易Yeah邮箱",
                "link": "http://www.yeah.net/"
            },
            {
                "text": "263邮箱",
                "link": "https://www.263.net/"
            },
            {
                "text": "沃邮箱",
                "link": "https://mail.wo.cn/"
            },
            {
                "text": "网易企业邮箱",
                "link": "https://qiye.163.com/"
            },
            {
                "text": "网易VIP邮箱",
                "link": "http://vip.163.com/"
            },
            {
                "text": "腾讯企业邮箱",
                "link": "https://exmail.qq.com/"
            },
            {
                "text": "移动139邮箱",
                "link": "http://mail.10086.cn/"
            },
            {
                "text": "163邮箱",
                "link": "http://mail.163.com"
            },
            {
                "text": "搜狐邮箱",
                "link": "http://mail.sohu.com/"
            },
            {
                "text": "电信189邮箱",
                "link": "http://webmail30.189.cn/w2/"
            },
            {
                "text": "Yeah.net邮箱",
                "link": "http://www.yeah.net"
            },
            {
                "text": "263企业邮箱",
                "link": "https://enterprisemail.263.net/"
            },
            {
                "text": "126邮箱",
                "link": "http://www.126.com"
            },
            {
                "text": "QQ邮箱",
                "link": "http://mail.qq.com"
            },
            {
                "text": "21CN邮箱",
                "link": "http://mail.21cn.com"
            },
            {
                "text": "新浪邮箱",
                "link": "http://mail.sina.com.cn"
            },
            {
                "text": "Outlook",
                "link": "http://www.outlook.com/"
            },
            {
                "text": "联通沃邮箱",
                "link": "http://mail.wo.cn/"
            }
        ]
    },
    {
        "text": "网络硬盘",
        "data": [
            {
                "text": "115网盘",
                "link": "http://www.115.com/"
            },
            {
                "text": "乐视云盘",
                "link": "http://mobile.le.com/lecloud/"
            },
            {
                "text": "腾讯微云",
                "link": "http://www.weiyun.com/index.html"
            },
            {
                "text": "百度网盘",
                "link": "https://pan.baidu.com/"
            },
            {
                "text": "微盘",
                "link": "http://vdisk.weibo.com/"
            },
            {
                "text": "天翼云",
                "link": "http://cloud.189.cn"
            },
            {
                "text": "坚果云",
                "link": "https://www.jianguoyun.com/"
            }
        ]
    },
    {
        "text": "云盘相册",
        "data": [
            {
                "text": "360安全云盘",
                "link": "https://yunpan.360.cn/?src=hao360"
            },
            {
                "text": "百度网盘",
                "link": "https://pan.baidu.com/"
            },
            {
                "text": "115网盘",
                "link": "http://115.com/"
            },
            {
                "text": "微盘",
                "link": "https://vdisk.weibo.com/"
            },
            {
                "text": "腾讯微云",
                "link": "http://www.weiyun.com/"
            },
            {
                "text": "天翼云盘",
                "link": "http://cloud.189.cn/"
            }
        ]
    },
    {
        "text": "奇趣搞笑",
        "data": [
            {
                "text": "糗事百科",
                "link": "http://www.qiushibaike.com/"
            },
            {
                "text": "德云相声网",
                "link": "http://www.guodegang.org/"
            },
            {
                "text": "煎蛋",
                "link": "http://jandan.net/"
            }
        ]
    },
    {
        "text": "新鲜科技",
        "data": [
            {
                "text": "果壳网",
                "link": "http://www.guokr.com/"
            },
            {
                "text": "36氪",
                "link": "http://www.36kr.com/"
            },
            {
                "text": "互联网的一些事",
                "link": "http://www.yixieshi.com/"
            },
            {
                "text": "雷锋网",
                "link": "http://www.leiphone.com/"
            },
            {
                "text": "新浪众测",
                "link": "http://zhongce.sina.com.cn/"
            },
            {
                "text": "黑猫投诉",
                "link": "http://tousu.sina.com.cn/"
            }
        ]
    },
    {
        "text": "爱手工",
        "data": [
            {
                "text": "编织人生",
                "link": "http://www.bianzhirensheng.com/"
            }
        ]
    },
    {
        "text": "新潮社区",
        "data": [
            {
                "text": "豆瓣",
                "link": "http://www.douban.com/"
            },
            {
                "text": "知乎",
                "link": "http://www.zhihu.com/"
            },
            {
                "text": "壹心理",
                "link": "http://www.xinli001.com/"
            },
            {
                "text": "龙腾网",
                "link": "http://www.ltaaa.com/"
            },
            {
                "text": "好知网",
                "link": "http://www.howzhi.com/"
            },
            {
                "text": "钓鱼网",
                "link": "http://www.diaoyu123.com/"
            },
            {
                "text": "测智网",
                "link": "http://www.iqeq.com.cn/"
            },
            {
                "text": "360文库",
                "link": "https://wenku.so.com/?src=123fl"
            },
            {
                "text": "360百科",
                "link": "https://baike.so.com/?src=123fl"
            },
            {
                "text": "印象笔记",
                "link": "https://www.yinxiang.com/"
            },
            {
                "text": "简书",
                "link": "http://www.jianshu.com/"
            }
        ]
    },
    {
        "text": "_购物",
        "data": [
            {
                "text": "京东",
                "link": "https://u.jd.com/qXnWeR"
            },
            {
                "text": "爱淘宝特卖",
                "link": "https://redirect.simba.taobao.com/rd?c=un&w=bd&f=https%3A%2F%2Fai.taobao.com%2F%3Fpid%3Dmm_26632331_7304251_107180250258&k=3e8e7ae3f29b7029&p=mm_26632331_7304251_107180250258"
            },
            {
                "text": "阿里1688",
                "link": "https://p4psearch.1688.com/hamlet.html?scene=6&cosite=sogoudaohang&location=mingzhan"
            },
            {
                "text": "天猫",
                "link": "https://redirect.simba.taobao.com/rd?c=un&w=bd&f=https%3A%2F%2Fs.click.taobao.com%2Ft%3Funion_lens%3DlensId%253AOPT%25401615280124%25402104be69_0782_1781632b34a_ced8%254001%253BeventPageId%253A8655681%26e%3Dm%253D2%2526s%253Dv%252Bp6OF1HixccQipKwQzePCperVdZeJviePMclkcdtjxyINtkUhsv0MS6Blk41Mgmiybki%252FObwflD415GGSsrJ4YxhNUZ2i3GTlT4SLzYDiBT2M421%252BABgTvflh4%252Fhqj89CGjsatFbg%252FkxFiXT%252FI5kZuVJ2zJE2c0ACFpTaudtkEaHks2%252FfPFu3EqY%252Bakgpmw%26pid%3Dmm_26632331_7304251_24130794&k=67a22f436b17a341&p=mm_26632331_7304251_24130794"
            },
            {
                "text": "聚划算",
                "link": "https://mos.m.taobao.com/union/jhsjx2020?pid=mm_14626936_8346309_57108144"
            },
            {
                "text": "拼多多",
                "link": "https://p.pinduoduo.com/jP1xMoSG"
            },
            {
                "text": "天猫超市",
                "link": "https://s.click.taobao.com/YqqD7cu"
            },
            {
                "text": "淘宝红包",
                "link": "https://ai.taobao.com/?pid=mm_14626936_8346309_133778679"
            }
        ]
    },
    {
        "text": "视频",
        "data": [
            {
                "text": "抖音短视频",
                "link": "https://www.douyin.com/?ug_source=sgdh"
            },
            {
                "text": "腾讯视频",
                "link": "https://v.qq.com/"
            },
            {
                "text": "爱奇艺",
                "link": "http://www.iqiyi.com/"
            },
            {
                "text": "优酷",
                "link": "https://www.youku.com/"
            },
            {
                "text": "哔哩哔哩",
                "link": "https://www.bilibili.com/"
            },
            {
                "text": "新游记",
                "link": "https://v.qq.com/x/cover/mzc00200m8ug6as.html"
            },
            {
                "text": "灿烂的前行",
                "link": "https://v.qq.com/x/cover/mzc00200h2j2rua.html"
            }
        ]
    },
    {
        "text": "_休闲",
        "data": [
            {
                "text": "4399小游戏",
                "link": "http://www.4399.com/?sogou"
            },
            {
                "text": "2048",
                "link": "https://play2048.co/"
            },
            {
                "text": "页游大全",
                "link": "https://d.mytanwan.com/flash/xryh/index.html?agent_id=1839&placeid=80799&cplaceid=151387298.5682861217.248424719921&type=8&game_id=2395&aid=xryh&rand=1&ref=https://baidu.com/&bd_vid=11337589128684108379"
            },
            {
                "text": "电竞经理",
                "link": "https://gamer.qq.com/v2/cloudgame/game/95678?ichannel=qqllq0Fqqllq1"
            },
            {
                "text": "重返帝国",
                "link": "https://guyu.gamer.qq.com/lbact/a20220811lbsicb0/wcjq.html"
            },
            {
                "text": "指尖领主",
                "link": "https://gamer.qq.com/v2/cloudgame/game/96079?ichannel=qqllq0Fqqllq1"
            },
            {
                "text": "不良人3",
                "link": "https://gamer.qq.com/v2/cloudgame/game/96215?ichannel=qqllq0Fqqllq1"
            },
            {
                "text": "时空猎人3",
                "link": "https://gamer.qq.com/v2/cloudgame/game/96134?ichannel=qqllq0Fqqllq1"
            }
        ]
    },
    {
        "text": "在线听歌",
        "data": [
            {
                "text": "酷狗音乐",
                "link": "http://www.kugou.com/"
            },
            {
                "text": "一听音乐网",
                "link": "http://www.1ting.com/"
            },
            {
                "text": "酷我音乐",
                "link": "http://www.kuwo.cn/"
            },
            {
                "text": "九酷音乐网",
                "link": "http://www.9ku.com/"
            },
            {
                "text": "QQ音乐",
                "link": "http://y.qq.com/"
            },
            {
                "text": "网易云音乐",
                "link": "http://music.163.com/"
            },
            {
                "text": "365音乐网",
                "link": "http://www.yue365.com/"
            },
            {
                "text": "yymp3音乐网",
                "link": "http://www.yymp3.com/"
            },
            {
                "text": "优酷音乐",
                "link": "http://music.youku.com/"
            },
            {
                "text": "乐视音乐",
                "link": "http://music.letv.com"
            },
            {
                "text": "搜狐音乐频道",
                "link": "http://tv.sohu.com/music/"
            },
            {
                "text": "5ND音乐网",
                "link": "http://www.5nd.com/"
            },
            {
                "text": "叮当音乐网",
                "link": "http://www.mtv123.com/"
            },
            {
                "text": "音乐巴士",
                "link": "http://www.yy8844.cn/"
            },
            {
                "text": "豆瓣音乐",
                "link": "https://music.douban.com/"
            },
            {
                "text": "千千音乐",
                "link": "https://music.taihe.com/"
            },
            {
                "text": "咪咕音乐",
                "link": "https://www.migu.cn/music.html"
            },
            {
                "text": "爱奇艺音乐",
                "link": "https://music.iqiyi.com/"
            },
            {
                "text": "腾讯视频音乐",
                "link": "https://v.qq.com/channel/music"
            },
            {
                "text": "网易音乐资讯",
                "link": "https://ent.163.com/music/"
            },
            {
                "text": "喜马拉雅音乐频道",
                "link": "https://www.ximalaya.com/channel/19/"
            },
            {
                "text": "中国原创音乐基地",
                "link": "http://5sing.kugou.com/"
            }
        ]
    },
    {
        "text": "音乐APP",
        "data": [
            {
                "text": "酷狗音乐",
                "link": "http://download.kugou.com/"
            },
            {
                "text": "QQ音乐",
                "link": "http://y.qq.com/#type=down&p=index.html"
            },
            {
                "text": "酷我音乐",
                "link": "http://www.kuwo.cn/down"
            },
            {
                "text": "网易云音乐",
                "link": "http://music.163.com/#/download"
            },
            {
                "text": "唱吧",
                "link": "http://changba.com/"
            },
            {
                "text": "全民K歌",
                "link": "http://kg.qq.com/"
            },
            {
                "text": "酷我K歌",
                "link": "http://k.kuwo.cn/"
            }
        ]
    },
    {
        "text": "DJ舞曲",
        "data": [
            {
                "text": "深港DJ俱乐部",
                "link": "http://www.ik123.com/"
            },
            {
                "text": "DJ嗨嗨网",
                "link": "http://www.djkk.com/"
            },
            {
                "text": "清风DJ音乐",
                "link": "http://www.vvvdj.com/"
            }
        ]
    },
    {
        "text": "生活",
        "data": [
            {
                "text": "58招聘",
                "link": "http://jump.luna.58.com/s?spm=b-31580022738699-pe-f-834&ch=123sogou_103"
            },
            {
                "text": "特价二手房",
                "link": "https://www.anjuke.com/?pi=navi-sogou-rmesf"
            },
            {
                "text": "携程旅行",
                "link": "https://vacations.ctrip.com/"
            },
            {
                "text": "中华英才网",
                "link": "http://www.chinahr.com/"
            },
            {
                "text": "BOSS直聘",
                "link": "https://www.zhipin.com/"
            },
            {
                "text": "智联招聘",
                "link": "https://landing.zhaopin.com/register?identity=c"
            },
            {
                "text": "世纪佳缘",
                "link": "https://www.jiayuan.com/"
            },
            {
                "text": "12306",
                "link": "https://www.12306.cn/"
            }
        ]
    },
    {
        "text": "常用",
        "data": [
            {
                "text": "_安居客",
                "link": "https://www.anjuke.com/sale/?pi=navi-sogou-kz"
            },
            {
                "text": "_热门二手房",
                "link": "http://jump.luna.58.com/s?spm=b-31580022738699-pe-f-834&ch=123sogou_101"
            },
            {
                "text": "热门酒店",
                "link": "https://ctrip.com/"
            },
            {
                "text": "163邮箱",
                "link": "http://mail.163.com/"
            },
            {
                "text": "QQ邮箱",
                "link": "http://mail.qq.com/"
            },
            {
                "text": "QQ音乐",
                "link": "https://y.qq.com/"
            },
            {
                "text": " 酷狗音乐",
                "link": "http://www.kugou.com/"
            },
            {
                "text": "酷我音乐",
                "link": "http://www.kuwo.cn/"
            }
        ]
    },
    {
        "text": "新闻",
        "data": [
            {
                "text": "新华网",
                "link": "http://www.news.cn/"
            },
            {
                "text": "腾讯新闻",
                "link": "https://news.qq.com/"
            },
            {
                "text": "网易新闻",
                "link": "https://news.163.com/"
            },
            {
                "text": "新浪新闻",
                "link": "http://news.sina.com.cn/"
            },
            {
                "text": "澎湃新闻",
                "link": "https://www.thepaper.cn/"
            },
            {
                "text": "新闻头条",
                "link": "https://news.qq.com/?fr=sgdh"
            },
            {
                "text": "搜狐新闻",
                "link": "http://news.sohu.com/"
            },
            {
                "text": "娱乐新闻",
                "link": "https://new.qq.com/ch/ent/?fr=sgdh"
            }
        ]
    },
    {
        "text": "军事",
        "data": [
            {
                "text": "腾讯军事",
                "link": "https://new.qq.com/ch/milite/"
            },
            {
                "text": "军事前沿",
                "link": "http://mil.qianyan001.com/"
            },
            {
                "text": "米尔军情",
                "link": "http://military.miercn.com/"
            },
            {
                "text": "凤凰军事",
                "link": "https://mil.ifeng.com"
            },
            {
                "text": "搜狐军事",
                "link": "https://mil.sohu.com/"
            },
            {
                "text": "网易军事",
                "link": "https://war.163.com/"
            }
        ]
    },
    {
        "text": "体育",
        "data": [
            {
                "text": "腾讯体育",
                "link": "http://sports.qq.com/"
            },
            {
                "text": "新浪体育",
                "link": "http://sports.sina.com.cn/"
            },
            {
                "text": "虎扑体育",
                "link": "https://www.hupu.com/"
            },
            {
                "text": "凤凰体育",
                "link": "http://sports.ifeng.com/"
            },
            {
                "text": "网易体育",
                "link": "http://sports.163.com/"
            },
            {
                "text": "搜狐体育",
                "link": "https://sports.sohu.com/"
            },
            {
                "text": "新华网体育",
                "link": "http://sports.news.cn/"
            }
        ]
    },
    {
        "text": "汽车",
        "data": [
            {
                "text": "_58二手车",
                "link": "https://www.58.com/?path=ershouche/?spm=u-2crfqvfuy97pnc4f21.pinpai_sogouqq&utm_source"
            },
            {
                "text": "腾讯汽车",
                "link": "http://auto.qq.com/"
            },
            {
                "text": "旅行约车",
                "link": "https://car.ctrip.com/zuche/landing?isList=0#ctm_ref=chp_var_txt"
            },
            {
                "text": "爱卡汽车",
                "link": "https://www.xcar.com.cn/"
            },
            {
                "text": "新浪汽车",
                "link": "http://auto.sina.com.cn/"
            },
            {
                "text": "太平洋汽车",
                "link": "http://www.pcauto.com.cn/"
            },
            {
                "text": "搜狐汽车",
                "link": "https://auto.sohu.com/"
            }
        ]
    },
    {
        "text": "财经",
        "data": [
            {
                "text": "新浪财经",
                "link": "https://finance.sina.com.cn/"
            },
            {
                "text": "_工商银行",
                "link": "http://www.icbc.com.cn/"
            },
            {
                "text": "_建设银行",
                "link": "http://www.ccb.com/"
            },
            {
                "text": "_农业银行",
                "link": "http://www.abchina.com/"
            },
            {
                "text": "_中国银行",
                "link": "https://www.boc.cn/"
            },
            {
                "text": "_招商银行",
                "link": "http://www.cmbchina.com/"
            },
            {
                "text": "东方财富",
                "link": "http://www.eastmoney.com/"
            }
        ]
    }
]