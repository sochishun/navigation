export const hotlinks = [
    {
        "text": "中国政府网",
        "link": "http://www.gov.cn/"
    },
    {
        "text": "外交部",
        "link": "https://www.mfa.gov.cn/"
    },
    {
        "text": "国防部",
        "link": "http://www.mod.gov.cn/"
    },
    {
        "text": "国家发展和改革委员会",
        "link": "http://www.ndrc.gov.cn/"
    },
    {
        "text": "工业和信息化部",
        "link": "http://www.miit.gov.cn/"
    },
    {
        "text": "商务部",
        "link": "http://www.mofcom.gov.cn/"
    },
    {
        "text": "国家统计局",
        "link": "http://www.stats.gov.cn/"
    },
];

export default [
    {
        "text": "国家机构",
        "data": [
            {
                "text": "中国政府网",
                "link": "http://www.gov.cn/"
            },
            {
                "text": "全国人大",
                "link": "http://www.npc.gov.cn/"
            },
            {
                "text": "全国政协",
                "link": "http://www.cppcc.gov.cn/"
            },
            {
                "text": "国家监察委员会",
                "link": "http://www.ccdi.gov.cn/"
            },
            {
                "text": "最高人民法院",
                "link": "http://www.court.gov.cn/"
            },
            {
                "text": "最高人民检察院",
                "link": "http://www.spp.gov.cn/"
            }
        ]
    },
    {
        "text": "国务院组成部门",
        "data": [
            {
                "text": "外交部",
                "link": "https://www.mfa.gov.cn/"
            },
            {
                "text": "国防部",
                "link": "http://www.mod.gov.cn/"
            },
            {
                "text": "国家发展和改革委员会",
                "link": "http://www.ndrc.gov.cn/"
            },
            {
                "text": "教育部",
                "link": "http://www.moe.gov.cn/"
            },
            {
                "text": "科学技术部",
                "link": "http://www.most.gov.cn/"
            },
            {
                "text": "工业和信息化部",
                "link": "http://www.miit.gov.cn/"
            },
            {
                "text": "国家民族事务委员会",
                "link": "http://www.seac.gov.cn/"
            },
            {
                "text": "公安部",
                "link": "http://www.mps.gov.cn/"
            },
            {
                "text": "民政部",
                "link": "http://www.mca.gov.cn/"
            },
            {
                "text": "司法部",
                "link": "http://www.moj.gov.cn/"
            },
            {
                "text": "财政部",
                "link": "http://www.mof.gov.cn/"
            },
            {
                "text": "人力资源和社会保障部",
                "link": "http://www.mohrss.gov.cn/"
            },
            {
                "text": "自然资源部",
                "link": "http://www.mnr.gov.cn/"
            },
            {
                "text": "生态环境部",
                "link": "http://www.mee.gov.cn/"
            },
            {
                "text": "住房和城乡建设部",
                "link": "http://www.mohurd.gov.cn/"
            },
            {
                "text": "交通运输部",
                "link": "http://www.mot.gov.cn/"
            },
            {
                "text": "水利部",
                "link": "http://www.mwr.gov.cn/"
            },
            {
                "text": "农业农村部",
                "link": "http://www.moa.gov.cn/"
            },
            {
                "text": "商务部",
                "link": "http://www.mofcom.gov.cn/"
            },
            {
                "text": "文化和旅游部",
                "link": "http://www.mct.gov.cn/"
            },
            {
                "text": "国家卫生健康委员会",
                "link": "http://www.nhc.gov.cn/"
            },
            {
                "text": "退役军人事务部",
                "link": "http://www.mva.gov.cn/"
            },
            {
                "text": "应急管理部",
                "link": "http://www.mem.gov.cn/"
            },
            {
                "text": "中国人民银行",
                "link": "http://www.pbc.gov.cn/"
            },
            {
                "text": "审计署",
                "link": "http://www.audit.gov.cn/"
            }
        ]
    },
    {
        "text": "国务院直属特设机构",
        "data": [
            {
                "text": "国有资产监督管理委员会",
                "link": "http://www.sasac.gov.cn/"
            }
        ]
    },
    {
        "text": "国务院直属机构",
        "data": [
            {
                "text": "海关总署",
                "link": "http://www.customs.gov.cn/"
            },
            {
                "text": "国家税务总局",
                "link": "http://www.chinatax.gov.cn/"
            },
            {
                "text": "国家市场监督管理总局",
                "link": "http://www.samr.gov.cn/"
            },
            {
                "text": "国家广播电视总局",
                "link": "http://www.nrta.gov.cn/"
            },
            {
                "text": "国家体育总局",
                "link": "http://www.sport.gov.cn/"
            },
            {
                "text": "国家统计局",
                "link": "http://www.stats.gov.cn/"
            },
            {
                "text": "国家国际发展合作署",
                "link": "http://www.cidca.gov.cn/"
            },
            {
                "text": "国家医疗保障局",
                "link": "http://www.nhsa.gov.cn/"
            },
            {
                "text": "国务院参事室",
                "link": "http://www.counsellor.gov.cn/"
            },
            {
                "text": "国务院机关事务管理局",
                "link": "http://www.ggj.gov.cn/"
            }
        ]
    },
    {
        "text": "国务院办事机构",
        "data": [
            {
                "text": "国务院港澳事务办公室",
                "link": "http://www.hmo.gov.cn/"
            },
            {
                "text": "国务院研究室",
                "link": "http://www.gov.cn/guoqing/2018-06/22/content_5300522.htm"
            }
        ]
    },
    {
        "text": "国务院直属事业单位",
        "data": [
            {
                "text": "新华通讯社",
                "link": "http://203.192.6.89/xhs/"
            },
            {
                "text": "中国科学院",
                "link": "http://www.cas.cn/"
            },
            {
                "text": "中国社会科学院",
                "link": "http://www.cssn.cn/"
            },
            {
                "text": "中国工程院",
                "link": "http://www.cae.cn/cae/html/main/index.html"
            },
            {
                "text": "国务院发展研究中心",
                "link": "http://www.drc.gov.cn/"
            },
            {
                "text": "中央广播电视总台",
                "link": "http://www.cnr.cn/"
            },
            {
                "text": "中国气象局",
                "link": "http://www.cma.gov.cn/"
            },
            {
                "text": "银行保险监督管理委员会",
                "link": "http://www.cbirc.gov.cn/"
            },
            {
                "text": "中国证券监督管理委员会",
                "link": "http://www.csrc.gov.cn/"
            },
            {
                "text": "中共中央党校",
                "link": "https://www.ccps.gov.cn/"
            }
        ]
    },
    {
        "text": "国务院部委管理的国家局",
        "data": [
            {
                "text": "信访局",
                "link": "http://www.gjxfj.gov.cn/"
            },
            {
                "text": "粮食和物资储备局",
                "link": "http://www.lswz.gov.cn/"
            },
            {
                "text": "能源局",
                "link": "http://www.nea.gov.cn/"
            },
            {
                "text": "国防科技工业局",
                "link": "http://www.sastind.gov.cn/"
            },
            {
                "text": "烟草专卖局",
                "link": "http://www.tobacco.gov.cn/"
            },
            {
                "text": "移民管理局",
                "link": "https://www.nia.gov.cn/"
            },
            {
                "text": "林业和草原局",
                "link": "http://www.forestry.gov.cn/"
            },
            {
                "text": "铁路局",
                "link": "http://www.nra.gov.cn/"
            },
            {
                "text": "中国民用航空局",
                "link": "http://www.caac.gov.cn/"
            },
            {
                "text": "国家邮政局",
                "link": "http://www.spb.gov.cn/"
            },
            {
                "text": "文物局",
                "link": "http://www.ncha.gov.cn/"
            },
            {
                "text": "中医药管理局",
                "link": "http://www.satcm.gov.cn/"
            },
            {
                "text": "矿山安全监察局",
                "link": "https://www.chinamine-safety.gov.cn/"
            },
            {
                "text": "国家外汇管理局",
                "link": "http://www.safe.gov.cn/"
            },
            {
                "text": "药品监督管理局",
                "link": "https://www.nmpa.gov.cn/"
            },
            {
                "text": "知识产权局",
                "link": "https://www.cnipa.gov.cn/"
            }
        ]
    },
    {
        "text": "地方政府",
        "data": [
            {
                "text": "北京",
                "link": "http://www.beijing.gov.cn/"
            },
            {
                "text": "天津",
                "link": "http://www.tj.gov.cn/"
            },
            {
                "text": "河北",
                "link": "http://www.hebei.gov.cn/"
            },
            {
                "text": "山西",
                "link": "http://www.shanxi.gov.cn/"
            },
            {
                "text": "内蒙古",
                "link": "http://www.nmg.gov.cn/"
            },
            {
                "text": "辽宁",
                "link": "http://www.ln.gov.cn/"
            },
            {
                "text": "吉林",
                "link": "http://www.jl.gov.cn/"
            },
            {
                "text": "黑龙江",
                "link": "http://www.hlj.gov.cn/"
            },
            {
                "text": "上海",
                "link": "http://www.shanghai.gov.cn/"
            },
            {
                "text": "江苏",
                "link": "http://www.jiangsu.gov.cn/"
            },
            {
                "text": "浙江",
                "link": "http://www.zj.gov.cn/"
            },
            {
                "text": "安徽",
                "link": "http://www.ah.gov.cn/"
            },
            {
                "text": "福建",
                "link": "http://www.fujian.gov.cn/"
            },
            {
                "text": "江西",
                "link": "http://www.jiangxi.gov.cn/"
            },
            {
                "text": "山东",
                "link": "http://www.shandong.gov.cn/"
            },
            {
                "text": "河南",
                "link": "http://www.henan.gov.cn/"
            },
            {
                "text": "湖北",
                "link": "http://www.hubei.gov.cn/"
            },
            {
                "text": "湖南",
                "link": "http://www.hunan.gov.cn/"
            },
            {
                "text": "广东",
                "link": "http://www.gd.gov.cn/"
            },
            {
                "text": "广西",
                "link": "http://www.gxzf.gov.cn/"
            },
            {
                "text": "海南",
                "link": "http://www.hainan.gov.cn/"
            },
            {
                "text": "重庆",
                "link": "http://www.cq.gov.cn/"
            },
            {
                "text": "四川",
                "link": "http://www.sc.gov.cn/"
            },
            {
                "text": "贵州",
                "link": "http://www.guizhou.gov.cn/"
            },
            {
                "text": "云南",
                "link": "http://www.yn.gov.cn/"
            },
            {
                "text": "西藏",
                "link": "http://www.xizang.gov.cn/"
            },
            {
                "text": "陕西",
                "link": "http://www.shaanxi.gov.cn/"
            },
            {
                "text": "甘肃",
                "link": "http://www.gansu.gov.cn/"
            },
            {
                "text": "青海",
                "link": "http://www.qinghai.gov.cn/"
            },
            {
                "text": "宁夏",
                "link": "http://www.nx.gov.cn/"
            },
            {
                "text": "新疆",
                "link": "http://www.xinjiang.gov.cn/"
            },
            {
                "text": "香港",
                "link": "http://www.gov.hk/"
            },
            {
                "text": "澳门",
                "link": "https://www.gov.mo/"
            },
            {
                "text": "台湾",
                "link": "http://www.gwytb.gov.cn/"
            },
            {
                "text": "新疆生产建设兵团",
                "link": "http://www.xjbt.gov.cn/"
            }
        ]
    },
    {
        "text": "驻港澳机构网站",
        "data": [
            {
                "text": "香港中联办",
                "link": "http://www.locpg.gov.cn/"
            },
            {
                "text": "澳门中联办",
                "link": "http://www.zlb.gov.cn/"
            }
        ]
    }
]