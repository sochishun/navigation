export const hotlinks = [
    {
        "text": "财经网",
        "link": "http://www.caijing.com.cn/"
    },
    {
        "text": "和讯网",
        "link": "http://www.hexun.com/"
    },
    {
        "text": "中财网",
        "link": "http://www.cfi.net.cn/"
    },
    {
        "text": "金融界",
        "link": "http://www.jrj.com.cn/"
    },
    {
        "text": "中国产业经济信息网",
        "link": "http://www.cinic.org.cn/cj/"
    },
    {
        "text": "FRM",
        "link": "https://www.gaodun.com/frm"
    },
];

export default [
    {
        "text": "银行官网",
        "data": [
            {
                "text": "工商银行",
                "link": "http://www.icbc.com.cn/"
            },
            {
                "text": "建设银行",
                "link": "http://www.ccb.com/"
            },
            {
                "text": "中国银行",
                "link": "http://www.boc.cn/"
            },
            {
                "text": "交通银行",
                "link": "http://www.bankcomm.com/"
            },
            {
                "text": "农业银行",
                "link": "http://www.abchina.com/"
            },
            {
                "text": "招商银行",
                "link": "http://www.cmbchina.com/"
            },
            {
                "text": "中国光大银行",
                "link": "http://www.cebbank.com/"
            },
            {
                "text": "中国民生银行",
                "link": "http://www.cmbc.com.cn/"
            },
            {
                "text": "平安银行",
                "link": "http://bank.pingan.com/"
            },
            {
                "text": "浦发银行",
                "link": "http://www.spdb.com.cn/"
            },
            {
                "text": "中信银行",
                "link": "http://bank.ecitic.com/"
            },
            {
                "text": "兴业银行",
                "link": "http://www.cib.com.cn/"
            },
            {
                "text": "华夏银行",
                "link": "http://www.hxb.com.cn/"
            },
            {
                "text": "广发银行",
                "link": "http://www.cgbchina.com.cn/"
            }
        ]
    },
    {
        "text": "理财工具",
        "data": [
            {
                "text": "外汇牌价",
                "link": "http://www.boc.cn/sourcedb/whpj/"
            },
            {
                "text": "银行卡查询",
                "link": "http://bank.eastmoney.com/CardList.html"
            },
            {
                "text": "如何办理网银",
                "link": "http://www.chinabank.com.cn/"
            }
        ]
    },
    {
        "text": "地方银行",
        "data": [
            {
                "text": "北京银行",
                "link": "http://www.bankofbeijing.com.cn/"
            },
            {
                "text": "徽商银行",
                "link": "http://www.hsbank.com.cn/"
            },
            {
                "text": "天津银行",
                "link": "http://www.bank-of-tianjin.com.cn/"
            },
            {
                "text": "深圳农村商业银行",
                "link": "http://www.4001961200.com/"
            },
            {
                "text": "上海银行",
                "link": "http://www.bankofshanghai.com/"
            },
            {
                "text": "汉口银行",
                "link": "http://www.hkbchina.com/"
            },
            {
                "text": "南京银行",
                "link": "http://www.njcb.com.cn/"
            },
            {
                "text": "宁波银行",
                "link": "http://www.nbcb.com.cn/"
            },
            {
                "text": "龙江银行",
                "link": "http://www.lj-bank.com/"
            },
            {
                "text": "北京农商银行",
                "link": "http://www.bjrcb.com/"
            },
            {
                "text": "江苏银行",
                "link": "http://www.jsbchina.cn/"
            },
            {
                "text": "广州银行",
                "link": "http://www.gzcb.com.cn/"
            },
            {
                "text": "重庆银行",
                "link": "http://www.cqcbank.com/"
            },
            {
                "text": "浙商银行",
                "link": "http://www.czbank.com/cn/index.shtml"
            },
            {
                "text": "杭州银行",
                "link": "http://www.hzbank.com.cn/"
            },
            {
                "text": "上海农商银行",
                "link": "http://www.srcb.com/"
            },
            {
                "text": "渤海银行",
                "link": "http://www.cbhb.com.cn/"
            },
            {
                "text": "重庆农村商业银行",
                "link": "http://www.cqrcb.com/"
            },
            {
                "text": "广州农村商业银行",
                "link": "http://www.grcbank.com/"
            },
            {
                "text": "青岛银行",
                "link": "http://www.qdccb.com/"
            },
            {
                "text": "成都银行",
                "link": "http://www.bocd.com.cn/"
            },
            {
                "text": "哈尔滨银行",
                "link": "https://www.hrbb.com.cn/"
            },
            {
                "text": "吉林银行",
                "link": "http://www.jlbank.com.cn/"
            },
            {
                "text": "大连银行",
                "link": "http://www.bankofdl.com/"
            },
            {
                "text": "齐鲁银行",
                "link": "http://www.qlbchina.com/"
            },
            {
                "text": "东莞银行",
                "link": "http://www.dongguanbank.cn/"
            },
            {
                "text": "长沙银行",
                "link": "http://www.cscb.cn/"
            },
            {
                "text": "河北银行",
                "link": "http://www.hebbank.com/"
            },
            {
                "text": "东莞农村商业银行",
                "link": "http://www.drcbank.com/"
            },
            {
                "text": "郑州银行",
                "link": "http://www.zzbank.cn/"
            },
            {
                "text": "蒙商银行",
                "link": "http://www.bsb.com.cn/"
            }
        ]
    },
    {
        "text": "银行机构",
        "data": [
            {
                "text": "中国人民银行",
                "link": "http://www.pbc.gov.cn/"
            },
            {
                "text": "银监会",
                "link": "http://www.cbirc.gov.cn/"
            },
            {
                "text": "中国银联",
                "link": "http://cn.unionpay.com/"
            },
            {
                "text": "国家开发银行",
                "link": "http://www.cdb.com.cn/"
            },
            {
                "text": "中国农业发展银行",
                "link": "http://www.adbc.com.cn/"
            },
            {
                "text": "Visa卡官网",
                "link": "http://www.visa.com.cn/index.shtml"
            },
            {
                "text": "JCB卡",
                "link": "http://www.jcbcard.cn/"
            },
            {
                "text": "征信中心",
                "link": "http://www.pbccrc.org.cn/"
            }
        ]
    },
    {
        "text": "电子支付",
        "data": [
            {
                "text": "支付宝",
                "link": "http://www.alipay.com/"
            },
            {
                "text": "百度钱包",
                "link": "https://www.baifubao.com/"
            },
            {
                "text": "快钱",
                "link": "https://www.99bill.com/"
            },
            {
                "text": "银联在线支付",
                "link": "https://online.unionpay.com/"
            },
            {
                "text": "我爱卡",
                "link": "http://www.51credit.com/"
            },
            {
                "text": "移动和包",
                "link": "https://www.cmpay.com/"
            },
            {
                "text": "财付通",
                "link": "https://www.tenpay.com"
            },
            {
                "text": "拉卡拉",
                "link": "http://www.lakala.com/"
            },
            {
                "text": "网易支付",
                "link": "https://epay.163.com/index.jsp"
            },
            {
                "text": "Apple Pay",
                "link": "https://www.apple.com/cn/apple-pay/"
            },
            {
                "text": "京东钱包",
                "link": "https://www.jdpay.com/"
            },
            {
                "text": "微信支付",
                "link": "https://pay.weixin.qq.com/index.php/core/home/login?return_url=%2F"
            }
        ]
    },
    {
        "text": "外资银行",
        "data": [
            {
                "text": "花旗银行",
                "link": "https://www.citibank.com.cn/sim/index.htm"
            },
            {
                "text": "汇丰银行",
                "link": "https://www.hsbc.com.cn/"
            },
            {
                "text": "渣打银行",
                "link": "https://www.sc.com/cn/"
            },
            {
                "text": "东亚银行",
                "link": "http://www.hkbea.com.cn/"
            }
        ]
    },
    {
        "text": "财经网站",
        "data": [
            {
                "text": "东方财富网",
                "link": "http://www.eastmoney.com/"
            },
            {
                "text": "财经网",
                "link": "http://www.caijing.com.cn/"
            },
            {
                "text": "证券之星",
                "link": "http://www.stockstar.com/"
            },
            {
                "text": "和讯网",
                "link": "http://www.hexun.com/"
            },
            {
                "text": "财新网",
                "link": "http://www.caixin.com/"
            },
            {
                "text": "中财网",
                "link": "http://www.cfi.net.cn/"
            },
            {
                "text": "网易财经",
                "link": "http://money.163.com/"
            },
            {
                "text": "金融界",
                "link": "http://www.jrj.com.cn/"
            },
            {
                "text": "全景网",
                "link": "http://www.p5w.net/"
            },
            {
                "text": "中金在线",
                "link": "http://www.cnfol.com/"
            },
            {
                "text": "天天基金网",
                "link": "http://fund.eastmoney.com/"
            },
            {
                "text": "中国证券网",
                "link": "http://www.cnstock.com/"
            },
            {
                "text": "凤凰财经",
                "link": "http://finance.ifeng.com/"
            },
            {
                "text": "同花顺财经",
                "link": "http://www.10jqka.com.cn/"
            },
            {
                "text": "证券时报网",
                "link": "http://www.stcn.com/"
            },
            {
                "text": "中国产业经济信息网",
                "link": "http://www.cinic.org.cn/cj/"
            },
            {
                "text": "中证网",
                "link": "http://www.cs.com.cn/"
            },
            {
                "text": "第一财经",
                "link": "http://www.yicai.com/"
            },
            {
                "text": "搜狐财经",
                "link": "http://business.sohu.com/"
            },
            {
                "text": "中国金融网",
                "link": "http://www.financeun.com/"
            },
            {
                "text": "中国网财经",
                "link": "http://finance.china.com.cn/"
            },
            {
                "text": "九方智投",
                "link": "https://www.9fzt.com/"
            }
        ]
    },
    {
        "text": "理财工具",
        "data": [
            {
                "text": "行情中心",
                "link": "http://quote.eastmoney.com/center/"
            },
            {
                "text": "基金排行",
                "link": "http://fund.eastmoney.com/data/fundranking.html"
            },
            {
                "text": "上证指数行情",
                "link": "http://quote.eastmoney.com/zs000001.html"
            },
            {
                "text": "深证成指行情",
                "link": "http://quote.eastmoney.com/zs399001.html"
            },
            {
                "text": "新浪股票行情",
                "link": "http://finance.sina.com.cn/realstock/"
            },
            {
                "text": "基金净值查询",
                "link": "http://fund.eastmoney.com/fund.html"
            },
            {
                "text": "理财计算器",
                "link": "http://finance.sina.com.cn/calc/index.html"
            },
            {
                "text": "模拟炒股",
                "link": "http://jiaoyi.sina.com.cn/jy/"
            },
            {
                "text": "外汇牌价",
                "link": "http://www.boc.cn/sourcedb/whpj/"
            },
            {
                "text": "黄金行情",
                "link": "http://quote.eastmoney.com/center/hjsc.html"
            },
            {
                "text": "大智慧",
                "link": "http://www.gw.com.cn/download.shtml"
            },
            {
                "text": "同花顺",
                "link": "http://download.10jqka.com.cn/"
            },
            {
                "text": "益盟操盘手",
                "link": "http://product.emoney.cn/"
            },
            {
                "text": "钱龙",
                "link": "http://www.ql18.com.cn/software/prodQijian_1.html"
            }
        ]
    },
    {
        "text": "证券机构",
        "data": [
            {
                "text": "上海证券交易所",
                "link": "http://www.sse.com.cn/"
            },
            {
                "text": "深圳证券交易所",
                "link": "http://www.szse.cn/"
            },
            {
                "text": "国泰君安",
                "link": "http://www.gtja.com/"
            },
            {
                "text": "广发证券",
                "link": "http://new.gf.com.cn/"
            },
            {
                "text": "银河证券",
                "link": "http://www.chinastock.com.cn/"
            },
            {
                "text": "华泰证券",
                "link": "http://www.htsc.com.cn/"
            },
            {
                "text": "光大证券",
                "link": "http://www.ebscn.com/"
            },
            {
                "text": "华西证券",
                "link": "http://www.hx168.com.cn/"
            },
            {
                "text": "方正证券",
                "link": "http://www.foundersc.com/"
            },
            {
                "text": "安信证券",
                "link": "http://www.essence.com.cn/"
            },
            {
                "text": "西部证券",
                "link": "http://www.westsecu.com/"
            },
            {
                "text": "南京证券",
                "link": "http://www.njzq.com.cn/"
            },
            {
                "text": "华安证券",
                "link": "http://www.hazq.com/"
            },
            {
                "text": "中信证券",
                "link": "http://www.cs.ecitic.com/"
            },
            {
                "text": "长江证券",
                "link": "http://www.95579.com/"
            },
            {
                "text": "国海证券",
                "link": "http://www.ghzq.com.cn/"
            },
            {
                "text": "中泰证券",
                "link": "http://www.zts.com.cn/"
            },
            {
                "text": "中国证监会",
                "link": "http://www.csrc.gov.cn/"
            },
            {
                "text": "上海黄金交易所",
                "link": "http://www.sge.com.cn/"
            },
            {
                "text": "上海期货交易所",
                "link": "http://www.shfe.com.cn/"
            },
            {
                "text": "中国证券业协会",
                "link": "http://www.sac.net.cn/"
            }
        ]
    },
    {
        "text": "基金公司",
        "data": [
            {
                "text": "华夏基金",
                "link": "http://www.chinaamc.com/"
            },
            {
                "text": "嘉实基金",
                "link": "http://www.jsfund.cn/"
            },
            {
                "text": "广发基金",
                "link": "http://www.gffunds.com.cn/"
            },
            {
                "text": "招商基金",
                "link": "http://www.cmfchina.com/"
            },
            {
                "text": "建信基金",
                "link": "http://www.ccbfund.cn/"
            },
            {
                "text": "中银基金",
                "link": "http://www.bocim.com/index.html"
            },
            {
                "text": "工银瑞信基金",
                "link": "http://www.icbccs.com.cn/"
            },
            {
                "text": "光大保德信基金",
                "link": "http://www.epf.com.cn/"
            },
            {
                "text": "中邮基金",
                "link": "http://www.postfund.com.cn/"
            },
            {
                "text": "长城基金",
                "link": "http://www.ccfund.com.cn/"
            },
            {
                "text": "汇添富基金",
                "link": "http://www.99fund.com/"
            },
            {
                "text": "国泰基金",
                "link": "http://www.gtfund.com/"
            },
            {
                "text": "华宝兴业",
                "link": "http://www.fsfund.com/"
            },
            {
                "text": "鹏华基金",
                "link": "http://www.phfund.com.cn/"
            },
            {
                "text": "富国基金",
                "link": "http://www.fullgoal.com.cn/"
            },
            {
                "text": "华商基金",
                "link": "http://www.hsfund.com/"
            },
            {
                "text": "博时基金",
                "link": "http://www.bosera.com/"
            },
            {
                "text": "易方达基金",
                "link": "http://www.efunds.com.cn/"
            },
            {
                "text": "上投摩根基金",
                "link": "http://www.51fund.com/"
            },
            {
                "text": "银华基金",
                "link": "http://www.yhfund.com.cn/"
            },
            {
                "text": "泰达宏利",
                "link": "http://www.mfcteda.com/"
            },
            {
                "text": "诺安基金",
                "link": "http://www.lionfund.com.cn/"
            },
            {
                "text": "融通基金",
                "link": "http://www.rtfund.com/"
            },
            {
                "text": "华安基金",
                "link": "http://www.huaan.com.cn/"
            },
            {
                "text": "中海基金",
                "link": "http://www.zhfund.com/"
            },
            {
                "text": "海富通基金",
                "link": "http://www.hftfund.com/"
            },
            {
                "text": "长盛基金",
                "link": "http://www.csfunds.com.cn/"
            },
            {
                "text": "蚂蚁基金",
                "link": "http://www.fund123.cn/"
            }
        ]
    },
    {
        "text": "银行理财",
        "data": [
            {
                "text": "工商银行网上理财",
                "link": "http://www.icbc.com.cn/ICBC/%E7%BD%91%E4%B8%8A%E7%90%86%E8%B4%A2/"
            },
            {
                "text": "中国银行个人理财",
                "link": "http://www.boc.cn/pbservice/pb3/"
            },
            {
                "text": "招行金葵花理财",
                "link": "http://www.cmbchina.com/personal/sunflower/"
            },
            {
                "text": "中国农行理财e站",
                "link": "http://ewealth.abchina.com/"
            },
            {
                "text": "交通银行理财产品",
                "link": "http://www.bankcomm.com/BankCommSite/shtml/jyjr/cn/7226/7266/7281/7282/list.shtml?channelId=7226"
            },
            {
                "text": "中信银行金融门户",
                "link": "http://www.citicbank.com/personal/investment/financinginfo/introduction/"
            },
            {
                "text": "光大银行阳光理财",
                "link": "http://www.cebbank.com/site/gryw/yglc/index.html"
            },
            {
                "text": "兴业银行理财产品",
                "link": "http://wealth.cib.com.cn/retail/onsale/index.html"
            },
            {
                "text": "平安银行理财频道",
                "link": "http://bank.pingan.com/geren/index.shtml"
            },
            {
                "text": "浦发银行理财产品",
                "link": "http://per.spdb.com.cn/bank_financing/financial_product/"
            },
            {
                "text": "民生银行理财产品",
                "link": "http://www.cmbc.com.cn/grkh/lc/index.htm"
            },
            {
                "text": "广发银行个人理财",
                "link": "http://www.cgbchina.com.cn/Channel/13225626"
            },
            {
                "text": "北京银行理财",
                "link": "http://www.bankofbeijing.com.cn/personal/tz-licai.shtml"
            }
        ]
    },
    {
        "text": "金融考试",
        "data": [
            {
                "text": "经济师",
                "link": "https://www.gaodun.com/jjs"
            },
            {
                "text": "中国经济师考试网",
                "link": "https://www.jingjishi.org.cn"
            },
            {
                "text": "FRM",
                "link": "https://www.gaodun.com/frm"
            },
            {
                "text": "中国FRM考试网",
                "link": "https://www.frm.cn/"
            },
            {
                "text": "CFA",
                "link": "https://www.gaodun.com/cfa"
            },
            {
                "text": "中国CFA考试网",
                "link": "http://www.cfa.cn/"
            },
            {
                "text": "银行招聘考试",
                "link": "https://www.gwy.com/yhzp/"
            },
            {
                "text": "农信社招聘考试",
                "link": "https://www.gwy.com/nxs/"
            },
            {
                "text": "量化金融分析师",
                "link": "https://www.gaodun.com/cqf"
            },
            {
                "text": "注册国际投资分析师",
                "link": "https://www.gaodun.com/ciia/"
            },
            {
                "text": "中国CIIA考试网",
                "link": "http://www.ciia.cn"
            },
            {
                "text": "特许另类投资分析师",
                "link": "https://www.gaodun.com/caia"
            },
            {
                "text": "中国CAIA考试网",
                "link": "https://www.caia.cn"
            },
            {
                "text": "特许财富管理师",
                "link": "https://www.gaodun.com/cwm/"
            },
            {
                "text": "中国CWM考试网",
                "link": "https://www.cwm.cn"
            },
            {
                "text": "基金从业资格",
                "link": "https://www.gaodun.com/jijin"
            },
            {
                "text": "证券从业资格",
                "link": "https://www.gaodun.com/zq"
            },
            {
                "text": "银行从业资格",
                "link": "https://www.gaodun.com/yh"
            },
            {
                "text": "期货从业资格",
                "link": "https://www.gaodun.com/qh"
            }
        ]
    },
    {
        "text": "财会考试",
        "data": [
            {
                "text": "注册会计师",
                "link": "https://www.gaodun.com/cpa"
            },
            {
                "text": "初级会计职称",
                "link": "https://www.gaodun.com/chuji"
            },
            {
                "text": "中级会计职称",
                "link": "https://www.gaodun.com/zhongji"
            },
            {
                "text": "高级会计职称",
                "link": "https://www.gaodun.com/gaoji/"
            },
            {
                "text": "税务师",
                "link": "https://www.gaodun.com/cta"
            },
            {
                "text": "中国税务师考试网",
                "link": "http://www.shuiwushi.com/"
            },
            {
                "text": "统计师",
                "link": "https://www.gaodun.com/tjs/"
            },
            {
                "text": "精算师",
                "link": "https://www.gaodun.com/jss/"
            },
            {
                "text": "审计师",
                "link": "https://www.gaodun.com/sjs/"
            },
            {
                "text": "中国审计师考试网",
                "link": "https://www.shenjishi.com"
            },
            {
                "text": "CFO",
                "link": "https://www.cfo.cn/"
            },
            {
                "text": "ACCA",
                "link": "https://www.gaodun.com/acca"
            },
            {
                "text": "中国ACCA考试网",
                "link": "http://www.chinaacca.org/"
            },
            {
                "text": "USCPA美国会计师",
                "link": "https://www.gaodun.com/uscpa"
            },
            {
                "text": "中国USCPA考试网",
                "link": "http://www.uscpa.net/"
            },
            {
                "text": "CMA",
                "link": "https://www.gaodun.com/cma"
            },
            {
                "text": "中国CMA考试网",
                "link": "http://www.cma.com.cn/"
            },
            {
                "text": "管理会计师",
                "link": "https://www.gaodun.com/mat"
            },
            {
                "text": "中国管理会计网",
                "link": "http://www.chinacma.org/"
            },
            {
                "text": "英国皇家特许会计师",
                "link": "https://www.gaodun.com/aca/"
            },
            {
                "text": "中国ACA考试网",
                "link": "http://www.aca.cn"
            },
            {
                "text": "国际注册内部审计师",
                "link": "https://www.gaodun.com/cia/"
            },
            {
                "text": "中国CIA考试网",
                "link": "http://www.cia.cn"
            },
            {
                "text": "特许管理会计师",
                "link": "https://www.gaodun.com/cima/"
            },
            {
                "text": "中国CIMA考试网",
                "link": "http://www.cima.cn"
            },
            {
                "text": "香港注册会计师考试",
                "link": "https://www.gaodun.com/hkicpa/"
            },
            {
                "text": "中国HKICPA考试网",
                "link": "http://www.hkicpa.cn"
            },
            {
                "text": "薪税师",
                "link": "https://www.gaodun.com/xss"
            },
            {
                "text": "中国薪税师考试网",
                "link": "https://www.xss.cn/"
            }
        ]
    }
]