export const hotlinks = [
    {
        "text": "BOSS直聘",
        "link": "https://www.zhipin.com/"
    },
    {
        "text": "智联招聘",
        "link": "http://www.zhaopin.com/"
    },
    {
        "text": "猎聘网",
        "link": "https://www.liepin.com/"
    },
];

export default [
    {
        "text": "招聘网站",
        "data": [
            {
                "text": "BOSS直聘",
                "link": "https://www.zhipin.com/"
            },
            {
                "text": "智联招聘",
                "link": "http://www.zhaopin.com/"
            },
            {
                "text": "猎聘网",
                "link": "https://www.liepin.com/"
            },
            {
                "text": "58同城招聘",
                "link": "http://www.58.com/?path=job.shtml"
            },
            {
                "text": "前程无忧",
                "link": "http://www.51job.com/"
            },
            {
                "text": "中华英才网",
                "link": "http://www.chinahr.com/"
            },
            {
                "text": "南方人才网",
                "link": "http://www.job168.com/"
            },
            {
                "text": "卓博人才网",
                "link": "http://www.jobcn.com/"
            },
            {
                "text": "智通人才网",
                "link": "http://www.job5156.com/"
            },
            {
                "text": "应届生求职网",
                "link": "http://www.yingjiesheng.com/"
            },
            {
                "text": "应届毕业生",
                "link": "http://www.yjbys.com/"
            },
            {
                "text": "1010兼职网",
                "link": "http://www.1010jz.com/"
            },
            {
                "text": "北京招聘会信息网",
                "link": "https://www.bjzph.com/"
            },
            {
                "text": "云队友",
                "link": "https://www.duiyou360.com/"
            },
            {
                "text": "人才热线",
                "link": "http://www.cjol.com/"
            },
            {
                "text": "36招聘",
                "link": "http://www.job36.com/"
            },
            {
                "text": "看准网",
                "link": "http://www.kanzhun.com/"
            },
            {
                "text": "实习僧",
                "link": "https://www.shixiseng.com/"
            },
            {
                "text": "百城招聘",
                "link": "http://www.stzp.cn/"
            },
            {
                "text": "职友集",
                "link": "http://www.jobui.com/"
            },
            {
                "text": "猪八戒",
                "link": "http://www.zbj.com/"
            },
            {
                "text": "时间财富",
                "link": "http://www.680.com/"
            },
            {
                "text": "一品威客",
                "link": "http://www.epwk.com/"
            }
        ]
    },
    {
        "text": "地方人才网",
        "data": [
            {
                "text": "浙江人才网",
                "link": "http://www.zjrc.com/"
            },
            {
                "text": "深圳人才网",
                "link": "http://www.szhr.com.cn/"
            },
            {
                "text": "湖南人才网",
                "link": "http://www.hnrcsc.com/"
            },
            {
                "text": "厦门人才网",
                "link": "http://www.xmrc.com.cn/"
            },
            {
                "text": "杭州人才网",
                "link": "http://www.hzrc.com/"
            },
            {
                "text": "云南招聘网",
                "link": "http://www.ynzp.com/"
            },
            {
                "text": "四川省人才网",
                "link": "http://www.scrc168.com/"
            },
            {
                "text": "齐鲁人才网",
                "link": "http://www.qlrc.com/"
            },
            {
                "text": "河北搜才网",
                "link": "http://www.hbsc.cn/"
            },
            {
                "text": "宁波人才网",
                "link": "http://www.nbrc.com.cn/"
            },
            {
                "text": "上海第一招聘网",
                "link": "http://www.01job.cn/"
            },
            {
                "text": "苏州人才网",
                "link": "http://www.szrc.cn/"
            },
            {
                "text": "汇博招聘",
                "link": "http://www.huibo.com/"
            },
            {
                "text": "天津泰达人才网",
                "link": "https://www.tedahr.com/"
            },
            {
                "text": "河北人才网",
                "link": "https://www.hbrc.com.cn/"
            },
            {
                "text": "山西人才网",
                "link": "https://www.sjrc.com.cn/"
            },
            {
                "text": "辽宁人才网",
                "link": "http://www.chinalnjob.com/"
            },
            {
                "text": "吉林人才网",
                "link": "https://www.jlrc.com.cn/"
            },
            {
                "text": "黑龙江人才网",
                "link": "http://www.hlj-rc.cn/"
            },
            {
                "text": "江苏人才网",
                "link": "http://www.jsrc.com/"
            },
            {
                "text": "安徽人才网",
                "link": "https://www.ahrcw.com/"
            },
            {
                "text": "安徽人才热线",
                "link": "http://www.anhuihr.com/"
            },
            {
                "text": "中国海峡人才网",
                "link": "https://www.hxrc.com/"
            },
            {
                "text": "江西人才市场",
                "link": "https://www.jxrcw.com/"
            },
            {
                "text": "湖北人才网",
                "link": "http://www.jobhb.com/"
            },
            {
                "text": "广西人才网",
                "link": "https://www.gxrc.com/"
            },
            {
                "text": "海南人才招聘网",
                "link": "https://www.hnrczpw.com/"
            },
            {
                "text": "贵州人才网",
                "link": "https://www.12114job.com/"
            },
            {
                "text": "贵阳人才网",
                "link": "http://www.gyrc.com.cn/"
            },
            {
                "text": "云南人才网",
                "link": "http://www.ynhr.com/"
            },
            {
                "text": "云南招聘网",
                "link": "https://www.ynzp.com/"
            },
            {
                "text": "陕西人才网",
                "link": "http://www.sxrcw.net/"
            },
            {
                "text": "陕西人才招聘网",
                "link": "http://www.sxrcw.com/"
            },
            {
                "text": "甘肃人才网",
                "link": "https://www.gszhaopin.com/"
            },
            {
                "text": "西北人才网",
                "link": "http://www.xbrc.com.cn/"
            },
            {
                "text": "青海人才网",
                "link": "https://www.qhrc.com.cn/"
            },
            {
                "text": "宁夏人才网",
                "link": "http://www.nxrc.com.cn/"
            },
            {
                "text": "银川人才网",
                "link": "http://www.zgycrc.com/"
            },
            {
                "text": "新疆人才网",
                "link": "https://www.xjhr.com/"
            }
        ]
    },
    {
        "text": "人事考试网",
        "data": [
            {
                "text": "中国人事考试网",
                "link": "http://www.cpta.com.cn/index.html"
            },
            {
                "text": "北京",
                "link": "http://rsj.beijing.gov.cn/ywsite/bjpta/"
            },
            {
                "text": "上海",
                "link": "http://www.rsj.sh.gov.cn/spta.shtml"
            },
            {
                "text": "天津",
                "link": "http://hrss.tj.gov.cn/jsdw/rsksw/"
            },
            {
                "text": "重庆",
                "link": "http://rlsbj.cq.gov.cn/ywzl/rsks/"
            },
            {
                "text": "河北",
                "link": "http://www.hebpta.com.cn/hebpta/"
            },
            {
                "text": "山西",
                "link": "http://rst.shanxi.gov.cn/rsks/"
            },
            {
                "text": "内蒙古",
                "link": "http://www.impta.com.cn/"
            },
            {
                "text": "辽宁",
                "link": "http://www.lnrsks.com/"
            },
            {
                "text": "吉林",
                "link": "http://www.jlzkb.com/"
            },
            {
                "text": "黑龙江",
                "link": "http://www.hljrsks.org.cn/"
            },
            {
                "text": "江苏",
                "link": "http://jshrss.jiangsu.gov.cn/col/col57253/index.html"
            },
            {
                "text": "浙江",
                "link": "http://www.zjks.com/"
            },
            {
                "text": "安徽",
                "link": "http://www.apta.gov.cn/"
            },
            {
                "text": "福建",
                "link": "http://www.fjpta.com/"
            },
            {
                "text": "江西",
                "link": "http://www.jxpta.com/"
            },
            {
                "text": "山东",
                "link": "http://hrss.shandong.gov.cn/rsks/"
            },
            {
                "text": "河南",
                "link": "http://www.hnrsks.com/"
            },
            {
                "text": "湖北",
                "link": "http://www.hbsrsksy.cn/"
            },
            {
                "text": "湖南",
                "link": "http://www.hunanpea.com/"
            },
            {
                "text": "广东",
                "link": "http://rsks.gd.gov.cn/"
            },
            {
                "text": "广西",
                "link": "http://www.gxpta.com.cn/"
            },
            {
                "text": "海南",
                "link": "http://hrss.hainan.gov.cn/"
            },
            {
                "text": "四川",
                "link": "http://rst.sc.gov.cn/"
            },
            {
                "text": "贵州",
                "link": "http://pta.guizhou.gov.cn/"
            },
            {
                "text": "云南",
                "link": "http://hrss.yn.gov.cn/ynrsksw/"
            },
            {
                "text": "西藏",
                "link": "http://hrss.xizang.gov.cn/"
            },
            {
                "text": "陕西",
                "link": "http://www.sxrsks.cn/"
            },
            {
                "text": "甘肃",
                "link": "http://ks.rst.gansu.gov.cn/ncms/index.shtml"
            },
            {
                "text": "青海",
                "link": "http://www.qhpta.com/ncms/index.shtml"
            },
            {
                "text": "宁夏",
                "link": "http://www.nxpta.com/"
            },
            {
                "text": "新疆",
                "link": "http://www.xjrsks.com.cn/"
            },
            {
                "text": "新疆生产建设兵团",
                "link": "http://btpta.xjbt.gov.cn/"
            },
            {
                "text": "军队人才网",
                "link": "http://www.81rc.mil.cn/"
            }
        ]
    },
    {
        "text": "银行招聘",
        "data": [
            {
                "text": "建设银行",
                "link": "http://job.ccb.com/cn/job/index.html"
            },
            {
                "text": "交通银行",
                "link": "https://job.bankcomm.com/"
            },
            {
                "text": "招商银行",
                "link": "http://career.cmbchina.com/cmbcareer/"
            },
            {
                "text": "民生银行",
                "link": "http://career.cmbc.com.cn:8080/portal/main/index.jsp"
            },
            {
                "text": "广发银行",
                "link": "http://www.cgbchina.com.cn/Channel/11581868"
            },
            {
                "text": "兴业银行",
                "link": "http://www.cib.com.cn/cn/aboutCIB/about/jobs/index.html"
            },
            {
                "text": "工商银行",
                "link": "https://campus.icbc.com.cn/"
            },
            {
                "text": "中国银行",
                "link": "https://www.boc.cn/aboutboc/bi4/"
            },
            {
                "text": "农业银行",
                "link": "https://career.abchina.com/"
            },
            {
                "text": "邮政储蓄银行",
                "link": "https://www.psbc.com/cn/gyyc/rczp/"
            },
            {
                "text": "中信银行",
                "link": "https://job.citicbank.com/"
            },
            {
                "text": "光大银行",
                "link": "http://cebbank.51job.com/"
            },
            {
                "text": "浦发银行",
                "link": "https://news.spdb.com.cn/about_spd/recruitment/"
            },
            {
                "text": "华夏银行",
                "link": "https://zhaopin.hxb.com.cn/"
            },
            {
                "text": "北京银行",
                "link": "https://bankofbeijing.zhiye.com/"
            },
            {
                "text": "上海银行",
                "link": "https://bosc.zhiye.com/index"
            },
            {
                "text": "浙商银行",
                "link": "http://zp.czbank.com.cn/"
            },
            {
                "text": "江苏银行",
                "link": "https://hr.jsbchina.cn/zp"
            },
            {
                "text": "南京银行",
                "link": "https://job.njcb.com.cn/"
            },
            {
                "text": "杭州银行",
                "link": "https://myjob.hzbank.com.cn/hzzp-apply/"
            },
            {
                "text": "青岛银行",
                "link": "https://hr.qdboffice.com/recruit/talent/official.html#/main"
            },
            {
                "text": "大连银行",
                "link": "http://www.bankofdl.com/home/pc/gywm/cpyc/rczp/index.shtml"
            },
            {
                "text": "广州银行",
                "link": "http://www.gzcb.com.cn/jrgy/rczp/"
            },
            {
                "text": "银行招聘网",
                "link": "http://www.yinhangzhaopin.com/"
            }
        ]
    },
    {
        "text": "行业人才网",
        "data": [
            {
                "text": "中国卫生人才网",
                "link": "http://www.21wecan.com/"
            },
            {
                "text": "建筑英才网",
                "link": "http://www.buildhr.com/"
            },
            {
                "text": "中国医疗人才网",
                "link": "http://www.doctorjob.com.cn/"
            },
            {
                "text": "中国外语人才网",
                "link": "http://www.jobeast.com"
            },
            {
                "text": "中国服装人才网",
                "link": "http://www.cfw.cn"
            },
            {
                "text": "丁香人才网",
                "link": "http://www.jobmd.cn/"
            },
            {
                "text": "高校人才网",
                "link": "http://www.gaoxiaojob.com"
            },
            {
                "text": "美容人才网",
                "link": "http://www.138job.com"
            },
            {
                "text": "英才网联",
                "link": "http://www.800hr.com/"
            },
            {
                "text": "一览电力",
                "link": "http://www.epjob88.com/"
            },
            {
                "text": "中国汽车人才网",
                "link": "http://www.carjob.com.cn"
            },
            {
                "text": "中国印刷人才网",
                "link": "http://www.pjob.net"
            },
            {
                "text": "最佳东方酒店人才",
                "link": "http://www.veryeast.cn/"
            },
            {
                "text": "中国建筑人才网",
                "link": "http://www.buildjob.net"
            },
            {
                "text": "化工英才网",
                "link": "http://www.chenhr.com/"
            },
            {
                "text": "留学人才网",
                "link": "http://www.liuxuehr.com/"
            },
            {
                "text": "一览英才网",
                "link": "http://www.job1001.com/"
            },
            {
                "text": "中国纺织人才网",
                "link": "http://www.texhr.cn/"
            },
            {
                "text": "教师招聘网",
                "link": "http://www.job910.com/"
            },
            {
                "text": "医药英才网",
                "link": "http://www.healthr.com/"
            },
            {
                "text": "鞋业人才网",
                "link": "http://www.shoeshr.com/"
            }
        ]
    }
]