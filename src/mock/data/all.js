/**
 * 分类和排序配置
 */
import * as 程序员 from './程序员';
import * as 设计师 from './设计师';
import * as CTO from './CTO';
import * as CEO from './CEO';
import * as 招聘 from './招聘';
import * as 爱好兴趣 from './爱好兴趣';
import * as 厨师 from './厨师';
import * as 女性 from './女性';
import * as 男性 from './男性';
import * as 旅游 from './旅游';
import * as 数码客 from './数码客';
import * as 健康师 from './健康师';
import * as 公益客 from './公益客';
import * as 摄影师 from './摄影师';
import * as 理财师 from './理财师';
import * as 房产中介 from './房产中介';
import * as 读者 from './读者';
import * as 自媒体 from './自媒体';
import * as 网络冲浪 from './网络冲浪';
import * as 下载站 from './下载站';
import * as 政府 from './政府';
// import * as 北京 from './北京';
// import * as 上海 from './上海';
// import * as 广东 from './广东';

export default [
    { category: '程序员', data: 程序员.default, hotlinks: 程序员.hotlinks },
    { category: '设计师', data: 设计师.default, hotlinks: 设计师.hotlinks },
    { category: 'CTO', data: CTO.default, hotlinks: CTO.hotlinks },
    { category: 'CEO', data: CEO.default, hotlinks: CEO.hotlinks },
    { category: '招聘', data: 招聘.default, hotlinks: 招聘.hotlinks },
    { category: '厨师', data: 厨师.default, hotlinks: 厨师.hotlinks },
    { category: '女性', data: 女性.default, hotlinks: 女性.hotlinks },
    { category: '男性', data: 男性.default, hotlinks: 男性.hotlinks },
    { category: '旅游', data: 旅游.default, hotlinks: 旅游.hotlinks },
    { category: '数码客', data: 数码客.default, hotlinks: 数码客.hotlinks },
    { category: '健康师', data: 健康师.default, hotlinks: 健康师.hotlinks },
    { category: '公益客', data: 公益客.default, hotlinks: 公益客.hotlinks },
    { category: '摄影师', data: 摄影师.default, hotlinks: 摄影师.hotlinks },
    { category: '理财师', data: 理财师.default, hotlinks: 理财师.hotlinks },
    { category: '房产中介', data: 房产中介.default, hotlinks: 房产中介.hotlinks },
    { category: '读者', data: 读者.default, hotlinks: 读者.hotlinks },
    { category: '自媒体', data: 自媒体.default, hotlinks: 自媒体.hotlinks },
    { category: '网络冲浪', data: 网络冲浪.default, hotlinks: 网络冲浪.hotlinks },
    { category: '下载站', data: 下载站.default, hotlinks: 下载站.hotlinks },
    { category: '爱好兴趣', data: 爱好兴趣.default, hotlinks: 爱好兴趣.hotlinks },
    { category: '政府', data: 政府.default, hotlinks: 政府.hotlinks },
    // { category: '北京', data: 北京 },
    // { category: '上海', data: 上海 },
    // { category: '广东', data: 广东 },
];