export const hotlinks = [
    {
        "text": "异次元软件世界",
        "link": "https://www.iplaysoft.com/"
    },
    {
        "text": "少数派 - 高效工作，品质生活",
        "link": "https://sspai.com/"
    },
    {
        "text": "小众软件",
        "link": "https://www.appinn.com/"
    },
    {
        "text": "脚本之家 - 软件下载",
        "link": "https://www.jb51.net/softs/"
    },
    {
        "text": "西西软件站",
        "link": "https://www.cr173.com/"
    },
    {
        "text": "系统之家",
        "link": "https://www.xitongzhijia.net/"
    },
];

export default [
    {
        "text": "全网下载",
        "data": [
            {
                "text": "就免费",
                "link": "https://www.9mf.net/"
            },
            {
                "text": "资源分享网",
                "link": "https://www.ziyuan.tv/"
            },
            {
                "text": "小白盘",
                "link": "https://www.xiaobaipan.com/"
            },
            {
                "text": "网盘传奇",
                "link": "https://jidanso.com/"
            },
            {
                "text": "虫部落",
                "link": "www.chongbuluo.com"
            },
            {
                "text": "如风搜",
                "link": "http://www.rufengso.net/"
            },
            {
                "text": "爱扒趣",
                "link": "https://www.zyboe.com/"
            }
        ]
    },
    {
        "text": "软件下载站",
        "data": [
            {
                "text": "异次元软件世界",
                "link": "https://www.iplaysoft.com/"
            },
            {
                "text": "少数派 - 高效工作，品质生活",
                "link": "https://sspai.com/"
            },
            {
                "text": "小众软件",
                "link": "https://www.appinn.com/"
            },
            {
                "text": "果核剥壳",
                "link": "https://www.ghxi.com/"
            },
            {
                "text": "六音软件",
                "link": "https://6yso.com/",
                "title": "分享绿色软件、免费软件、最新资讯！"
            },
            {
                "text": "绿软小站",
                "link": "https://www.gndown.com/",
                "title": "分享绿色软件、免费软件、最新资讯！"
            },
            {
                "text": "Yx12345下载站",
                "link": "http://www.yx12345.com/"
            },
            {
                "text": "脚本之家 - 软件下载",
                "link": "https://www.jb51.net/softs/"
            },
            {
                "text": "西西软件站",
                "link": "https://www.cr173.com/"
            },
            {
                "text": "闪电软件园",
                "link": "http://www.sd173.com/"
            },
            {
                "text": "完美下载站",
                "link": "https://www.wmzhe.com/"
            },
            {
                "text": "我爱分享网",
                "link": "http://www.zhanshaoyi.com/"
            },
            {
                "text": "下载之家",
                "link": "http://www.downza.cn/"
            },
            {
                "text": "PC下载网",
                "link": "https://www.pcsoft.com.cn/"
            },
            {
                "text": "星动下载",
                "link": "https://www.singdown.com/"
            },
            {
                "text": "下载集",
                "link": "https://www.xzji.com/"
            },
            {
                "text": "3DM游戏网 - 软件下载",
                "link": "https://soft.3dmgame.com/"
            },
            {
                "text": "系统之家",
                "link": "https://www.xitongzhijia.net/"
            },
            {
                "text": "pc6下载站",
                "link": "https://www.pc6.com/"
            }
        ]
    }
]