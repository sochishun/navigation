export const hotlinks = [
    {
        "text": "色影无忌",
        "link": "http://www.xitek.com/"
    },
    {
        "text": "蜂鸟网",
        "link": "http://www.fengniao.com/"
    },
    {
        "text": "中国国家地理",
        "link": "http://www.dili360.com/"
    },
];

export default [
    {
        "text": "摄影网站",
        "data": [
            {
                "text": "色影无忌",
                "link": "http://www.xitek.com/"
            },
            {
                "text": "蜂鸟网",
                "link": "http://www.fengniao.com/"
            },
            {
                "text": "中国摄影在线",
                "link": "http://www.cphoto.net/"
            },
            {
                "text": "中国国家地理",
                "link": "http://www.dili360.com/"
            },
            {
                "text": "黑光网",
                "link": "http://www.heiguang.com/"
            },
            {
                "text": "ZOL数码影像",
                "link": "http://dcdv.zol.com.cn/"
            },
            {
                "text": "POCO摄影网",
                "link": "http://www.poco.cn/"
            },
            {
                "text": "影像中国",
                "link": "http://www.cpanet.cn/"
            },
            {
                "text": "太平洋摄影",
                "link": "http://dp.pconline.com.cn/"
            },
            {
                "text": "新华摄影",
                "link": "http://www.xinhuanet.com/foto/index.htm"
            },
            {
                "text": "迪派影像",
                "link": "http://www.dpnet.com.cn/"
            },
            {
                "text": "FOTOMEN",
                "link": "http://fotomen.cn/"
            }
        ]
    },
    {
        "text": "摄影论坛",
        "data": [
            {
                "text": "佳友摄影论坛",
                "link": "http://www.photofans.cn/"
            },
            {
                "text": "天涯摄影社区",
                "link": "http://www.tianya.cn/new/Publicforum/ArticlesList.asp?strItem=tianyaphoto&idWriter=0&Key=0&Part=0"
            },
            {
                "text": "无忌论坛",
                "link": "http://forum.xitek.com/"
            },
            {
                "text": "蜂鸟论坛",
                "link": "http://bbs.fengniao.com/"
            },
            {
                "text": "新摄影论坛",
                "link": "http://forums.nphoto.net/"
            }
        ]
    },
    {
        "text": "教程技巧",
        "data": [
            {
                "text": "蜂鸟摄影学院",
                "link": "http://academy.fengniao.com/"
            },
            {
                "text": "教你用相机",
                "link": "http://dc.pconline.com.cn/jiqiao/"
            },
            {
                "text": "POCO摄影教程",
                "link": "http://www.poco.cn/skill/skill_list"
            },
            {
                "text": "新摄影技术教程",
                "link": "http://www.nphoto.net/news/list/06/"
            },
            {
                "text": "无忌学院",
                "link": "http://she.xitek.com/skills/"
            },
            {
                "text": "黑光摄影教程",
                "link": "https://www.heiguang.com/photography/"
            }
        ]
    },
    {
        "text": "相册图库",
        "data": [
            {
                "text": "360图片",
                "link": "http://image.so.com/"
            },
            {
                "text": "百度图片",
                "link": "http://image.baidu.com/"
            },
            {
                "text": "昵图网",
                "link": "http://www.nipic.com/"
            },
            {
                "text": "太平洋摄影部落",
                "link": "http://dp.pconline.com.cn/"
            },
            {
                "text": "天涯贴图专区",
                "link": "http://bbs.tianya.cn/list-no04-1.shtml"
            },
            {
                "text": "天堂图片网",
                "link": "http://www.ivsky.com/"
            },
            {
                "text": "花瓣网",
                "link": "http://huaban.com/"
            },
            {
                "text": "站长图库",
                "link": "https://www.zztuku.com/"
            },
            {
                "text": "站酷",
                "link": "https://www.zcool.com.cn/"
            },
            {
                "text": "千图网",
                "link": "http://www.58pic.com/"
            },
            {
                "text": "全景网",
                "link": "http://www.quanjing.com/"
            },
            {
                "text": "图行天下",
                "link": "http://www.photophoto.cn/"
            },
            {
                "text": "我图网",
                "link": "http://www.ooopic.com/"
            },
            {
                "text": "中国图库",
                "link": "http://www.tukuchina.cn/"
            },
            {
                "text": "汇图网",
                "link": "http://www.huitu.com/"
            },
            {
                "text": "站酷海洛",
                "link": "http://www.hellorf.com/"
            },
            {
                "text": "站长素材",
                "link": "http://sc.chinaz.com/"
            },
            {
                "text": "图虫网",
                "link": "http://www.tuchong.com/"
            }
        ]
    },
    {
        "text": "桌面壁纸",
        "data": [
            {
                "text": "360图片壁纸",
                "link": "http://image.so.com/i?q=%E5%A3%81%E7%BA%B8&src=srp"
            },
            {
                "text": "ZOL桌面壁纸",
                "link": "http://desk.zol.com.cn/"
            },
            {
                "text": "桌酷壁纸",
                "link": "http://www.zhuoku.com/"
            },
            {
                "text": "彼岸桌面",
                "link": "http://www.netbian.com/"
            },
            {
                "text": "回车桌面",
                "link": "https://www.enterdesk.com/"
            },
            {
                "text": "致美化",
                "link": "https://zhutix.com/"
            }
        ]
    },
    {
        "text": "器材品牌",
        "data": [
            {
                "text": "佳能",
                "link": "http://www.canon.com.cn/"
            },
            {
                "text": "尼康",
                "link": "http://www.nikon.com.cn/"
            },
            {
                "text": "索尼",
                "link": "http://www.sonystyle.com.cn/products/cyber-shot/index.htm"
            },
            {
                "text": "理光映像",
                "link": "http://www.pentax.com.cn/"
            },
            {
                "text": "富士",
                "link": "http://www.fujifilm.com.cn/"
            },
            {
                "text": "奥林巴斯",
                "link": "https://om-digitalsolutions.cn/"
            },
            {
                "text": "松下",
                "link": "http://consumer.panasonic.cn/product/cameras-camcorders.html"
            },
            {
                "text": "卡西欧",
                "link": "http://www.casio.com.cn/dc/index.html"
            },
            {
                "text": "徕卡",
                "link": "https://www.leica-camera.cn/"
            },
            {
                "text": "哈苏",
                "link": "https://www.hasselblad.com/zh-cn/"
            },
            {
                "text": "腾龙",
                "link": "http://www.tamron.com.cn/"
            },
            {
                "text": "适马",
                "link": "http://www.sigma-photo.com.cn/"
            },
            {
                "text": "JVC",
                "link": "http://www.jvc.com.cn/camcorder/"
            },
            {
                "text": "飞思",
                "link": "http://www.phaseone.com.cn/"
            }
        ]
    }
]