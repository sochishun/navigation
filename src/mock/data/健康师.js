export const hotlinks = [
    { "text": "丁香医生", "link": "http://www.dxy.com/" },
    { "text": "健康报网", "link": "http://www.jkb.com.cn/" },
    { "text": "基层医生网", "link": "http://www.jcys.com/" },
];

export default [
    {
        "text": "健康网站", "data": [
            { "text": "寻医问药", "link": "http://www.xywy.com/" },
            { "text": "好大夫在线", "link": "http://www.haodf.com/" },
            { "text": "家庭医生", "link": "http://www.familydoctor.com.cn/" },
            { "text": "有问必答网", "link": "http://www.120ask.com/" },
            { "text": "99健康网", "link": "http://www.99.com.cn/" },
            { "text": "求医网", "link": "http://www.qiuyi.cn/" },
            { "text": "丁香医生", "link": "http://www.dxy.com/" },
            { "text": "39健康网", "link": "http://www.39.net" },
            { "text": "搜狐健康", "link": "http://health.sohu.com" },
            { "text": "健康网", "link": "http://www.healthoo.com" },
            { "text": "复禾健康", "link": "http://www.fh21.com.cn/" },
            { "text": "民福健康网", "link": "http://www.39yst.com/" },
            { "text": "人民网健康", "link": "http://health.people.com.cn" },
            { "text": "39问医生", "link": "http://ask.39.net/" },
            { "text": "久久健康网", "link": "http://www.9939.com" },
            { "text": "微医", "link": "http://www.guahao.com/" },
            { "text": "健康第一线", "link": "http://www.vodjk.com/" },
            { "text": "医网", "link": "http://www.ewsos.com/" },
            { "text": "民福康", "link": "https://www.mfk.com/" },
            { "text": "糖尿病论坛", "link": "http://bbs.tnbz.com/" }
        ]
    }, {
        "text": "健康工具", "data": [
            { "text": "标准体重计算", "link": "http://tool.httpcn.com/Tool/BiaoZhunTiZhong.html" },
            { "text": "全国医院查询", "link": "https://www.haodf.com/yiyuan/all/list.htm" }
        ]
    }, {
        "text": "医学行业", "data": [
            { "text": "丁香园", "link": "http://www.dxy.cn/" },
            { "text": "爱爱医", "link": "http://www.iiyi.com/" },
            { "text": "医学教育网", "link": "http://www.med66.com/" },
            { "text": "卫生健康委员会", "link": "http://www.nhc.gov.cn/" },
            { "text": "好医生", "link": "http://www.haoyisheng.com/" },
            { "text": "生物谷", "link": "http://www.bioon.com/" },
            { "text": "525心理网", "link": "http://www.psy525.cn/" },
            { "text": "华医网", "link": "http://www.91huayi.com/" },
            { "text": "医药网", "link": "http://www.pharmnet.com.cn/" },
            { "text": "中华医学会", "link": "http://www.cma.org.cn/" },
            { "text": "基层医生网", "link": "http://www.jcys.com/" },
            { "text": "健康报网", "link": "http://www.jkb.com.cn/" }
        ]
    }]