export const hotlinks = [
    { "text": "瑞丽网", "link": "http://www.rayli.com.cn/" },
    { "text": "薄荷瘦身网", "link": "http://www.boohee.com/" },
    { "text": "服饰流行前线", "link": "http://www.pop-fashion.com/" },
    { "text": "编织人生", "link": "http://www.bianzhirensheng.com/" },
];

export default [
    {
        "text": "女性名站", "data": [
            { "text": "瑞丽网", "link": "http://www.rayli.com.cn/" },
            { "text": "太平洋时尚网", "link": "http://www.pclady.com.cn/" },
            { "text": "腾讯时尚", "link": "https://new.qq.com/ch/fashion/" },
            { "text": "搜狐时尚", "link": "http://fashion.sohu.com/" },
            { "text": "OnlyLady女人志", "link": "http://www.onlylady.com/" },
            { "text": "爱丽时尚网", "link": "http://www.aili.com/" },
            { "text": "网易女人", "link": "http://lady.163.com/" },
            { "text": "新华网时尚", "link": "http://www.news.cn/fashion/" },
            { "text": "时尚网", "link": "http://www.trends.com.cn/" },
            { "text": "ELLE中文网", "link": "http://www.ellechina.com/" },
            { "text": "发型站", "link": "http://www.faxingzhan.com/" },
            { "text": "YOKA网", "link": "http://www.yoka.com/" }
        ]
    },
    {
        "text": "美容瘦身", "data": [
            { "text": "无忧爱美", "link": "http://www.51aimei.com/" },
            { "text": "薄荷瘦身网", "link": "http://www.boohee.com/" },
            { "text": "瑞丽美容", "link": "http://beauty.rayli.com.cn/" }
        ]
    },
    {
        "text": "时装时尚", "data": [
            { "text": "VOGUE时尚网", "link": "http://www.vogue.com.cn/" },
            { "text": "凤凰网时尚", "link": "http://fashion.ifeng.com/" },
            { "text": "时尚品牌网", "link": "http://www.chinasspp.com/" },
            { "text": "编织人生", "link": "http://www.bianzhirensheng.com/" },
            { "text": "ladymax时尚网", "link": "http://www.ladymax.cn/" },
            { "text": "服饰流行前线", "link": "http://www.pop-fashion.com/" },
            { "text": "风尚中国", "link": "http://www.fengsung.com/" }
        ]
    },
    {
        "text": "花鸟鱼虫",
        "data": [
            {
                "text": "花卉世界网",
                "link": "http://www.flowerworld.cn/"
            },
            {
                "text": "中国兰花交易网",
                "link": "http://www.hmlan.com/"
            },
            {
                "text": "中国金鱼网",
                "link": "http://www.lanshou.net/"
            },
            {
                "text": "龙鱼之巅",
                "link": "http://longyu.cc/"
            },
            {
                "text": "中国信鸽协会",
                "link": "http://www.crpa.cn/"
            },
            {
                "text": "中华信鸽网",
                "link": "http://www.zhxg.com/"
            }
        ]
    },
    {
        "text": "宠物交流",
        "data": [
            {
                "text": "猛犬俱乐部",
                "link": "http://www.bandog.cn/"
            },
            {
                "text": "天涯宠物乐园",
                "link": "http://bbs.tianya.cn/list-75-1.shtml"
            },
            {
                "text": "58同城宠物市场",
                "link": "https://bj.58.com/pets.shtml"
            },
            {
                "text": "名犬网",
                "link": "http://www.cndog.net/"
            },
            {
                "text": "宠才网",
                "link": "https://www.pethr.com/"
            }
        ]
    },
    {
        "text": "宠物商城",
        "data": [
            {
                "text": "E宠商城",
                "link": "https://www.epet.com/"
            },
            {
                "text": "波奇网",
                "link": "http://www.boqii.com/"
            },
            {
                "text": "狗铺子",
                "link": "http://www.goupuzi.com/"
            }
        ]
    }
]