export const hotlinks = [
];

export default [
    {
        "text": "常用服务",
        "data": [
            {
                "text": "北京市人民政府",
                "link": "http://www.beijing.gov.cn/"
            },
            {
                "text": "工作居住证",
                "link": "http://www.bjrcgz.gov.cn/"
            },
            {
                "text": "北京交警APP",
                "link": "https://bj.122.gov.cn/#/app"
            },
            {
                "text": "车辆违法查询",
                "link": "http://bj.122.gov.cn/"
            },
            {
                "text": "限行车牌尾号",
                "link": "http://jtgl.beijing.gov.cn/jgj/lszt/659722/660341/index.html"
            },
            {
                "text": "车辆摇号",
                "link": "https://xkczb.jtw.beijing.gov.cn/"
            },
            {
                "text": "进京证办理指南",
                "link": "http://banshi.beijing.gov.cn/pubtask/task/1/110000000000/24d52caa-9565-11e9-8300-507b9d3e4710.html?locationCode=110000000000&serverType=1001"
            },
            {
                "text": "住房公积金查询",
                "link": "http://gjj.beijing.gov.cn/"
            },
            {
                "text": "社保服务平台",
                "link": "http://rsj.beijing.gov.cn/"
            },
            {
                "text": "市长信箱",
                "link": "http://szxx.beijing.gov.cn/szxxnew/"
            },
            {
                "text": "人事考试服务",
                "link": "http://rsj.beijing.gov.cn/bjpta/"
            },
            {
                "text": "辟谣平台",
                "link": "http://py.qianlong.com/"
            },
            {
                "text": "市政交通一卡通",
                "link": "https://www.bmac.com.cn/"
            },
            {
                "text": "北京旅游网",
                "link": "http://www.visitbeijing.com.cn/"
            }
        ]
    },
    {
        "text": "政府部门",
        "data": [
            {
                "text": "市政府办公厅",
                "link": "http://www.beijing.gov.cn/gongkai/zfxxgk/szfbgt/"
            },
            {
                "text": "市人力社保局",
                "link": "http://rsj.beijing.gov.cn"
            },
            {
                "text": "市发展改革委",
                "link": "http://fgw.beijing.gov.cn"
            },
            {
                "text": "市教委",
                "link": "http://jw.beijing.gov.cn/"
            },
            {
                "text": "市科委",
                "link": "http://kw.beijing.gov.cn"
            },
            {
                "text": "经济和信息化局",
                "link": "http://jxj.beijing.gov.cn/"
            },
            {
                "text": "市公安局",
                "link": "http://gaj.beijing.gov.cn"
            },
            {
                "text": "市民政局",
                "link": "http://mzj.beijing.gov.cn"
            },
            {
                "text": "市司法局",
                "link": "http://sfj.beijing.gov.cn"
            },
            {
                "text": "市财政局",
                "link": "http://czj.beijing.gov.cn/"
            },
            {
                "text": "市规划自然资源委",
                "link": "http://ghzrzyw.beijing.gov.cn/"
            },
            {
                "text": "市生态环境局",
                "link": "http://sthjj.beijing.gov.cn/"
            },
            {
                "text": "市住房城乡建设委",
                "link": "http://zjw.beijing.gov.cn"
            },
            {
                "text": "市城市管理委",
                "link": "http://csglw.beijing.gov.cn"
            },
            {
                "text": "市交通委",
                "link": "http://jtw.beijing.gov.cn"
            },
            {
                "text": "市农业农村局",
                "link": "http://nyncj.beijing.gov.cn/"
            },
            {
                "text": "市水务局",
                "link": "http://swj.beijing.gov.cn"
            },
            {
                "text": "市商务局",
                "link": "http://sw.beijing.gov.cn/"
            },
            {
                "text": "市文化和旅游局",
                "link": "http://whlyj.beijing.gov.cn/"
            },
            {
                "text": "市卫生健康委",
                "link": "http://wjw.beijing.gov.cn"
            },
            {
                "text": "市审计局",
                "link": "http://sjj.beijing.gov.cn/"
            },
            {
                "text": "市政府外办",
                "link": "http://wb.beijing.gov.cn"
            },
            {
                "text": "市国资委",
                "link": "http://gzw.beijing.gov.cn"
            },
            {
                "text": "市市场监督管理局",
                "link": "http://scjgj.beijing.gov.cn/"
            },
            {
                "text": "市安全监管局",
                "link": "http://ajj.beijing.gov.cn"
            },
            {
                "text": "市文资办",
                "link": "http://wzb.beijing.gov.cn"
            },
            {
                "text": "市广播电视局",
                "link": "http://gdj.beijing.gov.cn/"
            },
            {
                "text": "市文物局",
                "link": "http://wwj.beijing.gov.cn/"
            },
            {
                "text": "市体育局",
                "link": "http://tyj.beijing.gov.cn/"
            },
            {
                "text": "市统计局",
                "link": "http://tjj.beijing.gov.cn/"
            },
            {
                "text": "市园林绿化局",
                "link": "http://yllhj.beijing.gov.cn"
            },
            {
                "text": "市金融局",
                "link": "http://jrj.beijing.gov.cn"
            },
            {
                "text": "市知识产权局",
                "link": "http://zscqj.beijing.gov.cn"
            },
            {
                "text": "市信访办",
                "link": "http://xfb.beijing.gov.cn/"
            }
        ]
    },
    {
        "text": "新闻报刊",
        "data": [
            {
                "text": "人民网",
                "link": "http://www.people.com.cn/"
            },
            {
                "text": "参考消息",
                "link": "http://www.cankaoxiaoxi.com/"
            },
            {
                "text": "京报网",
                "link": "http://www.bjd.com.cn/"
            },
            {
                "text": "千龙网",
                "link": "http://www.qianlong.com/"
            },
            {
                "text": "北青网",
                "link": "http://www.ynet.com/"
            },
            {
                "text": "中青在线",
                "link": "http://www.cyol.com/"
            },
            {
                "text": "光明网",
                "link": "http://www.gmw.cn/"
            },
            {
                "text": "北京周报",
                "link": "http://www.beijingreview.com.cn/"
            },
            {
                "text": "望京网",
                "link": "http://www.wangjing.cn/"
            },
            {
                "text": "八通网",
                "link": "http://www.bato.cn/"
            },
            {
                "text": "燕郊网城",
                "link": "http://www.yanjiao.com/"
            },
            {
                "text": "网聚房山",
                "link": "http://www.haofs.com/"
            }
        ]
    },
    {
        "text": "本地生活",
        "data": [
            {
                "text": "北京天气",
                "link": "http://www.weather.com.cn/weather1d/101010100.shtml"
            },
            {
                "text": "首都机场",
                "link": "http://www.bcia.com.cn/"
            },
            {
                "text": "北京医院列表",
                "link": "https://beijing.haodf.com/"
            },
            {
                "text": "北京地区大学",
                "link": "http://hao.360.cn/sub/daxue_beijing.html"
            }
        ]
    },
    {
        "text": "旅游景点",
        "data": [
            {
                "text": "故宫博物院",
                "link": "https://www.dpm.org.cn/"
            },
            {
                "text": "颐和园",
                "link": "http://www.summerpalace-china.com/"
            },
            {
                "text": "八达岭长城",
                "link": "http://www.badaling.cn/"
            },
            {
                "text": "天坛公园",
                "link": "http://www.tiantanpark.com/"
            },
            {
                "text": "中国国家博物馆",
                "link": "http://www.chnmuseum.cn/"
            },
            {
                "text": "圆明园",
                "link": "http://www.yuanmingyuanpark.cn/"
            },
            {
                "text": "北京动物园",
                "link": "http://www.beijingzoo.com/"
            },
            {
                "text": "慕田峪长城",
                "link": "http://www.mutianyugreatwall.com/"
            },
            {
                "text": "明十三陵",
                "link": "http://www.mingtombs.com/"
            },
            {
                "text": "恭王府",
                "link": "https://www.pgm.org.cn/"
            },
            {
                "text": "雍和宫",
                "link": "http://www.yonghegong.cn/"
            },
            {
                "text": "北海公园",
                "link": "https://www.beihaipark.com.cn/"
            },
            {
                "text": "香山公园",
                "link": "http://www.xiangshanpark.com/"
            },
            {
                "text": "古北水镇",
                "link": "http://www.wtown.com/"
            },
            {
                "text": "八达岭野生动物园",
                "link": "http://www.bdlsw.com.cn/"
            },
            {
                "text": "潭柘寺",
                "link": "http://www.bjtanzhesi.com.cn/"
            },
            {
                "text": "北京欢乐谷",
                "link": "https://bj.happyvalley.cn/"
            },
            {
                "text": "红螺寺",
                "link": "http://hongluosi.com/"
            }
        ]
    },
    {
        "text": "各区政府",
        "data": [
            {
                "text": "东城区",
                "link": "http://www.bjdch.gov.cn/"
            },
            {
                "text": "西城区",
                "link": "http://www.bjxch.gov.cn/"
            },
            {
                "text": "朝阳区",
                "link": "http://www.bjchy.gov.cn/"
            },
            {
                "text": "海淀区",
                "link": "http://www.bjhd.gov.cn/"
            },
            {
                "text": "丰台区",
                "link": "http://www.bjft.gov.cn/"
            },
            {
                "text": "石景山区",
                "link": "http://www.bjsjs.gov.cn/"
            },
            {
                "text": "门头沟区",
                "link": "http://www.bjmtg.gov.cn/"
            },
            {
                "text": "房山区",
                "link": "http://www.bjfsh.gov.cn/"
            },
            {
                "text": "通州区",
                "link": "http://www.bjtzh.gov.cn"
            },
            {
                "text": "顺义区",
                "link": "http://www.bjshy.gov.cn/"
            },
            {
                "text": "大兴区",
                "link": "http://www.bjdx.gov.cn/"
            },
            {
                "text": "昌平区",
                "link": "http://www.bjchp.gov.cn/"
            },
            {
                "text": "平谷区",
                "link": "http://www.bjpg.gov.cn/"
            },
            {
                "text": "怀柔区",
                "link": "http://www.bjhr.gov.cn"
            },
            {
                "text": "密云区",
                "link": "http://www.bjmy.gov.cn/"
            },
            {
                "text": "延庆区",
                "link": "http://www.bjyq.gov.cn/"
            }
        ]
    }
]