export const hotlinks = [
    {
        "text": "编程猫社区",
        "link": "https://shequ.codemao.cn/",
        "tooltip": "少儿编程社区",
        "icon": "https://static.codemao.cn/community/shequ_logo.png"
    },
    {
        "text": "菜鸟教程",
        "link": "https://www.runoob.com/",
        "tooltip": "学的不仅是技术，更是梦想！"
    },
    {
        "text": "腾讯课堂",
        "link": "https://ke.qq.com/",
        "tooltip": "腾讯课堂_职业培训、考试提升在线教育平台"
    },
    {
        "text": "人人钢琴网",
        "link": "https://www.everyonepiano.cn/"
    },
    {
        "text": "爱蛙简笔画",
        "link": "https://www.jbhdq.com/",
        "tooltip": "简笔画大全主要提供：动物简笔画、人物简笔画等画画图片大全超简单。"
    }
];

export default [
    {
        "text": "每日刷题",
        "data": [
            {
                "text": "LintCode 领扣",
                "link": "https://www.lintcode.com/",
                "tooltip": "空前强大的在线编程训练系统！"
            },
            {
                "text": "力扣",
                "link": "https://leetcode-cn.com/",
                "tooltip": "全球极客挚爱的技术成长平台"
            },
            {
                "text": "前端面试每日3+1",
                "link": "http://www.h-camel.com/index.html",
                "tooltip": "前端面试每日3+1-以前端面试题来驱动学习，提倡每日学习与思考，每天进步一点！"
            },
            {
                "text": "迅捷CAD每日一图",
                "link": "http://www.xunjiecad.com/dailypic.html",
                "tooltip": "新手必备AutoCAD练习图纸，分分钟提高你的绘图效率！"
            },
            {
                "text": "50张CAD练习图纸",
                "link": "https://baijiahao.baidu.com/s?id=1645908268880011846",
                "tooltip": "50张CAD练习图纸，新手小白还不快收藏下来练练手"
            },
            {
                "text": "AutoCAD最全基础练习图合集",
                "link": "https://baijiahao.baidu.com/s?id=1649422698319004835",
                "tooltip": "AutoCAD入门没捷径，最全基础练习图合集送你，贵在坚持"
            }
        ]
    },
    {
        "text": "游戏开发",
        "data": [
            {
                "text": "网易有道卡搭 (Scratch)",
                "link": "https://kada.163.com/",
                "tooltip": "网易有道卡搭 - 创意编程社区-网易旗下专业少儿编程启蒙学习平台",
                "icon": "https://edu-image.nosdn.127.net/662a01e7c44949d9b5c705966ea94b3d.png?imageView&quality=100"
            },
            {
                "text": "编程猫社区",
                "link": "https://shequ.codemao.cn/",
                "tooltip": "少儿编程社区",
                "icon": "https://static.codemao.cn/community/shequ_logo.png"
            }
        ]
    },
    {
        "text": "IT 自学充电",
        "data": [
            {
                "text": "W3cplus前端网",
                "link": "https://www.w3cplus.com/",
                "tooltip": "引领web前沿，打造前端精品教程"
            },
            {
                "text": "菜鸟教程",
                "link": "https://www.runoob.com/",
                "tooltip": "学的不仅是技术，更是梦想！"
            },
            {
                "text": "菜鸟鸭",
                "link": "https://www.cainiaoya.com/",
                "tooltip": "菜鸟教程 - 菜鸟鸭,编程始于足下！"
            },
            {
                "text": "W3School",
                "link": "https://www.w3school.com.cn/",
                "tooltip": "全球最大的中文 Web 技术教程"
            },
            {
                "text": "LearnKu",
                "link": "https://learnku.com/",
                "tooltip": "LearnKu 终身编程者的知识社区"
            },
            {
                "text": "网道",
                "link": "https://wangdoc.com/",
                "tooltip": "复杂的技术，简单的讲解。"
            },
            {
                "text": "Bootstrap中文网",
                "link": "https://www.bootcss.com/",
                "tooltip": "Bootstrap中文网致力于为广大国内开发者提供详尽的中文文档、代码实例等，助力开发者掌握并使用这一框架。"
            },
            {
                "text": "MDN Web 文档",
                "link": "https://developer.mozilla.org/zh-CN/",
                "tooltip": "火狐浏览器官方维护的Web技术文档"
            },
            {
                "text": "IBM Developer 中文网站",
                "link": "https://developer.ibm.com/zh/",
                "tooltip": "火狐浏览器官方维护的Web技术文档"
            },
            {
                "text": "我要自学网",
                "link": "https://www.51zxw.net/",
                "tooltip": "视频教程|免费教程|自学电脑|3D教程|平面教程|影视动画教程|办公教程|机械设计教程|网站设计教程"
            }
        ]
    },
    {
        "text": "自媒体创作",
        "data": [
            {
                "text": "融媒宝",
                "link": "https://www.rongmeibao.net/",
                "tooltip": "融媒宝"
            }
        ]
    },
    {
        "text": "在线阅读",
        "data": [
            {
                "text": "有道翻译",
                "link": "http://dict.youdao.com/",
                "tooltip": "有道翻译"
            },
            {
                "text": "微信读书",
                "link": "https://weread.qq.com/",
                "tooltip": "微信读书"
            }
        ]
    },
    {
        "text": "在线课堂",
        "data": [
            {
                "text": "腾讯课堂",
                "link": "https://ke.qq.com/",
                "tooltip": "腾讯课堂_职业培训、考试提升在线教育平台"
            },
            {
                "text": "慕课网",
                "link": "https://www.imooc.com/",
                "tooltip": "慕课网-程序员的梦工厂"
            },
            {
                "text": "网易云课堂",
                "link": "https://study.163.com/",
                "tooltip": "网易云课堂 - 悄悄变强大"
            },
            {
                "text": "哔哩哔哩",
                "link": "https://www.bilibili.com/",
                "tooltip": "哔哩哔哩 (゜-゜)つロ 干杯~-bilibili"
            }
        ]
    },
    {
        "text": "CAD 设计",
        "data": [
            {
                "text": "迅捷CAD每日一图",
                "link": "http://www.xunjiecad.com/dailypic.html",
                "tooltip": "新手必备AutoCAD练习图纸，分分钟提高你的绘图效率！"
            },
            {
                "text": "50张CAD练习图纸",
                "link": "https://baijiahao.baidu.com/s?id=1645908268880011846",
                "tooltip": "50张CAD练习图纸，新手小白还不快收藏下来练练手"
            },
            {
                "text": "AutoCAD最全基础练习图合集",
                "link": "https://baijiahao.baidu.com/s?id=1649422698319004835",
                "tooltip": "AutoCAD入门没捷径，最全基础练习图合集送你，贵在坚持"
            }
        ]
    },
    {
        "text": "音乐曲谱",
        "data": [
            {
                "text": "在线钢琴模拟器",
                "link": "https://www.xiwnn.com/piano/"
            },
            {
                "text": "环球钢琴网",
                "link": "https://www.hqgq.com/"
            },
            {
                "text": "中国乐谱网",
                "link": "https://www.cnscore.com/"
            },
            {
                "text": "虫虫钢琴",
                "link": "https://www.gangqinpu.com/"
            },
            {
                "text": "人人钢琴网",
                "link": "https://www.everyonepiano.cn/"
            },
            {
                "text": "吉他堂",
                "link": "https://www.jitatang.com/"
            },
            {
                "text": "17吉他",
                "link": "http://www.17jita.com/"
            },
            {
                "text": "大伟吉他教室",
                "link": "https://www.daweijita.com/"
            },
            {
                "text": "吉他之家",
                "link": "http://www.798com.com/"
            },
            {
                "text": "吉他社",
                "link": "https://www.jitashe.org/"
            },
            {
                "text": "吉他吧",
                "link": "http://www.jitaba.cn/"
            },
            {
                "text": "吉他八号",
                "link": "https://www.jita8.top/"
            },
            {
                "text": "芊芊歌谱",
                "link": "http://www.qqgpw.com/"
            },
            {
                "text": "520吉他网",
                "link": "https://www.520jita.com/"
            },
            {
                "text": "虫虫吉他",
                "link": "http://www.ccguitar.cn/"
            }
        ]
    },
    {
        "text": "证书考试",
        "data": [
            {
                "text": "中大网校（一建）",
                "link": "https://www.wangxiao.cn/jz1/moni/"
            },
            {
                "text": "无忧考网（一建）",
                "link": "https://www.51test.net/jianzaoshi1/st/"
            },
            {
                "text": "233网校（一建）",
                "link": "https://www.233.com/jzs1/xmgl/zhenti/20070808/190824929.html"
            },
            {
                "text": "学易网校（一建）",
                "link": "http://www.studyez.com/jianzaoshi/lnst/"
            },
            {
                "text": "希赛网（题库）",
                "link": "https://www.educity.cn/tiku/"
            },
            {
                "text": "希赛网（电子商务设计师）",
                "link": "https://www.educity.cn/rk/dzsw/index.html"
            },
            {
                "text": "电子商务设计师”职称（全国软考）",
                "link": "https://www.zhihu.com/question/268725455"
            },
            {
                "text": "电子商务设计师考试心得",
                "link": "https://zhuanlan.zhihu.com/p/58351293"
            }
        ]
    },
    {
        "text": "福利分享",
        "remark": "娱乐搞笑、心灵鸡汤、音乐歌单、书籍阅读推荐、免费资源、促销信息、免费威客",
        "data": [
            {
                "text": "就免费",
                "link": "https://www.9mf.net/"
            }
        ]
    },
    {
        "text": "体育彩PIAO",
        "data": [
            {
                "text": "中国TI彩网",
                "link": "https://www.lottery.gov.cn/"
            },
            {
                "text": "中CAI网",
                "link": "https://www.zhcw.com/"
            },
            {
                "text": "中国福利彩PIAO",
                "link": "http://www.cwl.gov.cn/"
            },
            {
                "text": "新浪彩PIAO",
                "link": "https://lottery.sina.com.cn/"
            },
            {
                "text": "500彩PIAO网",
                "link": "https://zx.500.com/ssq/"
            },
            {
                "text": "360彩PIAO",
                "link": "https://cp.360.cn/"
            },
            {
                "text": "搜狐彩PIAO",
                "link": "http://caipiao.sohu.com/lotto/fc/"
            },
            {
                "text": "网易彩PIAO",
                "link": "https://3g.163.com/touch/caipiao"
            },
            {
                "text": "番茄彩PIAO",
                "link": "lottery.html"
            }
        ]
    },
    {
        "text": "摆摊交流",
        "data": [
            {
                "text": "地摊网",
                "link": "https://www.yqbdt.com/"
            },
            {
                "text": "摆地摊贴吧",
                "link": "https://tieba.baidu.com/f?kw=%E6%91%86%E5%9C%B0%E6%91%8A"
            }
        ]
    },
    {
        "text": "跑分评测",
        "data": [
            {
                "text": "CPU天梯表",
                "link": "http://cdn.malu.me/cpu/",
                "tooltip": "马路CPU性能天梯表"
            },
            {
                "text": "笔记本CPU性能排行榜",
                "link": "https://rank.kkj.cn/mcpu3.shtml",
                "tooltip": "移动CPU性能排行榜 - 快科技天梯榜"
            }
        ]
    },
    {
        "text": "笑话段子",
        "data": [
            {
                "text": "搞笑段子",
                "link": "https://www.xuexila.com/duanzi/gaoxiaoduanzi/",
                "tooltip": ""
            },
            {
                "text": "好笑话",
                "link": "http://xiaohua.dahe.cn/",
                "tooltip": "科技与产业创新服务平台"
            },
            {
                "text": "轻松一刻",
                "link": "http://www.17989.com/xiaohua/",
                "tooltip": ""
            },
            {
                "text": "夫妻笑话",
                "link": "https://www.chazidian.com/xiaohua_2/",
                "tooltip": ""
            },
            {
                "text": "ZOL笑话大全",
                "link": "https://xiaohua.zol.com.cn/",
                "tooltip": ""
            }
        ]
    },
    {
        "text": "甜言蜜语",
        "data": [
            {
                "text": "QQ情话",
                "link": "https://www.cqxqz.com/jdjz/jdj/45537.html",
                "tooltip": ""
            }
        ]
    },
    {
        "text": "销售口才",
        "data": [
            {
                "text": "亿欧网",
                "link": "https://www.iyiou.com/",
                "tooltip": "科技与产业创新服务平台"
            }
        ]
    },
    {
        "text": "穿搭技巧",
        "data": [
            {
                "text": "着装",
                "link": "http://report.iresearch.cn/",
                "tooltip": "互联网数据资讯聚合平台"
            }
        ]
    },
    {
        "text": "健身锻炼",
        "data": [
            {
                "text": "健身锻炼",
                "link": "https://www.cqxqz.com/jdjz/jdj/45537.html",
                "tooltip": ""
            }
        ]
    },
    {
        "text": "绘画",
        "data": [
            {
                "text": "爱蛙简笔画",
                "link": "https://www.jbhdq.com/",
                "tooltip": "简笔画大全主要提供：动物简笔画、人物简笔画等画画图片大全超简单。"
            }
        ]
    },
    {
        "text": "花艺",
        "data": [
            {
                "text": "花艺",
                "link": "https://www.iyiou.com/",
                "tooltip": "科技与产业创新服务平台"
            }
        ]
    }
]