export const hotlinks = [
    {
        "text": "贝壳找房",
        "link": "https://www.ke.com/"
    },
    {
        "text": "贝壳二手房",
        "link": "https://www.ke.com/ershoufang/"
    },
    {
        "text": "链家网",
        "link": "http://www.lianjia.com"
    },
    {
        "text": "58同城房产",
        "link": "http://www.58.com/?path=house.shtml"
    },
    {
        "text": "赶集网租房",
        "link": "https://bj.ganji.com/"
    },
    {
        "text": "土巴兔装修网",
        "link": "http://www.to8to.com/"
    },
    {
        "text": "宜家",
        "link": "http://www.ikea.com/cn/zh/"
    },
];

export default [
    {
        "text": "新房/二手房",
        "data": [
            {
                "text": "贝壳找房",
                "link": "https://www.ke.com/"
            },
            {
                "text": "贝壳二手房",
                "link": "https://www.ke.com/ershoufang/"
            },
            {
                "text": "房天下",
                "link": "http://www.fang.com"
            },
            {
                "text": "焦点房地产网",
                "link": "http://www.focus.cn"
            },
            {
                "text": "搜狐焦点网",
                "link": "http://house.focus.cn/"
            },
            {
                "text": "乐居",
                "link": "http://www.leju.com/"
            },
            {
                "text": "腾讯房产",
                "link": "http://house.qq.com/"
            },
            {
                "text": "安居客",
                "link": "https://www.anjuke.com"
            },
            {
                "text": "链家网",
                "link": "http://www.lianjia.com"
            },
            {
                "text": "搜狐二手房",
                "link": "http://esf.focus.cn"
            },
            {
                "text": "凤凰房产",
                "link": "http://house.ifeng.com/"
            },
            {
                "text": "我爱我家",
                "link": "http://www.5i5j.com/"
            },
            {
                "text": "房多多",
                "link": "http://www.fangdd.com/"
            },
            {
                "text": "365房产网",
                "link": "http://www.house365.com/"
            },
            {
                "text": "淘房网",
                "link": "http://www.taofang.com.cn/"
            },
            {
                "text": "21世纪不动产",
                "link": "http://www.century21cn.com/"
            },
            {
                "text": "中原地产",
                "link": "http://www.centaline.com.cn/"
            },
            {
                "text": "吉屋",
                "link": "http://www.jiwu.com/"
            },
            {
                "text": "搜房帮",
                "link": "http://agent.fang.com/"
            },
            {
                "text": "网易房产",
                "link": "http://house.163.com/"
            },
            {
                "text": "Q房网",
                "link": "http://www.qfang.com"
            },
            {
                "text": "房价网",
                "link": "http://www.fangjia.com"
            },
            {
                "text": "房探网",
                "link": "http://www.fangtan007.com/"
            },
            {
                "text": "人民网房产",
                "link": "http://house.people.com.cn/"
            },
            {
                "text": "推推99网",
                "link": "http://www.tuitui99.com/"
            },
            {
                "text": "城市房产",
                "link": "http://www.cityhouse.cn/"
            }
        ]
    },
    {
        "text": "租房",
        "data": [
            {
                "text": "58同城房产",
                "link": "http://www.58.com/?path=house.shtml"
            },
            {
                "text": "赶集网租房",
                "link": "https://bj.ganji.com/"
            },
            {
                "text": "途家网短租",
                "link": "http://www.tujia.com/"
            },
            {
                "text": "游天下短租",
                "link": "http://www.youtx.com/"
            },
            {
                "text": "豆瓣租房小组",
                "link": "http://www.douban.com/group/search?cat=1019&q=%E7%A7%9F%E6%88%BF"
            },
            {
                "text": "小猪短租",
                "link": "http://www.xiaozhu.com/"
            },
            {
                "text": "房天下租房",
                "link": "http://zu.fang.com/"
            },
            {
                "text": "自如租房",
                "link": "http://www.ziroom.com"
            },
            {
                "text": "优客逸家",
                "link": "http://www.uoko.com"
            },
            {
                "text": "爱彼迎",
                "link": "https://zh.airbnb.com"
            },
            {
                "text": "58同城租房",
                "link": "https://58.com/chuzu/"
            },
            {
                "text": "新浪租房",
                "link": "http://bj.zufang.sina.com.cn/"
            },
            {
                "text": "百姓网房产",
                "link": "http://beijing.baixing.com/fang/"
            },
            {
                "text": "游天下短租房",
                "link": "http://www.youtx.com/"
            }
        ]
    },
    {
        "text": "房屋装修",
        "data": [
            {
                "text": "房天下装修",
                "link": "http://home.fang.com/"
            },
            {
                "text": "CCTV交换空间",
                "link": "http://tv.cctv.com/lm/jhkj/"
            },
            {
                "text": "搜狐焦点家居",
                "link": "http://home.focus.cn/"
            },
            {
                "text": "新浪家居",
                "link": "http://jiaju.sina.com.cn/"
            },
            {
                "text": "齐家装修网",
                "link": "https://www.jia.com/beijing/"
            },
            {
                "text": "一起装修网",
                "link": "http://www.17house.com/"
            },
            {
                "text": "土巴兔装修网",
                "link": "http://www.to8to.com/"
            },
            {
                "text": "太平洋家居",
                "link": "http://www.pchouse.com.cn/"
            },
            {
                "text": "中华橱柜网",
                "link": "http://www.chinachugui.com/"
            },
            {
                "text": "365建材网",
                "link": "http://www.jiancai365.cn"
            },
            {
                "text": "篱笆网装修",
                "link": "http://www.liba.com/"
            },
            {
                "text": "和家网装修",
                "link": "http://www.51hejia.com/"
            },
            {
                "text": "宜家",
                "link": "http://www.ikea.com/cn/zh/"
            },
            {
                "text": "红星美凯龙",
                "link": "http://www.chinaredstar.com/"
            },
            {
                "text": "居然之家",
                "link": "http://www.juran.com.cn/"
            },
            {
                "text": "百安居",
                "link": "http://www.bnq.com.cn/"
            },
            {
                "text": "曲美家居",
                "link": "http://www.qumei.com/"
            },
            {
                "text": "谷居家居网",
                "link": "http://guju.com.cn/"
            },
            {
                "text": "酷家乐装修",
                "link": "http://www.kujiale.com/"
            },
            {
                "text": "爱装网",
                "link": "http://www.365azw.com/"
            },
            {
                "text": "橱柜网",
                "link": "http://www.chinachugui.com/"
            },
            {
                "text": "建材采购网",
                "link": "http://www.jiancai365.cn/"
            },
            {
                "text": "筑客网",
                "link": "http://www.zhuke.com/"
            },
            {
                "text": "别墅工场",
                "link": "http://www.bieshu.com/"
            },
            {
                "text": "美间",
                "link": "https://www.meijian.io/"
            }
        ]
    },
    {
        "text": "房产工具",
        "data": [
            {
                "text": "房贷计算器",
                "link": "http://newhouse.fang.com/house/tools.htm"
            },
            {
                "text": "公积金贷款计算器",
                "link": "https://newhouse.fang.com/jsq/gjj.htm"
            },
            {
                "text": "装修预算",
                "link": "http://home.fang.com/jiancai/zongyusuan_jsq.html"
            },
            {
                "text": "税费计算器",
                "link": "https://newhouse.fang.com/jsq/sf.htm"
            },
            {
                "text": "房屋租赁合同",
                "link": "http://wenku.baidu.com/view/768d7dfafab069dc502201e6.html"
            }
        ]
    },
    {
        "text": "建筑行业",
        "data": [
            {
                "text": "住房和城乡建设部",
                "link": "http://www.mohurd.gov.cn/"
            },
            {
                "text": "ABBS建筑论坛",
                "link": "http://jianzhu.abbs.com.cn/"
            },
            {
                "text": "土木在线",
                "link": "http://www.co188.com/"
            },
            {
                "text": "地产中国网",
                "link": "http://house.china.com.cn/"
            },
            {
                "text": "拓者设计吧",
                "link": "http://www.tuozhe8.com/"
            },
            {
                "text": "筑龙网",
                "link": "http://www.zhulong.com/"
            },
            {
                "text": "中国建材网",
                "link": "http://www.bmlink.com/"
            },
            {
                "text": "中房网",
                "link": "http://www.fangchan.com/"
            },
            {
                "text": "中国建造师网",
                "link": "http://www.coc.gov.cn/"
            }
        ]
    },
    {
        "text": "房产APP",
        "data": [
            {
                "text": "安居客",
                "link": "https://www.anjuke.com/mobile"
            },
            {
                "text": "掌上链家",
                "link": "http://www.lianjia.com/client/"
            },
            {
                "text": "365淘房",
                "link": "http://app.house365.com/"
            },
            {
                "text": "透明房产",
                "link": "http://topic.funi.com/apps/web.html"
            },
            {
                "text": "芒果在线",
                "link": "http://app.517.cn/"
            },
            {
                "text": "百姓网",
                "link": "http://www.baixing.com/a/mobile"
            },
            {
                "text": "58同城",
                "link": "http://pic2.58.com/m58/app58/m_static/home.html?1462782358508"
            },
            {
                "text": "房多多",
                "link": "http://www.fangdd.com/download/app"
            },
            {
                "text": "房天下",
                "link": "http://client.3g.fang.com/http/wap/index.html"
            },
            {
                "text": "贝壳找房",
                "link": "https://www.ke.com/client"
            },
            {
                "text": "Q房网",
                "link": "https://app.qfang.com/index.html"
            },
            {
                "text": "我爱我家",
                "link": "http://www.5i5j.com/zt/app/index.html"
            },
            {
                "text": "蚂蚁短租",
                "link": "http://www.mayi.com/activity/app50"
            },
            {
                "text": "火炬租房",
                "link": "http://www.huoju365.com/"
            }
        ]
    },
    {
        "text": "地方房产",
        "data": [
            {
                "text": "365淘房",
                "link": "http://www.house365.com/"
            },
            {
                "text": "深圳房地产信息网",
                "link": "http://www.szhome.com/"
            },
            {
                "text": "中国房产超市网",
                "link": "http://www.fccs.com/"
            },
            {
                "text": "青岛新闻网房产",
                "link": "http://house.qingdaonews.com/"
            },
            {
                "text": "济南房地产网",
                "link": "http://www.jnhouse.com/"
            },
            {
                "text": "杭州快房网",
                "link": "http://www.kfw001.com/"
            },
            {
                "text": "吉屋网",
                "link": "http://www.jiwu.com/"
            }
        ]
    }
]