export const hotlinks = [
];

export default [
    {
        "text": "上海门户",
        "data": [
            {
                "text": "上海市人民政府",
                "link": "http://www.shanghai.gov.cn/"
            },
            {
                "text": "上海热线",
                "link": "http://www.online.sh.cn/"
            },
            {
                "text": "东方网",
                "link": "http://www.eastday.com/"
            },
            {
                "text": "新民网",
                "link": "http://www.xinmin.cn/"
            },
            {
                "text": "上观",
                "link": "http://www.jfdaily.com/"
            },
            {
                "text": "新民晚报",
                "link": "http://xmwb.xinmin.cn/"
            },
            {
                "text": "宽带山",
                "link": "https://www.kdslife.com/"
            },
            {
                "text": "观察者网",
                "link": "http://www.guancha.cn/"
            },
            {
                "text": "文汇网",
                "link": "http://www.whb.cn/"
            },
            {
                "text": "上海广播电视台",
                "link": "https://www.smg.cn/"
            },
            {
                "text": "百视通",
                "link": "https://www.bestv.com.cn/"
            },
            {
                "text": "第一财经",
                "link": "https://www.yicai.com/"
            }
        ]
    },
    {
        "text": "本地生活",
        "data": [
            {
                "text": "天气预报",
                "link": "http://www.weather.com.cn/shanghai/index.shtml"
            },
            {
                "text": "上海地铁",
                "link": "http://www.shmetro.com/"
            },
            {
                "text": "机动车违法查询",
                "link": "http://sh.122.gov.cn/"
            },
            {
                "text": "中国东方航空",
                "link": "http://www.ceair.com/"
            },
            {
                "text": "第一招聘网",
                "link": "http://www.01job.cn/"
            },
            {
                "text": "专家门诊",
                "link": "https://sh.haodf.com/"
            },
            {
                "text": "三甲医院大全",
                "link": "https://yyk.99.com.cn/sanjia/shanghai/"
            },
            {
                "text": "上海医保",
                "link": "http://ybj.sh.gov.cn/"
            },
            {
                "text": "上海市大学网址",
                "link": "http://hao.360.cn/sub/daxue_shanghai.html"
            },
            {
                "text": "复旦大学",
                "link": "http://www.fudan.edu.cn/"
            },
            {
                "text": "上海交通大学",
                "link": "http://www.sjtu.edu.cn/"
            },
            {
                "text": "同济大学",
                "link": "http://www.tongji.edu.cn/"
            },
            {
                "text": "华东师范大学",
                "link": "http://www.ecnu.edu.cn/"
            },
            {
                "text": "华东政法大学",
                "link": "http://www.ecupl.edu.cn/"
            },
            {
                "text": "华东理工大学",
                "link": "https://www.ecust.edu.cn/"
            },
            {
                "text": "上海师范大学",
                "link": "http://www.shnu.edu.cn/"
            },
            {
                "text": "网上房地产",
                "link": "http://www.fangdi.com.cn/"
            }
        ]
    },
    {
        "text": "各区政府",
        "data": [
            {
                "text": "浦东新区",
                "link": "http://www.pudong.gov.cn/"
            },
            {
                "text": "黄浦区",
                "link": "https://www.shhuangpu.gov.cn/"
            },
            {
                "text": "静安区",
                "link": "http://www.jingan.gov.cn/"
            },
            {
                "text": "徐汇区",
                "link": "http://www.xuhui.gov.cn/"
            },
            {
                "text": "长宁区",
                "link": "http://www.shcn.gov.cn/"
            },
            {
                "text": "普陀区",
                "link": "http://www.shpt.gov.cn/"
            },
            {
                "text": "虹口区",
                "link": "http://www.shhk.gov.cn/"
            },
            {
                "text": "杨浦区",
                "link": "http://www.shyp.gov.cn/"
            },
            {
                "text": "宝山区",
                "link": "http://www.shbsq.gov.cn/"
            },
            {
                "text": "闵行区",
                "link": "http://www.shmh.gov.cn/"
            },
            {
                "text": "嘉定区",
                "link": "http://www.jiading.gov.cn/"
            },
            {
                "text": "金山区",
                "link": "https://www.jinshan.gov.cn/"
            },
            {
                "text": "松江区",
                "link": "http://www.songjiang.gov.cn/"
            },
            {
                "text": "青浦区",
                "link": "http://www.shqp.gov.cn/"
            },
            {
                "text": "奉贤区",
                "link": "http://www.fengxian.gov.cn/"
            },
            {
                "text": "崇明区",
                "link": "http://www.shcm.gov.cn/"
            }
        ]
    }
]