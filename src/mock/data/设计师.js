export const hotlinks = [
    {
        "text": "Iconfont - 阿里巴巴矢量图标库",
        "link": "https://www.iconfont.cn/"
    },
    {
        "text": "网页颜色选择器",
        "link": "http://tool.chinaz.com/tools/pagecolor.aspx"
    },
    {
        "text": "模板王",
        "link": "http://www.mobanwang.com/",
        "tooltip": "模板王精选上万个网页模板免费下载"
    },
];

export default [
    {
        "text": "图标",
        "data": [
            {
                "text": "Bootstrap Icons",
                "link": "https://icons.bootcss.com/"
            },
            {
                "text": "Font Awesome v4 图标查询",
                "link": "http://www.fontawesome.com.cn/faicons/"
            },
            {
                "text": "Font Awesome v6 图标查询",
                "link": "https://fontawesome.com/v6.0/icons"
            },
            {
                "text": "Iconfont - 阿里巴巴矢量图标库",
                "link": "https://www.iconfont.cn/"
            }
        ]
    },
    {
        "text": "色彩",
        "data": [
            {
                "text": "HTML 颜色值",
                "link": "https://www.runoob.com/html/html-colorvalues.html"
            },
            {
                "text": "网页设计常用色彩搭配表",
                "link": "http://tool.c7sky.com/webcolor/"
            },
            {
                "text": "网页颜色选择器",
                "link": "http://tool.chinaz.com/tools/pagecolor.aspx"
            },
            {
                "text": "在线调色板",
                "link": "https://www.bejson.com/ui/getcolor/"
            }
        ]
    },
    {
        "text": "素材",
        "data": [
            {
                "text": "Easyicon",
                "link": "https://www.easyicon.net/",
                "tooltip": "提供★超过六十万个★SVG、PNG、ICO、ICNS图标搜索、图标下载服务。"
            },
            {
                "text": "爱给网",
                "link": "http://www.aigei.com/",
                "tooltip": "中国最大的数字娱乐免费素材下载网站,免费提供免费的音效配乐|3D模型|视频|游戏素材资源下载。"
            },
            {
                "text": "六图网",
                "link": "https://www.16pic.com/",
                "tooltip": "六图网-psd素材网_免费设计素材下载_正版高清图片下载库"
            },
            {
                "text": "千库网",
                "link": "https://588ku.com/",
                "tooltip": "千库网_免费png图片背景素材下载,做设计不抠图"
            },
            {
                "text": "百图汇",
                "link": "http://www.5tu.cn/",
                "tooltip": "设计素材,图片素材,设计模板,精品与原创素材网-百图汇"
            },
            {
                "text": "搜图神器",
                "link": "https://www.logosc.cn/so/",
                "tooltip": "一键搜索多家国内外知名高清免版权图库。"
            },
            {
                "text": "第一字体网",
                "link": "http://www.diyiziti.com/",
                "tooltip": "第一字体网为您提供最全的字体转换器在线转换、艺术字体在线生成器和字体下载"
            },
            {
                "text": "视觉传达",
                "link": "http://hao.shijuechuanda.com/",
                "tooltip": "设计网址导航"
            },
            {
                "text": "360图片",
                "link": "http://image.so.com/"
            },
            {
                "text": "百度图片",
                "link": "http://image.baidu.com/"
            },
            {
                "text": "昵图网",
                "link": "http://www.nipic.com/"
            },
            {
                "text": "太平洋摄影部落",
                "link": "http://dp.pconline.com.cn/"
            },
            {
                "text": "天涯贴图专区",
                "link": "http://bbs.tianya.cn/list-no04-1.shtml"
            },
            {
                "text": "天堂图片网",
                "link": "http://www.ivsky.com/"
            },
            {
                "text": "花瓣网",
                "link": "http://huaban.com/"
            },
            {
                "text": "千图网",
                "link": "http://www.58pic.com/"
            },
            {
                "text": "全景网",
                "link": "http://www.quanjing.com/"
            },
            {
                "text": "图行天下",
                "link": "http://www.photophoto.cn/"
            },
            {
                "text": "图库中国",
                "link": "http://www.tukuchina.cn/"
            },
            {
                "text": "汇图网",
                "link": "http://www.huitu.com/"
            },
            {
                "text": "站长素材",
                "link": "http://sc.chinaz.com/"
            },
            {
                "text": "站长图库",
                "link": "https://www.zztuku.com/"
            },
            {
                "text": "我图网",
                "link": "http://www.ooopic.com/"
            },
            {
                "text": "站长字体下载",
                "link": "http://font.chinaz.com/"
            },
            {
                "text": "素彩网",
                "link": "https://www.sc115.com/"
            },
            {
                "text": "CG模型网",
                "link": "http://www.cgmodel.com/"
            },
            {
                "text": "素材CNN",
                "link": "http://www.sccnn.com/"
            },
            {
                "text": "VGC",
                "link": "https://www.vcg.com/"
            },
            {
                "text": "Veer图库",
                "link": "https://www.veer.com/"
            }
        ]
    },
    {
        "text": "模板",
        "data": [
            {
                "text": "源码之家",
                "link": "https://www.mycodes.net/",
                "tooltip": "提供最新免费网站源码下载！"
            },
            {
                "text": "模板王",
                "link": "http://www.mobanwang.com/",
                "tooltip": "模板王精选上万个网页模板免费下载"
            },
            {
                "text": "CSS模板之家",
                "link": "http://www.cssmoban.com/",
                "tooltip": "CSS模板之家为你提供大量精品网页模板"
            },
            {
                "text": "模板世界",
                "link": "http://www.templatesy.com/",
                "tooltip": "模板世界，静态网站模版，免费网站模版下载"
            },
            {
                "text": "模板派",
                "link": "http://www.mobanpai.com/",
                "tooltip": "模板派提供网站模板,网页模板,企业网站模板"
            },
            {
                "text": "PageAdmin v3模板站",
                "link": "http://v3.pageadmin.net/",
                "tooltip": "PageAdmin v3模板站提供各种v3系列的网站模板和免费模板下载,广泛用于政府,学校和企业网站建设模板"
            },
            {
                "text": "模板下载吧",
                "link": "https://www.mbxzb.com/",
                "tooltip": "模板下载吧（mbxzb.com）专业提供高端网站模板"
            },
            {
                "text": "模板无忧",
                "link": "http://www.mb5u.com/",
                "tooltip": "模板无忧是国内最具人气的网站模板、网页模板下载站，提供网站模板、网页模板、程序模板下载及建站相关素材"
            },
            {
                "text": "闲鱼资源网",
                "link": "https://www.xianyuboke.com/",
                "tooltip": "闲鱼资源网：收集各种网站模板,网站建设模板 ,精品网站源码"
            },
            {
                "text": "免费网站模板网",
                "link": "https://www.freemoban.com/",
                "tooltip": "免费网站模板网提供大量的网站源码下载，HTML/织梦dedecms模板下载等"
            }
        ]
    },
    {
        "text": "海报",
        "data": [
            {
                "text": "MAKA码卡",
                "link": "https://www.maka.im/",
                "tooltip": "海量免费设计模板"
            },
            {
                "text": "DM传单设计",
                "link": "https://detail.tmall.com/item.htm?spm=a230r.1.14.82.602a7a9edrexQE&id=641828995978&ns=1&abbucket=1",
                "tooltip": "DM传单设计"
            },
            {
                "text": "淘宝.百套web登录页模板",
                "link": "https://item.taobao.com/item.htm?spm=a230r.1.14.46.11081bcdV2OrUe&id=619059470898&ns=1&abbucket=1#detail",
                "tooltip": "百套web登录页面源码注册页前端H5模板后台系统登陆静态页html"
            },
            {
                "text": "淘宝.外贸网站模板",
                "link": "https://item.taobao.com/item.htm?spm=a230r.1.14.201.4422257apjf3Sh&id=611512635155&ns=1&abbucket=1#detail",
                "tooltip": "外贸网站模板"
            }
        ]
    },
    {
        "text": "AI设计",
        "data": [
            {
                "text": "Pebblely",
                "link": "https://pebblely.com/",
                "tooltip": "自动化生成电商主图与详情设计，每月免费40张"
            },
            {
                "text": "StockImg AI",
                "link": "https://stockimg.ai/",
                "tooltip": "自动生成商品详情图片"
            },
            {
                "text": "美图秀秀AI商品图",
                "link": "https://www.x-design.com/",
                "tooltip": ""
            },
            {
                "text": "搞定设计AI商品图",
                "link": "https://www.gaoding.com/ai/ecommerce",
                "tooltip": ""
            }
        ]
    },
    {
        "text": "创意设计",
        "data": [
            {
                "text": "手写体网",
                "link": "http://shouxieti.com/"
            },
            {
                "text": "爱稀奇",
                "link": "http://www.ixiqi.com/"
            },
            {
                "text": "设计癖",
                "link": "https://www.shejipi.com/"
            },
            {
                "text": "花瓣网",
                "link": "https://huaban.com/"
            },
            {
                "text": "美图秀秀",
                "link": "https://pc.meitu.com/"
            },
            {
                "text": "堆糖",
                "link": "http://www.duitang.com/"
            },
            {
                "text": "场库",
                "link": "http://www.vmovier.com/"
            },
            {
                "text": "快剪辑",
                "link": "https://www.kuaijianji.com/?ch=seo_wl_hao360ku"
            }
        ]
    },
    {
        "text": "设计名站",
        "data": [
            {
                "text": "红动中国",
                "link": "https://www.redocn.com/",
                "tooltip": "红动中国 - 找设计！上红动"
            },
            {
                "text": "UI 中国",
                "link": "https://www.ui.cn/",
                "tooltip": "UI中国用户体验设计平台"
            },
            {
                "text": "站酷",
                "link": "https://www.zcool.com.cn/",
                "tooltip": "设计师互动平台 - 打开站酷，发现更好的设计！"
            },
            {
                "text": "Dribbble 追波",
                "link": "https://dribbble.com/",
                "tooltip": "Discover the World’s Top Designers & Creative Professionals"
            },
            {
                "text": "UISDC 优设网",
                "link": "http://www.uisdc.com/",
                "tooltip": "设计师交流学习平台 - 看设计文章，学软件教程，找灵感素材，尽在优设网！"
            },
            {
                "text": "花瓣网",
                "link": "https://huaban.com/",
                "tooltip": "陪你做生活的设计师（创意灵感天堂，搜索、发现设计灵感、设计素材）"
            },
            {
                "text": "IdN™（中文版）",
                "link": "https://cn.idnworld.com/",
                "tooltip": "IdN是一本為國際性雜誌，名符其實的成為一個國際設計師網絡。"
            },
            {
                "text": "Arting365",
                "link": "http://arting365.com/"
            },
            {
                "text": "爱视觉",
                "link": "http://www.shijue.me/"
            },
            {
                "text": "设计之家",
                "link": "http://www.sj33.cn/"
            },
            {
                "text": "优设",
                "link": "http://www.uisdc.com/"
            },
            {
                "text": "设计之窗",
                "link": "http://www.333cn.com/"
            },
            {
                "text": "设计师网",
                "link": "http://www.shejis.com/"
            },
            {
                "text": "UI制造者",
                "link": "http://www.uimaker.com/"
            },
            {
                "text": "视觉同盟",
                "link": "http://www.visionunion.com/"
            },
            {
                "text": "优优教程网",
                "link": "https://uiiiuiii.com/"
            },
            {
                "text": "一幅图",
                "link": "http://www.yifutu.com/"
            },
            {
                "text": "纳金网",
                "link": "http://www.narkii.com/"
            },
            {
                "text": "拓者设计吧",
                "link": "http://www.tuozhe8.com/"
            },
            {
                "text": "室内设计联盟",
                "link": "https://www.cool-de.com/"
            },
            {
                "text": "99艺术网",
                "link": "http://www.99ys.com/"
            },
            {
                "text": "翼狐网",
                "link": "https://www.yiihuu.com/"
            },
            {
                "text": "POP服装趋势网",
                "link": "http://www.pop-fashion.com/"
            },
            {
                "text": "雷锋PPT网",
                "link": "http://www.lfppt.com/"
            },
            {
                "text": "二维码工坊",
                "link": "https://www.2weima.com/"
            },
            {
                "text": "设计达人",
                "link": "https://www.shejidaren.com/"
            },
            {
                "text": "DOOOOR设计网",
                "link": "https://www.doooor.com/"
            },
            {
                "text": "设计之窗",
                "link": "http://www.333cn.com/"
            },
            {
                "text": "创客站",
                "link": "https://www.chuangkit.com/"
            },
            {
                "text": "快剪辑",
                "link": "https://www.kuaijianji.com/?ch=seo_wl_hao360sheji"
            }
        ]
    }
]