export const hotlinks = [
    {
        "text": "央视网",
        "link": "http://www.cctv.com/"
    },
    {
        "text": "中华网",
        "link": "http://www.china.com/"
    },
    {
        "text": "环球军事网",
        "link": "http://mil.huanqiu.com/"
    },
    {
        "text": "中华军事",
        "link": "http://military.china.com/"
    },
    {
        "text": "第一财经",
        "link": "http://www.yicai.com/"
    },
    {
        "text": "中国企业家网",
        "link": "http://www.iceo.com.cn/"
    },
];

export default [
    {
        "text": "新闻网站",
        "data": [
            {
                "text": "环球网",
                "link": "http://www.huanqiu.com/"
            },
            {
                "text": "搜狐新闻",
                "link": "http://news.sohu.com/"
            },
            {
                "text": "网易新闻",
                "link": "http://news.163.com/"
            },
            {
                "text": "凤凰网",
                "link": "http://www.ifeng.com/"
            },
            {
                "text": "腾讯新闻",
                "link": "http://news.qq.com/"
            },
            {
                "text": "人民网",
                "link": "http://www.people.com.cn/"
            },
            {
                "text": "新华网",
                "link": "http://www.xinhuanet.com/"
            },
            {
                "text": "央视网",
                "link": "http://www.cctv.com/"
            },
            {
                "text": "中国网",
                "link": "http://www.china.com.cn/"
            },
            {
                "text": "国际在线",
                "link": "http://www.cri.cn/"
            },
            {
                "text": "中国日报网",
                "link": "http://cn.chinadaily.com.cn/"
            },
            {
                "text": "中国经济网",
                "link": "http://www.ce.cn/"
            },
            {
                "text": "光明网",
                "link": "http://www.gmw.cn/"
            },
            {
                "text": "央广网",
                "link": "http://www.cnr.cn/"
            },
            {
                "text": "中国军网",
                "link": "http://www.81.cn/"
            },
            {
                "text": "网信网",
                "link": "http://www.cac.gov.cn/"
            },
            {
                "text": "中国新闻网",
                "link": "http://www.chinanews.com.cn/"
            },
            {
                "text": "中华网",
                "link": "http://www.china.com/"
            },
            {
                "text": "海外网",
                "link": "http://www.haiwainet.cn/"
            },
            {
                "text": "央视网新闻",
                "link": "http://news.cctv.com/"
            },
            {
                "text": "中国政府网",
                "link": "http://www.gov.cn/index.htm"
            },
            {
                "text": "中国青年报",
                "link": "http://www.cyol.net/"
            },
            {
                "text": "南风窗",
                "link": "http://www.nfcmag.com/"
            },
            {
                "text": "羊城晚报",
                "link": "http://www.ycwb.com/"
            },
            {
                "text": "南方周末",
                "link": "http://www.infzm.com/"
            },
            {
                "text": "半月谈",
                "link": "http://www.banyuetan.org/"
            },
            {
                "text": "中国新闻周刊网",
                "link": "http://www.inewsweek.cn/"
            }
        ]
    },
    {
        "text": "地方新闻",
        "data": [
            {
                "text": "广州日报大洋网",
                "link": "http://www.dayoo.com/"
            },
            {
                "text": "深圳之窗",
                "link": "http://www.shenchuang.com/"
            },
            {
                "text": "重庆晚报",
                "link": "http://www.cqwb.com.cn/"
            },
            {
                "text": "深圳新闻网",
                "link": "http://www.sznews.com/"
            },
            {
                "text": "浙江在线",
                "link": "http://www.zjol.com.cn/"
            },
            {
                "text": "福建东南网",
                "link": "http://www.fjsen.com/"
            },
            {
                "text": "大河网",
                "link": "http://www.dahe.cn/"
            },
            {
                "text": "上海热线",
                "link": "http://www.online.sh.cn/"
            },
            {
                "text": "东方网",
                "link": "http://www.eastday.com/"
            },
            {
                "text": "南方网",
                "link": "http://www.southcn.com/"
            },
            {
                "text": "广西新闻网",
                "link": "http://www.gxnews.com.cn/"
            },
            {
                "text": "新京报网",
                "link": "http://www.bjnews.com.cn/"
            },
            {
                "text": "华声在线",
                "link": "http://www.voc.com.cn/"
            },
            {
                "text": "华龙网",
                "link": "http://www.cqnews.net/"
            },
            {
                "text": "扬子晚报",
                "link": "http://www.yangtse.com/"
            },
            {
                "text": "荆楚网",
                "link": "http://www.cnhubei.com/"
            },
            {
                "text": "看看新闻网",
                "link": "http://www.kankanews.com/"
            },
            {
                "text": "新周刊",
                "link": "https://www.neweekly.com.cn/"
            }
        ]
    },
    {
        "text": "军事要闻",
        "data": [
            {
                "text": "环球军事网",
                "link": "http://mil.huanqiu.com/"
            },
            {
                "text": "凤凰军事",
                "link": "http://news.ifeng.com/mil/"
            },
            {
                "text": "新浪军事",
                "link": "http://mil.news.sina.com.cn/"
            },
            {
                "text": "中华军事",
                "link": "http://military.china.com/"
            },
            {
                "text": "西陆军事",
                "link": "http://junshi.xilu.com/"
            },
            {
                "text": "米尔军情",
                "link": "http://www.miercn.com/"
            },
            {
                "text": "搜狐军事",
                "link": "http://mil.sohu.com/"
            },
            {
                "text": "网易军事",
                "link": "http://war.163.com/"
            }
        ]
    },
    {
        "text": "体育新闻",
        "data": [
            {
                "text": "NBA中国",
                "link": "http://china.nba.com/"
            },
            {
                "text": "虎扑体育",
                "link": "https://www.hupu.com/"
            },
            {
                "text": "网易体育",
                "link": "http://sports.163.com/"
            },
            {
                "text": "搜狐体育",
                "link": "http://sports.sohu.com/"
            },
            {
                "text": "直播吧",
                "link": "https://www.zhibo8.cc/"
            },
            {
                "text": "PPTV体育",
                "link": "http://sports.pptv.com/"
            },
            {
                "text": "CCTV体育",
                "link": "http://sports.cctv.com/"
            }
        ]
    },
    {
        "text": "时事论坛",
        "data": [
            {
                "text": "人民网强国社区",
                "link": "http://bbs.people.com.cn"
            },
            {
                "text": "中国共产党新闻网",
                "link": "http://cpc.people.com.cn/"
            },
            {
                "text": "天涯社区国际观察",
                "link": "http://www.tianya.cn/Publicforum/ArticlesList/0/worldlook.shtml"
            },
            {
                "text": "观察者网",
                "link": "http://www.guancha.cn/"
            },
            {
                "text": "爱思想",
                "link": "http://www.aisixiang.com/"
            }
        ]
    },
    {
        "text": "财经商业",
        "data": [
            {
                "text": "第一财经",
                "link": "http://www.yicai.com/"
            },
            {
                "text": "财新网",
                "link": "http://www.caixin.com"
            },
            {
                "text": "央视网经济",
                "link": "http://jingji.cctv.com/"
            },
            {
                "text": "金融界网",
                "link": "http://www.zgjrjw.com/"
            },
            {
                "text": "经济观察网",
                "link": "http://www.eeo.com.cn/"
            },
            {
                "text": "每日经济新闻",
                "link": "http://www.nbd.com.cn/"
            },
            {
                "text": "观点地产网",
                "link": "http://www.guandian.cn/"
            },
            {
                "text": "经济参考报",
                "link": "http://www.jjckb.cn"
            },
            {
                "text": "投中网",
                "link": "http://www.chinaventure.com.cn/"
            },
            {
                "text": "商业评论网",
                "link": "http://www.ebusinessreview.cn/"
            },
            {
                "text": "中国企业家网",
                "link": "http://www.iceo.com.cn/"
            },
            {
                "text": "中国经营网",
                "link": "http://www.cb.com.cn/"
            },
            {
                "text": "电鳗快报",
                "link": "http://www.dmkb.net/"
            },
            {
                "text": "i黑马网",
                "link": "http://www.iheima.com"
            }
        ]
    },
    {
        "text": "小说网站",
        "data": [
            {
                "text": "起点中文网",
                "link": "https://www.qidian.com"
            },
            {
                "text": "起点女生网",
                "link": "https://www.qdmm.com"
            },
            {
                "text": "小说阅读网",
                "link": "https://www.readnovel.com"
            },
            {
                "text": "红袖添香",
                "link": "https://www.hongxiu.com"
            },
            {
                "text": "创世中文网",
                "link": "http://chuangshi.qq.com"
            },
            {
                "text": "17K小说网",
                "link": "http://www.17k.com"
            },
            {
                "text": "言情小说吧",
                "link": "https://www.xs8.cn"
            },
            {
                "text": "潇湘书院",
                "link": "http://www.xxsy.net"
            },
            {
                "text": "纵横中文网",
                "link": "http://www.zongheng.com"
            },
            {
                "text": "飞卢小说网",
                "link": "https://b.faloo.com"
            },
            {
                "text": "铁血读书",
                "link": "https://book.tiexue.net"
            },
            {
                "text": "咪咕阅读",
                "link": "https://www.migu.cn/read.html"
            },
            {
                "text": "书海小说网",
                "link": "http://www.shuhai.com"
            },
            {
                "text": "连城读书",
                "link": "http://www.lcread.com"
            },
            {
                "text": "书香电子书",
                "link": "http://www.sxcnw.net"
            },
            {
                "text": "红薯网",
                "link": "https://www.hongshu.com"
            },
            {
                "text": "凌云文学网",
                "link": "http://www.lingyun5.com"
            },
            {
                "text": "网易云阅读",
                "link": "http://yuedu.163.com"
            },
            {
                "text": "磨铁中文网",
                "link": "https://www.laikan.com/"
            },
            {
                "text": "SF轻小说",
                "link": "http://book.sfacg.com"
            },
            {
                "text": "话本小说网",
                "link": "http://www.ihuaben.com"
            },
            {
                "text": "逐浪网",
                "link": "http://www.zhulang.com"
            },
            {
                "text": "锦文小说网",
                "link": "https://jw.laikan.com/"
            },
            {
                "text": "四月天",
                "link": "http://www.4yt.net"
            },
            {
                "text": "凤鸣轩",
                "link": "http://www.fmx.cn"
            },
            {
                "text": "逸云书院",
                "link": "http://yynovel.laikan.com/index"
            },
            {
                "text": "醉唐中文网",
                "link": "http://www.zuitang.com"
            },
            {
                "text": "塔读文学",
                "link": "https://www.tadu.com/"
            },
            {
                "text": "墨墨言情网",
                "link": "https://mm.laikan.com/"
            },
            {
                "text": "八月居",
                "link": "http://www.bayueju.com"
            },
            {
                "text": "云阅文学",
                "link": "http://www.iyunyue.com"
            },
            {
                "text": "黑岩网",
                "link": "https://www.heiyan.com"
            },
            {
                "text": "天下书盟",
                "link": "http://www.fbook.net/"
            },
            {
                "text": "云起书院",
                "link": "http://yunqi.qq.com"
            },
            {
                "text": "乐读电子书",
                "link": "http://www.txtbook.com.cn/"
            }
        ]
    },
    {
        "text": "文化文学",
        "data": [
            {
                "text": "简书",
                "link": "https://www.jianshu.com"
            },
            {
                "text": "知音网",
                "link": "http://www.zhiyin.cn"
            },
            {
                "text": "龙腾网",
                "link": "http://www.ltaaa.com"
            },
            {
                "text": "豆瓣读书",
                "link": "https://book.douban.com"
            },
            {
                "text": "作文网",
                "link": "http://www.zuowen.com"
            },
            {
                "text": "雨枫轩",
                "link": "http://www.rain8.com"
            },
            {
                "text": "凤凰读书",
                "link": "https://culture.ifeng.com/"
            },
            {
                "text": "搜狐文化",
                "link": "https://cul.sohu.com/"
            }
        ]
    },
    {
        "text": "在线听书",
        "data": [
            {
                "text": "喜马拉雅",
                "link": "https://www.ximalaya.com"
            },
            {
                "text": "天方听书网",
                "link": "http://www.tingbook.com"
            },
            {
                "text": "蜻蜓FM",
                "link": "https://www.qingting.fm"
            },
            {
                "text": "懒人畅听",
                "link": "http://www.lrts.me"
            },
            {
                "text": "评书吧",
                "link": "http://www.pingshu8.com"
            }
        ]
    },
    {
        "text": "看书APP",
        "data": [
            {
                "text": "熊猫看书",
                "link": "http://www.xmkanshu.com"
            },
            {
                "text": "Kindle电子书",
                "link": "https://www.amazon.cn/Kindle商店/b/ref=topnav_storetab_kinc?node=116087071"
            },
            {
                "text": "多看阅读",
                "link": "http://www.duokan.com"
            }
        ]
    },
    {
        "text": "文档文库",
        "data": [
            {
                "text": "豆丁",
                "link": "https://www.docin.com"
            },
            {
                "text": "国家图书馆",
                "link": "http://www.nlc.cn"
            },
            {
                "text": "百度文库",
                "link": "https://wenku.baidu.com/"
            }
        ]
    },
]