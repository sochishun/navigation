export const hotlinks = [
    { "text": "中国志愿者网", "link": "http://www.zgzyz.org.cn/" },
    { "text": "公益时报", "link": "http://www.gongyishibao.com/" },
    { "text": "中国红十字会总会", "link": "http://weibo.com/u/2205860842" },
    { "text": "微公益", "link": "http://gongyi.weibo.com/" },
    { "text": "中国扶贫基金会", "link": "http://www.cfpa.org.cn/" },
];

export default [
    {
        "text": "公益项目", "data": [
            { "text": "宝贝回家", "link": "http://www.baobeihuijia.com/" },
            { "text": "免费午餐", "link": "http://www.mianfeiwucan.org/" },
            { "text": "格桑花西部助学", "link": "http://www.gesanghua.org/" },
            { "text": "青少年维权网", "link": "http://www.chinachild.org/" }
        ]
    },
    {
        "text": "公益资讯", "data": [
            { "text": "中国志愿者网", "link": "http://www.zgzyz.org.cn/" },
            { "text": "央视公益", "link": "http://gongyi.cctv.com/" },
            { "text": "腾讯公益", "link": "http://gongyi.qq.com/" },
            { "text": "搜狐公益", "link": "http://gongyi.sohu.com/" },
            { "text": "网易公益", "link": "http://gongyi.163.com/" },
            { "text": "人民网公益", "link": "http://gongyi.people.com.cn/GB/index.html" },
            { "text": "新华公益", "link": "http://www.xinhuanet.com/gongyi/" },
            { "text": "新浪公益", "link": "http://gongyi.sina.com.cn/" },
            { "text": "公益时报", "link": "http://www.gongyishibao.com/" }
        ]
    },
    {
        "text": "公益微博", "data": [
            { "text": "中国红十字会总会", "link": "http://weibo.com/u/2205860842" },
            { "text": "微公益", "link": "http://gongyi.weibo.com/" },
            { "text": "壹基金救援联盟", "link": "http://weibo.com/bj5885" },
            { "text": "青少年发展基金会", "link": "http://weibo.com/u/1913886700" },
            { "text": "蓝天救援队", "link": "http://weibo.com/lantianjiuyuandui" }
        ]
    }, {
        "text": "公益基金", "data": [
            { "text": "中国社会福利基金会", "link": "http://www.cswef.org/" },
            { "text": "联合国儿童基金会", "link": "https://www.unicef.org/zh/" },
            { "text": "壹基金", "link": "http://www.onefoundation.cn/" },
            { "text": "中国扶贫基金会", "link": "http://www.cfpa.org.cn/" },
            { "text": "基金会中心网", "link": "http://www.foundationcenter.org.cn/" }
        ]
    }
]