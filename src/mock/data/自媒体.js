export const hotlinks = [
    {
        "text": "西瓜",
        "link": "https://www.ixigua.com/"
    },
    {
        "text": "抖音",
        "link": "https://www.douyin.com/"
    },
    {
        "text": "快手",
        "link": "https://www.kuaishou.com/"
    },
    {
        "text": "小红书",
        "link": "https://www.xiaohongshu.com/explore"
    }
];

export default [
    {
        "text": "短视频",
        "data": [
            {
                "text": "西瓜",
                "link": "https://www.ixigua.com/"
            },
            {
                "text": "抖音",
                "link": "https://www.douyin.com/"
            },
            {
                "text": "快手",
                "link": "https://www.kuaishou.com/"
            },
            {
                "text": "小红书",
                "link": "https://www.xiaohongshu.com/explore"
            }
        ]
    }
]