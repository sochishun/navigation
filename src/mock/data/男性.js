export const hotlinks = [
    {
        "text": "汽车之家",
        "link": "http://www.autohome.com.cn/"
    },
    {
        "text": "易车网",
        "link": "http://www.bitauto.com/"
    },
    {
        "text": "搜狐体育",
        "link": "http://sports.sohu.com/"
    },
    {
        "text": "钓鱼网",
        "link": "http://www.diaoyu123.com"
    },
];

export default [
    {
        "text": "汽车综合",
        "data": [
            {
                "text": "搜狐汽车",
                "link": "http://auto.sohu.com/"
            },
            {
                "text": "汽车之家",
                "link": "http://www.autohome.com.cn/"
            },
            {
                "text": "易车网",
                "link": "http://www.bitauto.com/"
            },
            {
                "text": "太平洋汽车网",
                "link": "http://www.pcauto.com.cn/"
            },
            {
                "text": "爱卡汽车",
                "link": "http://www.xcar.com.cn/"
            },
            {
                "text": "腾讯汽车",
                "link": "https://new.qq.com/ch/auto/"
            },
            {
                "text": "凤凰汽车",
                "link": "http://auto.ifeng.com/"
            },
            {
                "text": "新浪汽车",
                "link": "http://auto.sina.com.cn/"
            },
            {
                "text": "越野E族",
                "link": "http://www.fblife.com/"
            },
            {
                "text": "驾驶员考试网",
                "link": "http://www.jsyks.com/"
            },
            {
                "text": "网易汽车",
                "link": "http://auto.163.com/"
            },
            {
                "text": "网通社汽车",
                "link": "http://auto.news18a.com/"
            },
            {
                "text": "网上车市",
                "link": "http://www.cheshi.com/"
            },
            {
                "text": "卡车之家",
                "link": "http://www.360che.com/"
            },
            {
                "text": "驾校一点通",
                "link": "http://www.jxedt.com/"
            },
            {
                "text": "车问网",
                "link": "http://www.chewen.com/"
            },
            {
                "text": "车讯汽车网",
                "link": "http://www.chexun.com/"
            },
            {
                "text": "中国汽车消费网",
                "link": "http://www.315che.com/"
            },
            {
                "text": "新车评网",
                "link": "http://www.xincheping.com/"
            },
            {
                "text": "亚讯车网",
                "link": "http://www.yescar.cn/"
            },
            {
                "text": "拆车坊",
                "link": "http://chaiche.chexun.com/"
            },
            {
                "text": "神州租车",
                "link": "http://www.zuche.com/"
            },
            {
                "text": "一嗨租车",
                "link": "http://www.1hai.cn/"
            }
        ]
    },
    {
        "text": "二手车网址",
        "data": [
            {
                "text": "淘车网",
                "link": "http://www.taoche.com/"
            },
            {
                "text": "二手车之家",
                "link": "http://www.che168.com/"
            },
            {
                "text": "爱卡二手车",
                "link": "http://used.xcar.com.cn/"
            },
            {
                "text": "网上车市二手车",
                "link": "http://ershouche.cheshi.com/"
            },
            {
                "text": "华夏二手车",
                "link": "http://www.hx2car.com/"
            },
            {
                "text": "人人车",
                "link": "http://www.renrenche.com/"
            },
            {
                "text": "58二手汽车",
                "link": "http://www.58.com/?path=ershouche/"
            },
            {
                "text": "百姓网二手车",
                "link": "http://www.baixing.com/?bannerId=935&profile=ershouche&return=/cheliang/"
            }
        ]
    },
    {
        "text": "体育综合",
        "data": [
            {
                "text": "腾讯体育",
                "link": "http://sports.qq.com/"
            },
            {
                "text": "虎扑体育",
                "link": "http://www.hupu.com/"
            },
            {
                "text": "搜狐体育",
                "link": "http://sports.sohu.com/"
            },
            {
                "text": "网易体育",
                "link": "http://sports.163.com/"
            },
            {
                "text": "体坛网",
                "link": "http://www.titan24.com/"
            },
            {
                "text": "凤凰体育",
                "link": "http://sports.ifeng.com/"
            },
            {
                "text": "直播吧",
                "link": "http://www.zhibo8.cc/"
            },
            {
                "text": "东方体育",
                "link": "http://sports.eastday.com/"
            },
            {
                "text": "国家体育总局",
                "link": "http://www.sport.gov.cn/"
            },
            {
                "text": "NBA中国",
                "link": "http://china.nba.com/"
            },
            {
                "text": "中国蓝协",
                "link": "http://www.cba.net.cn/"
            },
            {
                "text": "中国足球协会",
                "link": "http://www.thecfa.cn/"
            },
            {
                "text": "中国体彩网",
                "link": "http://www.lottery.gov.cn/"
            },
            {
                "text": "体育热点新闻",
                "link": "http://sports.hao.360.cn/#cid=sport"
            },
            {
                "text": "捷报比分",
                "link": "http://www.nowscore.com/"
            },
            {
                "text": "7M体育",
                "link": "http://www.7m.cn/"
            },
            {
                "text": "央视体育",
                "link": "http://sports.cctv.com/"
            },
            {
                "text": "PPTV体育",
                "link": "http://sports.pptv.com/"
            },
            {
                "text": "雷速体育",
                "link": "https://www.leisu.com/"
            },
            {
                "text": "雪缘园",
                "link": "http://www.gooooal.com/"
            },
            {
                "text": "懂球帝",
                "link": "http://www.dongqiudi.com/"
            },
            {
                "text": "优酷体育视频",
                "link": "http://sports.youku.com/"
            },
            {
                "text": "搜狐体育视频",
                "link": "http://tv.sohu.com/sports/"
            },
            {
                "text": "爱奇艺体育",
                "link": "http://sports.iqiyi.com/"
            },
            {
                "text": "企鹅直播",
                "link": "http://live.qq.com/"
            },
            {
                "text": "广东体育频道",
                "link": "http://sports.gdtv.cn/"
            },
            {
                "text": "YY体育直播",
                "link": "http://www.yy.com/sport"
            },
            {
                "text": "出奇体育",
                "link": "https://www.chuqi.com/"
            }
        ]
    },
    {
        "text": "热点赛事",
        "data": [
            {
                "text": "NBA",
                "link": "http://china.nba.com/"
            },
            {
                "text": "CBA",
                "link": "https://sports.163.com/cba/"
            },
            {
                "text": "欧冠",
                "link": "https://sports.sohu.com/s/uefacl"
            },
            {
                "text": "英超",
                "link": "https://sports.163.com/yc/"
            },
            {
                "text": "西甲",
                "link": "https://sports.163.com/xj/"
            },
            {
                "text": "意甲",
                "link": "https://sports.163.com/yj/"
            },
            {
                "text": "中超",
                "link": "https://sports.sohu.com/s/csl"
            },
            {
                "text": "德甲",
                "link": "https://sports.sohu.com/s/bundesliga"
            },
            {
                "text": "欧洲杯",
                "link": "https://euro2020.sohu.com/"
            },
            {
                "text": "美洲杯",
                "link": "https://sports.sohu.com/s/afac"
            },
            {
                "text": "法甲",
                "link": "https://sports.sohu.com/s/ligue1"
            },
            {
                "text": "亚冠",
                "link": "https://sports.sohu.com/s/afccl"
            },
            {
                "text": "中甲",
                "link": "https://sports.sohu.com/s/ccl"
            },
            {
                "text": "电竞",
                "link": "https://sports.sohu.com/s/e_sport"
            }
        ]
    },
    {
        "text": "足球",
        "data": [
            {
                "text": "虎扑足球",
                "link": "http://soccer.hupu.com/"
            },
            {
                "text": "搜狐欧美足球",
                "link": "http://sports.sohu.com/guojizuqiu.shtml"
            },
            {
                "text": "搜狐国足",
                "link": "http://sports.sohu.com/guoneizuqiu.shtml"
            },
            {
                "text": "网易国际足球",
                "link": "http://sports.163.com/world"
            },
            {
                "text": "网易中国足球",
                "link": "http://sports.163.com/china/"
            },
            {
                "text": "球探体育",
                "link": "http://www.win007.com/"
            }
        ]
    },
    {
        "text": "篮球",
        "data": [
            {
                "text": "虎扑篮球",
                "link": "http://nba.hupu.com/"
            },
            {
                "text": "搜狐NBA",
                "link": "http://sports.sohu.com/nba.shtml"
            },
            {
                "text": "腾讯NBA",
                "link": "http://sports.qq.com/nba/"
            },
            {
                "text": "央视NBA",
                "link": "http://sports.cctv.com/nba/"
            },
            {
                "text": "网易NBA",
                "link": "http://sports.163.com/nba/"
            },
            {
                "text": "搜狐CBA",
                "link": "http://cbachina.sports.sohu.com/"
            }
        ]
    },
    {
        "text": "其它体育项目",
        "data": [
            {
                "text": "爱羽客",
                "link": "http://www.aiyuke.com/"
            },
            {
                "text": "中羽在线网",
                "link": "http://www.badmintoncn.com/"
            },
            {
                "text": "钓鱼网",
                "link": "http://www.diaoyu123.com"
            },
            {
                "text": "海峡钓鱼网",
                "link": "https://bbs.hx36.net/forum.php"
            },
            {
                "text": "拳击航母",
                "link": "http://www.qjhm.net/"
            }
        ]
    },
]