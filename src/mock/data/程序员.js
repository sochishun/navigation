export const hotlinks = [
    {
        "text": "ThinkPHP 官方网站",
        "link": "https://www.thinkphp.cn/",
        "tooltip": "ThinkPHP框架"
    },
    {
        "text": "Bootstrap 5 中文文档",
        "link": "https://v5.bootcss.com/"
    },
    {
        "text": "Element-Plus 官方文档",
        "link": "http://element-plus.org/"
    },
    {
        "text": "Apache ECharts",
        "link": "https://echarts.apache.org/zh/index.html"
    },
    {
        "text": "存在码",
        "link": "https://cunzaima.cn/",
        "title": "一个致力于为中文开发者提供学习资源的平台"
    },
];


export default [
    {
        "text": "PHP",
        "data": [
            {
                "text": "PHP 官网",
                "link": "https://www.php.net/"
            },
            {
                "text": "PHP 教程",
                "link": "https://www.runoob.com/php/php-tutorial.html"
            },
            {
                "text": "PHP PSR 标准规范",
                "link": "https://learnku.com/docs/psr"
            },
            {
                "text": "FastAdmin 开发者文档",
                "link": "https://doc.fastadmin.net/developer"
            },
            {
                "text": "ThinkPHP 官方网站",
                "link": "https://www.thinkphp.cn/",
                "tooltip": "ThinkPHP框架"
            },
            {
                "text": "ThinkPHP 6.0 完全开发手册",
                "link": "https://www.kancloud.cn/manual/thinkphp6_0/1037479"
            },
            {
                "text": "Laravel 官方网站",
                "link": "https://laravel.com/",
                "tooltip": "The PHP Framework For Web Artisans"
            },
            {
                "text": "Laravel 10 中文文档",
                "link": "https://learnku.com/docs/laravel/10.x"
            },
            {
                "text": "Laravel 速查表",
                "link": "https://learnku.com/docs/laravel-cheatsheet/10.x"
            },
            {
                "text": "Laravel Admin 文档",
                "link": "https://laravel-admin.org/"
            },
            {
                "text": "phinx 官方英文手册",
                "link": "https://book.cakephp.org/phinx/0/en/index.html"
            },
            {
                "text": "phinx 中文手册",
                "link": "https://tsy12321.gitbooks.io/phinx-doc/content/"
            },
            {
                "text": "GraphQL PHP 中文文档",
                "link": "https://learnku.com/docs/graphql-php"
            },
            {
                "text": "Twig 模板引擎官方文档",
                "link": "https://twig.symfony.com/"
            }
        ]
    },
    {
        "text": "JAVA",
        "data": [
            {
                "text": "JDK 11 在线中文手册",
                "link": "https://www.runoob.com/manual/jdk11api/index.html",
                "tooltip": "JDK 11 在线中文手册"
            },
            {
                "text": "spring",
                "link": "https://spring.io/",
                "tooltip": "spring 官方网站"
            },
            {
                "text": "Spring Boot 官方教程",
                "link": "https://spring.io/projects/spring-boot",
                "tooltip": "Spring Boot 官方教程"
            },
            {
                "text": "Jboot",
                "link": "https://jboot.io/",
                "tooltip": "Jboot 官方网站"
            },
            {
                "text": "JFinal",
                "link": "https://www.jfinal.com/",
                "tooltip": "JFinal 极速开发"
            },
            {
                "text": "Spring Boot 2.x基础教程",
                "link": "https://blog.didispace.com/spring-boot-learning-2x/",
                "tooltip": "程序猿DD | Java | Spring Boot | Spring Cloud | 最新干货分享"
            },
            {
                "text": "Spring Boot框架入门教程（快速学习版）",
                "link": "http://c.biancheng.net/spring_boot/",
                "tooltip": "C语言中文网：C语言程序设计门户网站(入门教程、编程软件)"
            }
        ]
    },
    {
        "text": "Linux",
        "data": [
            {
                "text": "常用 Linux 命令",
                "link": "https://www.dute.org/linux-command"
            },
            {
                "text": "Linux 教程",
                "link": "https://www.runoob.com/linux/linux-tutorial.html"
            },
            {
                "text": "Docker 教程",
                "link": "https://www.runoob.com/docker/docker-tutorial.html"
            }
        ]
    },
    {
        "text": "性能测试",
        "data": [
            {
                "text": "Thinkphp 单元测试手册",
                "link": "https://www.kancloud.cn/code7/tpunit/220874",
                "tooltip": "chcp 65001"
            },
            {
                "text": "PHPUnit 手册",
                "link": "http://www.phpunit.cn/manual/4.8/zh_cn/index.html",
                "tooltip": "php think unit --filter ExampleTest --testdox-html public/unitreport.html"
            },
            {
                "text": "MySQL 性能优化的最佳 21 条经验",
                "link": "http://www.thinkphp.cn/topic/54536.html"
            },
            {
                "text": "我必须告诉大家的 MySQL 优化原理",
                "link": "https://www.jianshu.com/p/d7665192aaaf"
            },
            {
                "text": "JMeter 必知必会",
                "link": "https://zhuanlan.zhihu.com/p/72905562"
            },
            {
                "text": "JMeter 教程网",
                "link": "https://www.hissummer.com/"
            }
        ]
    },
    {
        "text": "持续集成",
        "data": [
            {
                "text": "Kubernetes 中文文档",
                "link": "http://docs.kubernetes.org.cn/"
            },
            {
                "text": "Jenkins 中文文档",
                "link": "https://www.w3cschool.cn/jenkins/"
            },
            {
                "text": "Jenkins 发布 PHP 代码",
                "link": "https://blog.csdn.net/aoli_shuai/article/details/82975557"
            },
            {
                "text": "jenkins-php 持续集成和自动化测试",
                "link": "https://blog.csdn.net/yxwb1253587469/article/details/52635414"
            },
            {
                "text": "基于Jenkins 实现php项目的自动化测试、自动打包和自动部署",
                "link": "https://blog.csdn.net/weixin_34038293/article/details/91783674"
            },
            {
                "text": "jenkins-php 插件",
                "link": "http://jenkins-php.org/installation.html"
            },
            {
                "text": "Jenkins 中文社区",
                "link": "https://jenkins-zh.cn/"
            },
            {
                "text": "PHP_CodeSniffer",
                "link": "https://github.com/squizlabs/PHP_CodeSniffer"
            }
        ]
    },
    {
        "text": "HTML",
        "data": [
            {
                "text": "HTML 参考手册",
                "link": "https://www.runoob.com/tags/html-reference.html"
            },
            {
                "text": "HTML DOM 教程",
                "link": "https://www.runoob.com/htmldom/htmldom-tutorial.html"
            },
            {
                "text": "HTML 符号实体参考手册",
                "link": "https://www.runoob.com/tags/html-symbols.html"
            },
            {
                "text": "HTML ISO-8859-1 参考手册",
                "link": "https://www.runoob.com/tags/ref-entities.html"
            },
            {
                "text": "SVG 教程",
                "link": "https://www.runoob.com/svg/svg-tutorial.html"
            },
            {
                "text": "Canvas API",
                "link": "https://wangdoc.com/webapi/canvas"
            }
        ]
    },
    {
        "text": "CSS",
        "data": [
            {
                "text": "CSS3 参考手册",
                "link": "http://www.w3chtml.com/css3/"
            },
            {
                "text": "CSS3 教程",
                "link": "https://www.runoob.com/css3/css3-tutorial.html"
            },
            {
                "text": "CSS3 渐变",
                "link": "https://www.runoob.com/css3/css3-gradients.html"
            },
            {
                "text": "CSS3 2D 转换",
                "link": "https://www.runoob.com/css3/css3-2dtransforms.html"
            },
            {
                "text": "CSS3 过渡",
                "link": "https://www.runoob.com/css3/css3-transitions.html"
            },
            {
                "text": "CSS3 动画",
                "link": "https://www.runoob.com/css3/css3-animations.html"
            },
            {
                "text": "CSS3 多媒体查询",
                "link": "https://www.runoob.com/css3/css3-mediaqueries.html"
            },
            {
                "text": "CSS3 网格布局",
                "link": "https://www.runoob.com/css3/css-grid.html"
            }
        ]
    },
    {
        "text": "JavaScript",
        "data": [
            {
                "text": "MDN Web Docs",
                "link": "https://developer.mozilla.org/zh-CN/"
            },
            {
                "text": "JavaScript 教程",
                "link": "https://www.runoob.com/js/js-tutorial.html"
            },
            {
                "text": "ES 6 教程",
                "link": "https://www.runoob.com/w3cnote/es6-setup.html"
            },
            {
                "text": "ECMAScript 6 入门教程",
                "link": "https://es6.ruanyifeng.com/"
            },
            {
                "text": "正则表达式查询",
                "link": "https://www.jb51.net/tools/zhengze.htm"
            },
            {
                "text": "TypeScript 教程（菜鸟教程）",
                "link": "https://www.runoob.com/typescript/ts-tutorial.html"
            },
            {
                "text": "TypeScript 教程（阮一峰）",
                "link": "https://wangdoc.com/typescript/"
            }
        ]
    },
    {
        "text": "前端框架",
        "data": [
            {
                "text": "Bootstrap 5 中文文档",
                "link": "https://v5.bootcss.com/"
            },
            {
                "text": "Bootstrap 4 中文文档",
                "link": "https://v4.bootcss.com/"
            },
            {
                "text": "Tailwind CSS 中文文档",
                "link": "https://www.tailwindcss.cn/"
            },
            {
                "text": "AdminLTE 官方文档",
                "link": "https://adminlte.io/docs/3.1/index.html"
            },
            {
                "text": "Layui",
                "link": "https://gitee.com/layui"
            },
            {
                "text": "layer 弹出层组件",
                "link": "https://layuion.com/layer/"
            },
            {
                "text": "jQuery API 3.3.1 速查表",
                "link": "https://jquery.cuishifeng.cn/"
            },
            {
                "text": "axios 文档",
                "link": "https://axios-http.com/zh/docs/intro"
            },
            {
                "text": "Chrome 开发者工具中文文档",
                "link": "https://www.html.cn/doc/chrome-devtools/"
            },
            {
                "text": "uni-app 文档",
                "link": "https://uniapp.dcloud.net.cn/"
            },
            {
                "text": "微信小程序官方文档",
                "link": "https://developers.weixin.qq.com/miniprogram/dev/framework/"
            },
            {
                "text": "WeUI 文档",
                "link": "https://github.com/Tencent/weui/wiki"
            },
            {
                "text": "Vue.js V2 文档",
                "link": "https://cn.vuejs.org/v2/guide/"
            },
            {
                "text": "Vue.js V3 文档",
                "link": "https://v3.cn.vuejs.org/guide/introduction.html"
            },
            {
                "text": "Vue Cli 文档",
                "link": "https://cli.vuejs.org/zh/"
            },
            {
                "text": "Vuex 文档",
                "link": "https://vuex.vuejs.org/zh/"
            },
            {
                "text": "Vue Router 文档",
                "link": "https://router.vuejs.org/zh/"
            },
            {
                "text": "Vuetify 文档",
                "link": "https://v2.vuetifyjs.com/zh-Hans/"
            },
            {
                "text": "BootstrapVue 文档",
                "link": "https://bootstrap-vue.org/",
                "tooltip": "http://code.z01.com/bootstrap-vue/docs/"
            },
            {
                "text": "Ant Design Vue Pro 文档",
                "link": "https://pro.antdv.com/"
            },
            {
                "text": "Ant Design Vue 文档",
                "link": "https://next.antdv.com/docs/vue/getting-started-cn/"
            },
            {
                "text": "Element 2 官方文档",
                "link": "https://element.eleme.cn/"
            },
            {
                "text": "Element-Plus 官方文档",
                "link": "http://element-plus.org/"
            },
            {
                "text": "layui - vue",
                "link": "http://www.layui-vue.com/zh-CN/index"
            }
        ]
    },
    {
        "text": "图表",
        "data": [
            {
                "text": "Apache ECharts",
                "link": "https://echarts.apache.org/zh/index.html"
            },
            {
                "text": "Chart.js",
                "link": "https://www.chartjs.org/"
            },
            {
                "text": "D3.js",
                "link": "https://d3js.org/"
            },
            {
                "text": "three.js",
                "link": "https://threejs.org/"
            }
        ]
    },
    {
        "text": "Node",
        "data": [
            {
                "text": "node.js 中文文档",
                "link": "https://nodejs.org/zh-cn/docs/"
            },
            {
                "text": "Node.js 中文网",
                "link": "http://nodejs.cn/api/"
            },
            {
                "text": "Node.js 教程",
                "link": "https://www.runoob.com/nodejs/nodejs-tutorial.html"
            },
            {
                "text": "Deno 文档",
                "link": "https://www.denojs.cn/"
            },
            {
                "text": "Deno 文档",
                "link": "https://www.denojs.cn/"
            },
            {
                "text": "Koa 文档",
                "link": "https://koa.bootcss.com/"
            },
            {
                "text": "Mongoose 文档",
                "link": "http://www.mongoosejs.net/"
            },
            {
                "text": "Sequelize 文档",
                "link": "https://www.sequelize.com.cn/"
            },
            {
                "text": "Egg 文档",
                "link": "https://eggjs.org/zh-cn/"
            },
            {
                "text": "Oak 文档",
                "link": "https://github.com/oakserver/oak"
            },
            {
                "text": "Fastify 文档",
                "link": "https://www.fastify.cn/"
            },
            {
                "text": "riot.js 文档",
                "tooltip": "开发零依赖独立组件，精巧的MVC架构",
                "link": "https://riot.js.org/documentation/"
            },
            {
                "text": "Electron 文档",
                "link": "https://www.electronjs.org/"
            },
            {
                "text": "Electron 中文网",
                "link": "https://electron.org.cn/"
            },
            {
                "text": "electron-vue 文档",
                "website": "https://github.com/SimulatedGREG/electron-vue",
                "link": "https://simulatedgreg.gitbooks.io/electron-vue/content/cn/"
            }
        ]
    },
    {
        "text": "前端CDN",
        "data": [
            {
                "text": "BootCDN 前端开源项目 CDN 加速服务",
                "link": "https://www.bootcdn.cn/"
            },
            {
                "text": "CDNJS.NET - 免费开源前端公共库",
                "link": "https://cdnjs.net/"
            },
            {
                "text": "360 前端静态资源库",
                "link": "https://cdn.baomitu.com/"
            },
            {
                "text": "Staticfile 前端静态资源库",
                "link": "http://www.staticfile.org/"
            },
            {
                "text": "阿里云镜像站",
                "link": "https://developer.aliyun.com/mirror/"
            }
        ]
    },
    {
        "text": "随机数据接口",
        "data": [
            {
                "text": "Random User Generator",
                "link": "https://www.randomuser.me/",
                "tooltip": "使用方法：https://randomuser.me/api/"
            },
            {
                "text": "Lorem Picsum",
                "link": "https://picsum.photos/",
                "tooltip": "使用方法：https://picsum.photos/200/300"
            },
            {
                "text": "Unsplash",
                "link": "https://unsplash.com/",
                "tooltip": "使用方法：https://source.unsplash.com/random/600x500"
            },
            {
                "text": "Bing 每日壁纸",
                "link": "https://www.dujin.org/",
                "tooltip": "使用方法：https://api.dujin.org/bing/1366.php"
            },
            {
                "text": "岁月小筑图片API",
                "link": "http://img.xjh.me/",
                "tooltip": "使用方法：https://img.xjh.me/random_img.php"
            },
            {
                "text": "岁月小筑图片API",
                "link": "https://www.randomuser.me/",
                "tooltip": "使用方法：https://randomuser.me/api/"
            }
        ]
    }
]