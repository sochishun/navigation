# README.md

版本号: 2.0.0

## 介绍

极简主页（环球导航），基于 Element-Plus 前端框架，采用 JSON 数据文件，支持壁纸、搜索功能、暗黑主题模式。

## 内容规范 FAQ

### 如何增加分类

1. 新增 `src/mock/xxx.json` 分类数据文件。
2. 打开文件 `src/mock/_category.js`，在指定排序位置插入新分类的文件名。
3. 进入目录 `src/mock/tool/`，执行命令 `node merge.js`，合并所有分类文件到 `_all.js` 数据源文件。

### 如何修改分类名称和排序

1. 重命名文件：`src/mock/xxx.json`。
2. 打开文件 `src/mock/_category.js`，更改指定的分类名称，如有需要，更改分类的排序位置。
3. 进入目录 `src/mock/tool/`，执行命令 `node merge.js`，合并所有分类文件到 `_all.js` 数据源文件。

### 如何修改快捷导航数据

打开文件 `src/mock/_favorite.js`，更改文件内容。

### 如何修改搜索引擎数据

打开文件 `src/mock/_search.js`，更改文件内容。

### 如何修改我的导航数据

在 “我的导航” 页面，点击页面上的 “编辑导航数据” 按钮，根据提示修改数据内容即可。

## 安装

```bash
npm install element-plus @element-plus/icons-vue
```
