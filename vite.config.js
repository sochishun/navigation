import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },

  // 配置开发服务器端口
  // server: {
  //   port: 5173,
  // },

  // 增加 base 选项，用于 Gitee Pages 基础地址，注意末尾要加斜杠
  base: '/navigation',
})
